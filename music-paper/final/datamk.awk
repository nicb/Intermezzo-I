#
# datamk.awk,v 0.1 1997/08/24 03:16:42 nicb Exp
#
# datamk.awk: data maker - picks up final note data from the 'sezioni'
#	folder and prepares data in suitable format for pgmk.awk; this
#	data must then be integrated by hand with bar size etc. in
#	order to prepare all the info for the page description
#
function delete_array(a,
	i)
{
	for (i=a[0]; i >= 0; --i)
		delete a[i];
}
function save_into_array(arr,item,
	idx)
{
	idx = arr[0];
	arr[++idx] = item;
	arr[0] = idx;
}
function is_already_in_list(instrno,
	i,result)
{
	result = 0;
	for (i = 1; i <= _instrlist_[0]; ++i)
		if (_instr_[instrno] == _instrlist_[i])
		{
			result = i;
			break;
		}
	return result;
}
function add_to_instr_list(instrno)
{
	if (!is_already_in_list(instrno))
		save_into_array(_instrlist_,_instr_[instrno]);
}
function reset_instrument_list()
{
	delete_array(_instrlist_);
	_instrlist_[0] = 0;
}
function produce_line(bar,
	i)
{
	printf("%d|", bar);
	for (i = 1; i < _instrlist_[0]; ++i)
		printf("%s,", _instrlist_[i]);
	
	if (_instrlist_[0])
		printf("%s|\n", _instrlist_[i]);
	else
		print "|";
}
function print_header(start, end, barno, ilist)
{
	print "#\n# Data prepared automagically by datamk.awk,v 0.1\n#";
	printf("# starting bar: %d, ending bar: %d, bar number start: %d\n",
		start, end, barno);
	printf ("# instrument list (not in the correct order):\n# %s\n", ilist);
}
function print_trailer()
{
	print "#\n# End of data prepared by datamk.awk,v";
}
function init_struct(ilist)
{
	_instr_[0] = split(ilist,_instr_,"|");
}
BEGIN {
	barstart = barstart == "" ? 1 : barstart;
	barend = barend == "" ? barstart+5 : barend;
	barno_start = barno_start == "" ? barstart : barno_start;
	_curbar_ = 1;
	instr_list = instr_list == "" ? \
		"wblocks|snare|sorff|tamb|gcassa|incu|sonagli|sorff|tblocks|roto|crash" \
		: instr_list;
	#
	# Structure Initialization (according to the A section)
	#
	init_struct(instr_list);
	_instrlist_[0] = 0;

	print_header(barstart, barend, barno_start,instr_list);
}
END {
	if (_instrlist_[0])
		produce_line(barno_start);

	print_trailer();
}
/^#/ {
	print;
	next;
}
/^===/ {
	if (_curbar_ >= barstart)
	{
		produce_line(barno_start++);
		reset_instrument_list();
	}
	++_curbar_;
	if (_curbar_ > barend)
		exit(0);
	next;
}
{
	if (_curbar_ >= barstart)
		add_to_instr_list($5);
}
#
# datamk.awk,v
# Revision 0.1  1997/08/24 03:16:42  nicb
# added instrument list option capability
# corrected bug in RCS header tags
#
# Revision 0.0  1997/08/22 09:45:01  nicb
# Initial Revision
#
