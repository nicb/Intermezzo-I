#
# measurer.awk,v 0.0 1997/08/25 07:31:15 nicb Exp
#
# measure the duration of a given segment as output by rfit
#
# options:
#	-v metro=<value>	(default: 96)	metronomic tempo value
#	-v num=<value>		(default: 4)	beats per bar
#	-v offset=<value>	(default: 0)	offset timing added
#
function output_duration(bnum,
	bdur)
{
	bdur = (60/metro) * num;
	printf("Metro: %.2f, N.di battute: %d, n. di beats: %d\n", metro,
		barnum, num);
	printf("Durata (approssimativa): %f\n", (bdur*barnum)+offset);
}
BEGIN {
	metro = metro == "" ? 96 : metro;
	num = num == "" ? 4 : num;
	barnum = 1;
	offset = offset == "" ? 0 : offset;
}
END {
	output_duration(barnum);
}
/^===/ {
	++barnum;
}
#
# measurer.awk,v
# Revision 0.0  1997/08/25 07:31:15  nicb
# Initial Revision
#
