#
# This program is needed to verify bar numbers on top of pages
#
BEGIN {
	FS="|";
}
/^#/ { # skip these
	next;
}
$1 ~ /^[0-9]/ {
	++cnt;
	if (cnt != $1)
		print "bar " cnt " does not match! (" cnt "!=" $1 ")";
	next;
}
{
	print "line out of place at bar " cnt;
}
