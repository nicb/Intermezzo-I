#
# $Id: pgmk.awk,v 0.7 1998/04/19 15:38:32 nicb Exp nicb $
#
# mkpg.awk: music page formatter for the Intermezzo I piece; it assumes
# 	the "header" groff and the "pageA3p.include" include file to
#	be present in the current directory; it produces a page to
#	be fed to the following command:
#
#	awk -f mkpg.awk [options] <page.data> | pic | gpicpa3 --- etc.
#
# Page data format
#
# (Bass recorder assumed always present)
#
# <bar no>|<instr list>|<space required>
#
# where <instr list> is a comma-separated list of instruments
# among:
#	incu,tamb,tblocks,wblocks,orff,gc,snare,roto,crash,sonagli
# (they do not need to be in this specific order)
#
# <space required> is a fraction of the page's width; it *must*
# round up to 1 eventually (the program will issue a warning when
# this does not happen)
#
# The options are:
#
#	-v _sep_value_=<value> (default: 0.45)	value for separation
#						between systems
#	-v _pageno_=<value> (default: 1)	page number
#	-v _upper_margin_=<value> (default: 3)	upper margin (in cm)
#	-v _systemdelta_=<value> (default: 3.6) space between flute & perc
# 
function delete_array(a,
	i)
{
	for (i=a[0]; i >= 0; --i)
		delete a[i];
}
function get_structure_field(a,nf,
	temp,result)
{
	temp[0] = split(a,temp,"|")
	result = temp[nf];
	delete_array(temp);
	return result;
}
function is_instrument_there(bar,instr,
	result)
{
	result = index(bar,instr);
	return result;
}
function is_instrument_in_system(instr,
	result,i)
{
	result = 0;

	for (i = 1; i <= _bar_[0]; ++i)
		if ((result=is_instrument_there(_bar_[i],instr)))
			break;
			
	return result;
}
function save_into_array(arr,item,
	idx)
{
	idx = arr[0];
	arr[++idx] = item;
	arr[0] = idx;
}
function get_bar_size(bar)
{
	return get_structure_field(bar,3);
}
function get_bar_number(bar)
{
	return get_structure_field(bar,1);
}
function get_bar_instruments(bar)
{
	return get_structure_field(bar,2);
}
function get_bar_number_of_instruments(bar,
	tstring,temp,result)
{
	tstring = get_bar_instruments(bar);
	result = temp[0] = split(tstring,temp,",");
	delete_array(temp);
	return result;
}
function get_instrument_level(instr,
	i,result)
{
	result = 0;

	for (i = 1; i <= _instr_[0]; ++i)
		if (instr == _instr_[i])
		{
			result = i;
			break;
		}

	return result;
}
function system_header(barno)
{
	if (__sysno__ > 0)
		printf("separator(%f)\n", _sep_value_);

	printf("recorder_system(R%d);\n\nnext_system()\n\n", __sysno__);
}
function get_instr_line(instr,sysno,
	result)
{
	result = sprintf(_instrl_[instr],sysno);
	return result;
}
function get_instrument_label(instr,sysno,
	fl,nls)
{
	fl = substr(_instr_[instr],1,1);
	fl = toupper(fl);
	nls = substr(_instr_[instr],2);
	return sprintf("%s%s%d", fl, nls, sysno);
}
function output_locations(lstring,
	temp,i)
{
	temp[0] = split(lstring,temp,"|");

	for (i = 1; i <= temp[0]; ++i)
		printf("staff(%s)\n", temp[i]);

	delete_array(temp);
	printf("\n");
}
function output_instrument(instr,
	i,n,loc_string,flag)
{
	n = 0;
	last_loc = 0;
	flag = 0;
	loc_string = "";
	printf("%s\n", get_instr_line(instr,__sysno__));
	for (i = 1; i <= _bar_[0]; ++i)
	{
		if (!flag && is_instrument_there(_bar_[i],instr))
		{
			if (loc_string != "")
				loc_string=sprintf("%s|", loc_string);
			loc_string = sprintf("%s%.2f", loc_string, n);
			flag = 1;
		}
		else if (flag && !is_instrument_there(_bar_[i],instr))
		{
			loc_string = sprintf("%s,%.2f",loc_string, n);
			flag = 0;
		}
		n += get_bar_size(_bar_[i]);
	}
	if (flag)
		loc_string = sprintf("%s,1",loc_string, n);
	if (loc_string == "")
		loc_string = "0,0";

	output_locations(loc_string);
}
function output_system(barno,
	i)
{
	system_header(barno);

	for (i = 1; i <= _instr_[0]; ++i)
		if (is_instrument_in_system(_instr_[i]) || barno == 1)
		{
			output_instrument(_instr_[i]);
			print "next_staff()\n";
		}
}
function get_lowest_instrument(bar,
	instrs,temp,i,result,curinstr)
{
	instrs = get_bar_instruments(bar);
	temp[0] = split(instrs,temp,",");
	result = 0;

	for (i = 1; i <= temp[0]; ++i)
		if ((curinstr=get_instrument_level(temp[i])) > result)
			result = curinstr;
	
	delete_array(temp);

	return result;
}
function get_highest_instrument(bar,
	instrs,temp,i,result,curinstr)
{
	instrs = get_bar_instruments(bar);
	temp[0] = split(instrs,temp,",");
	result = 100;

	for (i = 1; i <= temp[0]; ++i)
		if ((curinstr=get_instrument_level(temp[i])) < result)
		{
			result = curinstr;
		}
	
	delete_array(temp);

	return result;
}
function get_highest_instrument_of_all(\
	i,result,cur)
{
	result = 100;
	for (i = 1; i <= _bar_[0]; ++i)
		if ((cur=get_highest_instrument(_bar_[i])) < result)
			result = cur;

	return result;
}
function get_lowest_instrument_of_all(\
	i,result,cur)
{
	result = 0;
	for (i = 1; i <= _bar_[0]; ++i)
		if ((cur=get_lowest_instrument(_bar_[i])) > result)
			result = cur;

	return result;
}
function output_empty_bar(start,end,
	h,l)
{
	h = get_instrument_label(get_highest_instrument_of_all(),__sysno__);
	l = get_instrument_label(get_lowest_instrument_of_all(),__sysno__);
	printf("empty_bar(%.2f,%.2f,%s,%s);\n", start, end, h, l);
}
function output_separator(svalue,lowest)
{
	printf("vline(%.2f,R%d,%s,0,vlinedefaultmarg)\n", svalue, __sysno__,
		get_instrument_label(lowest,__sysno__));
}
function output_brace_separator(highest,lowest)
{
	printf("vbrace(%s,%s);\n", get_instrument_label(highest,__sysno__),
		get_instrument_label(lowest,__sysno__));
	print "vline_raw(0,system_y_start,system_y_end,0,vlinedefaultmarg)";
}
function output_first_separators()
{
	print "vbrace(Incu0,Sonagli0)";
	print "vline_raw(0,system_y_start,system_y_end,0,vlinedefaultmarg)";
}
function output_separators(barno,sysno,
	i,sep,lowest,next_lowest,next_highest,last_sep,numinstrs)
{
	sep = last_sep = 0;
	if (barno == 1)
		output_first_separators();
	else
	{
		next_lowest = get_lowest_instrument_of_all();
		next_highest = get_highest_instrument_of_all();
		output_brace_separator(next_highest,next_lowest);
	}
	for (i = 1; i < _bar_[0]; ++i)
	{
		last_sep = sep;
		sep += get_bar_size(_bar_[i]);

		if(!get_bar_number_of_instruments(_bar_[i]))
			output_empty_bar(last_sep,sep);

		lowest = get_lowest_instrument(_bar_[i]);
		next_lowest = get_lowest_instrument(_bar_[i+1]);
		lowest = lowest > next_lowest ? lowest : next_lowest;
		output_separator(sep,lowest);
	}
	if(!get_bar_number_of_instruments(_bar_[i]))
		output_empty_bar(sep,1);
	output_separator(1,lowest);
}
function output_barnumber(barno,sysno)
{
	printf("barnumber(R%d,%d)\n", sysno,barno);
}
function reset_arrays()
{
	delete_array(_bar_);
	_bar_[0] = 0;
}
function produce_system(barno)
{
	output_system(barno);
	output_barnumber(barno,__sysno__);
	output_separators(barno,__sysno__);
	++__sysno__;
}
function print_header(sv,page,um,sysdelta)
{
	print ".\\\" produced automatically by $RCSfile: pgmk.awk,v $ $Revision: 0.7 $";
	printf(".nr PageNo %d\n", page);
	print ".so header";
	printf(".sp %.2fc\n", um);
	print ".PS\ncopy \"pageA3p.include\"\n";
	if (sysdelta != "")
		printf("systemdelta=%.4f\n", sysdelta);
	printf("# separation value = %f\n", sv);
}
function print_trailer()
{
	print ".PE";
}
BEGIN \
{
	FS="|";
	#
	# Structure Initialization
	#
	_instr_[1] = "incu";
	_instrl_["incu"] = "staff_init(\"Incudine\",1,linedelta,Incu%d)";
	_instr_[2] = "tamb";
	_instrl_["tamb"] = "staff_init(\"Floor Tom\",1,linedelta,Tamb%d)";
	_instr_[3] = "tblocks";
	_instrl_["tblocks"] = "staff_init3(\"T.Blocks\",\"H\",\"M\",\"L\",Tblocks%d)";
	_instr_[4] = "wblocks";
	_instrl_["wblocks"] = "staff_init2(\"W.Blocks\",\"H\",\"L\",Wblocks%d)";
	_instr_[5] = "sorff";
	_instrl_["sorff"] = "staff_init2(\"S.Orff\",\"H\",\"L\",Sorff%d)";
	_instr_[6] = "gcassa";
	_instrl_["gcassa"] = "staff_init(\"G.Cassa\",1,linedelta,Gcassa%d)";
	_instr_[7] = "snare";
	_instrl_["snare"] = "staff_init(\"Tamb.Rullante\",1,linedelta,Snare%d)";
	_instr_[8] = "roto";
	_instrl_["roto"] = "staff_init2(\"Tom-Toms\",\"H\",\"L\",Roto%d)";
	_instr_[9] = "crash";
	_instrl_["crash"] = "staff_init(\"Piatto Crash\",1,linedelta,Crash%d)";
	_instr_[10] = "sizzle";
	_instrl_["sizzle"]="staff_init(\"Piatto Cinese\",1,linedelta,Sizzle%d)";
	_instr_[11] = "sonagli";
	_instrl_["sonagli"] = "staff_init(\"Sonagli\",1,linedelta,Sonagli%d)";
	_instr_[0] = 11;
	#
	#
	#
	_x_offset_ = 0;
	_bar_[0] = 0;
	__sysno__ = 0;
	_head_barno_ = 1;
	_sep_value_ = _sep_value_ == "" ? 0.45 : _sep_value_
	first_bar_flag = 0;
	_pageno_ = _pageno_ == "" ? 1 : _pageno_;
	_upper_margin_ = _upper_margin_ == "" ? 3 : _upper_margin_;

	print_header(_sep_value_,_pageno_,_upper_margin_,_systemdelta_);
}
END \
{
	produce_system(_head_barno_);
	print_trailer();
}
/^#/ || /^\.\\\"/ {
	print;
	next;
}
{
	if (!first_bar_flag)
	{
		_head_barno_ = $1;
		first_bar_flag = 1;
	}
	_sz_ =  $3;

	_x_offset_ += _sz_;

	if(_x_offset_ <= 1)
		save_into_array(_bar_,$0);
	else
	{
		produce_system(_head_barno_);
		reset_arrays();
		save_into_array(_bar_,$0);
		_x_offset_ = _sz_;
		_head_barno_ = $1;
	}
}
#
# $Log: pgmk.awk,v $
# Revision 0.7  1998/04/19 15:38:32  nicb
# Initial Revision, picked up from draft-1/pgmk.awk
# this produces incomplete files which must be completed by hand
#
# Revision 0.7  1997/08/30 14:06:25  nicb
# added sizzle cymbal (used in the D section)
#
# Revision 0.6  1997/08/24 03:11:27  nicb
# added empty_bar() and upper_margin functionalities
#
# Revision 0.5  1997/08/23 20:04:53  nicb
# corrected delete_array() bug
# corrected offset calculation bug
# added handling of first system
# added page number handling
#
# Revision 0.4  1997/08/20 22:40:06  nicb
# added variable separation space
#
# Revision 0.3  1997/08/20 21:26:27  nicb
# added bar numbering macro handler
#
# Revision 0.2  1997/08/20 21:02:17  nicb
# added first bar handling
#
# Revision 0.1  1997/08/20 20:44:14  nicb
# added vertical line separators
#
# Revision 0.0  1997/08/20 20:17:44  nicb
# Initial Revision (incomplete)
#
