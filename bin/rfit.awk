#
# rfit.awk,v 0.19 1997/06/01 09:35:16 nicb Exp
#
# rhythm fitter - takes in sorted input (like fsorted.dat)
#		  and tries to fit into rhythmic/metric grid
#
function abs(num)
{
	return (num < 0) ? -num : num;
}
function print_allowed_vals(num_values) # for debug
{
	if (!no_normal_output)
		print "# allowed values(" num_values "): " allowed_vals_string;
}
function dur_best_fit(num, bar_dur, result_vector,
	best_diff, _max_value, this_dur, diff_dur, i, j)
{
	best_diff = 10000;
	_max_value = max_grain_value * 8;
	for (i=1; i <= num_allowed_values; ++i)
	{
		div_bar = bar_dur / allowed_vals[i];
		for (j = 1; j <= _max_value; ++j)
		{
			this_dur = j * div_bar;
			diff_dur = abs(num - this_dur);
#printf "time: %f - this_dur(%d/%d): %f (diff_dur: %f), bar_dur: %f\n", num, j, allowed_vals[i], this_dur, diff_dur, bar_dur;
			if (diff_dur < (best_diff - diff_eps))
			{
				best_diff = diff_dur;
				result_vector[0] = j;
				result_vector[1] = allowed_vals[i];
#print "**** BEST RESULT";
			}
			else if (this_dur > num)
				break;
		}
	}

	return best_diff;
}
#
# at_best_fit:
#
# 	this function find the closest match to a certain rhythmic
#	pattern given by the duration divisor and puts the best
#	match into the result_vector array which is returned to
#	the caller
#
#	if no_sync is true, the closest match is found regardless
#	of any duration divisor (using the duration fitter, which
#	has more degrees of freedom)
#
function at_best_fit(num, dur_div, bar_dur, result_vector,
	best_diff, _max_value, this_dur, this_div, diff_dur, i, j, div_bar,
	last_diff)
{
	best_diff = 10000;
	_max_value = max_grain_value * 8;

	if (no_sync)
	{
		for (i=1; (this_div = i * dur_div) <= max_grain_value; i *= 2)
		{
			last_diff = 10000;
			div_bar = bar_dur / this_div;
			for (j = 0; j < _max_value; ++j)
			{
				this_dur = j * div_bar;
				diff_dur = abs(num - this_dur);
				if (diff_dur < (best_diff - diff_eps))
				{
					best_diff = diff_dur;
					result_vector[0] = j;
					result_vector[1] = this_div;
				}
				if (diff_dur > last_diff)
					break;
				last_diff = diff_dur;
			}
		}
	}
	else
	{
		for (i=1; i <= num_allowed_values; ++i)
		{
			div_bar = bar_dur / allowed_vals[i];
			for (j = 0; j < _max_value; ++j)
			{
				this_dur = j * div_bar;
				diff_dur = abs(num - this_dur);
#printf "time: %f - this_dur(%d/%d): %f (diff_dur: %f), bar_dur: %f\n", num, j, allowed_vals[i], this_dur, diff_dur, bar_dur;
				if (diff_dur < (best_diff - diff_eps))
				{
					best_diff = diff_dur;
					result_vector[0] = j;
					result_vector[1] = allowed_vals[i];
#print "**** BEST RESULT";
				}
				else if (this_dur > num)
					break;
			}
		}
	}
#		best_diff = dur_best_fit(num, bar_dur, result_vector);

	return best_diff;
}
function collect_stats(at_diff,dur_diff)
{
	++total_calls;
	tot_at_diff += at_diff;
	tot_dur_diff += dur_diff;
}
function fit_output(at, note, dur, bar_dur, extra_info,
	at_vector, dur_vector, tmp_out)
{
	best_dur_diff = dur_best_fit(dur, bar_dur, dur_vector);
	best_at_diff = at_best_fit(at, dur_vector[1], bar_dur, at_vector);
	collect_stats(best_at_diff, best_dur_diff);
	if (debug_output)
		return sprintf ("%d:%d:%s:%d:%d:%s:%g:%g", at_vector[0]+1,
			at_vector[1], note, dur_vector[0], dur_vector[1],
			extra_info, best_at_diff, best_dur_diff); 
	else
		return sprintf ("%3d/%-3d %4s %3d/%-3d %s", at_vector[0]+1,
		at_vector[1], note, dur_vector[0], dur_vector[1], extra_info);
}
function calculate_tempo(t)
{
	if (t != last_calculated_tempo)
	{
		last_calculated_tempo = t;
		calculate_tempo_bar_dur = (240 / t); # (60/tempo)*4
	}
	return calculate_tempo_bar_dur;
}
function get_note_name(num, out_s,	_note, _octave)
{
	_octave = (int(num/12)-2);
	_note = (int(num%12))+1;
	out_s = sprintf("%s%d", note_name[_note], _octave);
	return out_s;
}
function bar_changed(at, bar_dur,
	result, nbars, eps)
{
	result = 0;
	eps=0.04
	nbars = int(at+eps/bar_dur);

	if (nbars > last_bar_change)
		result = 1;

	last_bar_change = nbars;
	return result;
}
function do_output(at, note, dur, tempo, extra_info, b_num, b_div,
	four_four_dur, offset_dur, out_s, div, mult, bar_dur, f_out)
{
	four_four_dur = calculate_tempo(tempo);
 	div = b_div / 4;
	mult = b_num / div;
	bar_dur = (four_four_dur/4) * mult;
	offset_dur = at + phase_offset;
#printf("*****offset_dur %7.4f bar_dur %7.4f last_bar_dur %7.4f at %7.4f last_at %7.4f\n", offset_dur, bar_dur, last_bar_dur, at, last_at);

	out_s = sprintf("%s%s",out_s,fit_output(offset_dur, note, dur,
		four_four_dur, extra_info));

	if (!no_normal_output)
		print out_s;

	last_at = at;
}
function print_version(out_s)
{
    out_s = "# rfit.awk,v 0.19 1997/06/01 09:35:16 nicb Exp\n" \
	"# allowed divisors: \"" allowed_vals_string "\"\n";

	return out_s;
}
function do_header(sect, tempo, bar_num, bar_div,
	out_s, tmp_s)
{
	if (phase_offset)
		out_s = \
		sprintf("%s# %s(%d/%d): mm/quarter = %4.2f, phase offset = %4.8g",
			print_version(tmp_s), sect, bar_num, bar_div,
			tempo, phase_offset);
	else
		out_s = sprintf("%s# %s(%d/%d): mm/quarter = %4.2f",
			print_version(tmp_s), sect, bar_num, bar_div, tempo);
	if (!no_normal_output)
		print out_s;
}
function load_allowed_values(values_string,
	retval)
{
	for (i in allowed_vals)
		delete allowed_vals[i];
	allowed_vals_string=values_string;
	retval=split(values_string, allowed_vals, ":");
	print_allowed_vals(retval);
	return retval;
}
BEGIN {
	last_bar_dur = 10000;
	last_at = 10000;
	last_bar_change = 0;
	bar_dur_num = (bar_dur_num) ? bar_dur_num : 4;
	bar_dur_div = (bar_dur_div) ? bar_dur_div : 4;
	off_eps = 0.01; at_eps = 0.01; diff_eps = 0.00001;
	no_sync=1;
	num_allowed_values = load_allowed_values("1:2:4:6:8:10:12:16:20:24:26:28:32:40:48:56:64");
	max_grain_value = 64;
	split("c:c#:d:d#:e:f:f#:g:g#:a:a#:b", note_name, ":");
}
END {
	if (debug_output)
		print "average at diff :" (tot_at_diff/total_calls) ":, average dur diff :" (tot_dur_diff/total_calls) ":";
}
NR == 1 {
	phase_offset = ph ? ph : 0;
	if (version_only)
	{
		printf print_version();
		exit(0);
	}
	if (allowed_vals_string)
		num_allowed_values = load_allowed_values(allowed_vals_string);
}
/^allowed/ {
	num_allowed_values = load_allowed_values($2);
	next;
}
/^#/ {
	print;
	next;
}
{
	_x_info = "";
	if (!tempo_x)
		tempo_x = 62;
	if (!first_time)
	{
		first_time = 1;
		do_header(section_name, tempo_x, bar_dur_num, bar_dur_div);
	}
	for (i = 4; i <= NF; ++i)
		_x_info = sprintf("%s %s", _x_info, $i);
	do_output($1,$2,$3, tempo_x, _x_info, bar_dur_num, bar_dur_div);
	next;
}
#
# rfit.awk,v
# Revision 0.19  1997/06/01 09:35:16  nicb
# removed bar placement (now operated by rebar.awk in a following pass)
#
# Revision 0.18  1997/05/18 16:07:53  nicb
# corrected more bugs in bar counting
#
# Revision 0.17  1997/05/18 14:06:57  nicb
# corrected bug in bar calculation
#
# Revision 0.16  1997/05/11 18:32:04  nicb
# added support for non 4/4 rhythm marks
#
# Revision 0.15  1997/02/22 21:19:49  nicb
# added no_sync option to suit intermezzo-I needs
#
# Revision 0.14  1995/04/08 16:46:58  nicb
# added extra info handling
#
# Revision 0.13  1995/04/08  16:30:18  nicb
# better allowed vals handling
#
# Revision 0.12  1995/01/28  16:33:21  nicb
# Initial revision of the cello variations
# corrected bug in the allowed values option
#
# Revision 0.11  94/04/29  18:49:57  nicb
# put the full divisors as default
# 
# Revision 0.10  93/10/30  13:10:32  nicb
# modified header to print complete information
# 
# Revision 0.9  93/08/06  17:37:56  nicb
# now average statistics follow the -d option
# 
# Revision 0.8  93/07/27  22:25:25  nicb
# simplified single-section pass two
# 
# Revision 0.7  93/07/14  10:35:14  nicb
# modified at calculator to better fit dur calculation
# 
# Revision 0.6  93/07/11  15:06:09  nicb
# added <allowed> rhythmic divisions record
# optimized array loadings
# 
# Revision 0.5  93/07/10  10:34:22  nicb
# corrected bug in do_header() function
# 
# Revision 0.4  93/07/10  10:27:59  nicb
# added phase offset variable
# added version function
# 
# Revision 0.3  93/07/10  09:06:38  nicb
# added all sections
# 
# Revision 0.2  93/05/16  16:07:02  nicb
# *** empty log message ***
# 
# Revision 0.1  93/05/09  16:44:12  nicb
# added debug output option
# added 1/1 rhythm feature
# 
# Revision 0.0  93/05/09  15:37:21  nicb
# Initial revision
# 
#
