#
# $Id: rfit-format-converter.awk,v 0.1 1997/08/10 18:13:38 nicb Exp $
#
# rfit-format-converter.awk:
#
# 	formats input to one suitable to the rfit.awk program,
#	that is:
#
#	<at>|<note>|<dur>
#
function round(num,
	rounder)
{
	rounder = num >= 0 ? +0.5 : -0.5;
	return int(num+rounder);
}
function convert_sample(n)
{
	return round(n);
}
function convert_instr(n)
{
	return 1;
}
function convert_frq(n,
	frac, base)
{
	frac = (n-1)/100;
	base = 10.00;

	return base+frac;
}
function convert_amp(n,
	result,conv20,max16,off)
{
	off = 96;
	max16 = 32768;
	conv20 = 20/log(10);
	result = (conv20*log(n/max16))+off;

	return result;
}
function convert_dur(n,
	result)
{
	if (!_dur_)
		result = _dur_ = (60/_tempo_)/2;
	else
		result = _dur_;

	return result;
}
function print_header()
{
	;
}
BEGIN {
	_tempo_=60;
	_dur_=0;

	print_header();
}
/^#/ {
	gsub(/^#/, ";");
	print;
	next;
}
{
	at=$1;
	sample=convert_sample($2);
	instr=convert_instr($2);
	frq=convert_frq($2);
	amp=convert_amp($3);
	dur=convert_dur($2);

	printf "%8.5f %f %8.5f %f %d\n", at, frq, dur, amp, sample;
}
