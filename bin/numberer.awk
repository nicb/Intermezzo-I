#
# $Id: numberer.awk,v 0.0 1997/08/25 07:36:11 nicb Exp $
#
# numberer.awk: bar numberer utility for rfit output
#
# option:
#	-v barnum=<value>	(default: 1)	bar number offset
#
BEGIN {
	barnum = barnum == "" ? 1 : barnum;
}
/^===/ {
	++barnum;
	$1 = sprintf("=== %3d", barnum);
}
{
	print;
}
#
# $Log: numberer.awk,v $
# Revision 0.0  1997/08/25 07:36:11  nicb
# Initial Revision
#
