#
# $Id: rebar.awk,v 0.5 1997/08/15 16:48:58 nicb Exp $
#
# This awk program takes the output of the rfit.awk program (versions
# 0.19 and higher) which gives the first field in absolute metric
# timing. $RCSfile: rebar.awk,v $ must take this input and recalculate the output
# considering the metre given at the input (4/4 by default).
#
function delete_array(a,
	i)
{
	for (i=a[0]; i <= 0; --i)
		delete a[i];
}
function round(num)
{
	return int(num < 0 ? num-0.5 : num+0.5);
}
function abs(num)
{
	return num < 0 ? -num : num;
}
function output(line)
{
	print line;
}
function warning(line)
{
	printf("WARNING: %s\n", line) > "/dev/stderr";
}
function print_header()
{
	output("# rebar by $RCSfile: rebar.awk,v $ $Revision: 0.5 $");
}
function calc_div_dur(div,
	qt_dur, result)
{
	qt_dur = 60/tempo;
	result = qt_dur/div;

	return result;
}
function rat_to_time(n, d,
	result, div_dur)
{
	div_dur = calc_div_dur(d);
	result = n * div_dur;

	return result;
}
function check_nbars(nbars)
{
	while(nbars > last_nbars)
	{
		output("===");
		++last_nbars;
	}
	if (nbars < last_nbars)
		warning(sprintf("nbars < last_nbars (%d < %d)",
			nbars, last_nbars));
}
function calc_bar_offset(n, d,
	abs_at, bar_dur, nbars, result, eps)
{
	eps = 0.0001;
	abs_at = rat_to_time(n, d);
	bar_dur = rat_to_time(bar_num, bar_div);
	nbars = int((abs_at+eps)/bar_dur);
	result = abs_at - (nbars*bar_dur);
	check_nbars(nbars);
#printf("*** calc_bar_offset(%d,%d)-> abs_at=%.8f, nbars=%d, result=%.8f\n",
#	n, d, abs_at, nbars, result) > "/dev/stderr";

	return abs(result);
}
function recalc_numerator(nbar_off, d,
	div_dur, result)
{
	div_dur = calc_div_dur(d);
	result = nbar_off/div_dur;

	return result;
}
function is_fractionary(num,
	intnum, eps, diff, result)
{
	result = 0;
	eps = 0.001;
	diff = (diff = num - (int(num)+(eps/2))) > 0 ? 1 - diff : diff;
	result = abs(diff) > eps ? 1 : 0;
#printf ("**** is_fractionary(%.8f [int=%d])=%d (diff: %8f)\n", num, intnum, result, diff) > "/dev/stderr";
	return result;
}
function find_integer(num,
	would_be_int, result)
{
	result = 1;
	would_be_int = num;

	while(is_fractionary(would_be_int))
	{
		++result;
		would_be_int = num * result;
#printf "*** num: %.8f multiplier:  %d, result: %.8f\n", num, result, would_be_int > "/dev/stderr";
	}

	return result;
}
function div_adjust(num,
	result)
{
	result = int((num < bar_div) ? bar_div/num : 1);
	return result;
}
function rebar(at,
	num, div, temp, nbar_off, mul, result, eps)
{
	eps=0.0001;
	temp[0] = split(at, temp, "/");
	num = temp[1] - 1;		# put it into offset-zero mode
	div = temp[2];
	delete_array(temp);
	
	mul = div_adjust(div);
	num *= mul;
	div *= mul;
	nbar_off = calc_bar_offset(num, div);
	num = recalc_numerator(nbar_off, div);
#printf("* rebar(%s): nbar_off=%.8f, num=%.8f, div=%.8f\n", at, nbar_off, num, div);
	mul = find_integer(num);
	num *= mul;
	div *= mul;
	num += 1; 			# back in offset-one
#printf("** rebar(%s): nbar_off=%.8f, num=%.8f(%d), div=%.8f(%d), mul=%d\n", at, nbar_off, num, num+eps, div+eps, div, mul);

	#
	# eps must be added in order to make sure that float to integer
	# conversion is done properly - there are some problems with awk
	# at that level
	#
	result = sprintf("%d/%d", num+eps, div+eps);
	return result;
}
BEGIN {
	bar_num = 4;
	bar_div = 4;
	last_nbars = 0;
	tempo=60;
	print_header();
}
/^#/ {
	output($0);
	next;
}
{
	$1 = rebar($1);
	output($0);
	next;
}
#
# $Log: rebar.awk,v $
# Revision 0.5  1997/08/15 16:48:58  nicb
# corrected bug that did not allow multiple bar rests
#
# Revision 0.4  1997/06/01 14:03:42  nicb
# corrected more rounding errors
#
# Revision 0.3  1997/06/01 13:48:06  nicb
# corrected internal gawk bug on int conversion
#
# Revision 0.2  1997/06/01 09:36:28  nicb
# now functional (beta)
#
# Revision 0.1  1997/06/01 08:15:26  nicb
# solved precision logic problems
# still bugged: rebarring not quite right
#
