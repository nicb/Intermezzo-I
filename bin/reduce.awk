#
# $Id: reduce.awk,v 0.0 1997/06/01 13:49:22 nicb Exp $
#
# reduce:
#
#	takes in two numbers in numerator/divisor form and
#	attempts to reduce them to their lower terms. This code is stolen
#	from the rational.c rred() function in the mup distribution
#	package.
# 
# 	reduce()	reduce a rational number to standard form
# 
# 	This function puts a rational number into standard form; that is,
# 	numerator and denominator will be relatively prime and the denominator
# 	will be positive.  On input, they may be any integers whose absolute
# 	values do not exceed MAXLONG.
# 
# 	Parameters:	num	numerator
#			den	denominator
#			result	array of min terms
#				(result[1] = num; result[2] = den)
# 
# 	Return value:	None.
# 
# 	Side effects:	If ap->d is 0, the function sets raterrno to RATDIV0,
# 			either prints a message or calls a user-supplied
# 			error handler, and sets *ap to 0/1.  Otherwise, it
# 			sets raterrno to RATNOERR and puts *ap in standard form.
#

function reduce(num, den, result,
	b,c,r,sign)
{
#printf "reduce: num %d, den %d\n", num, den;
	result[0] = 4;
	num = round(num);
	den = round(den);
	result[3] = num;
	result[4] = den;
	#
	# Since the numerator and denominator can be anything <= MAXLONG,
	# we must guard against division by 0.
	#
	if (den == 0) {
		print "reduce: division by zero attempted" > "/dev/stderr";
		result[1] = result[2] = 0;
		return;
	}

	if (num == 0) {	 	# if so, answer is "0/1"
		result[1] = 0;
		result[2] = 1;
		return;
	}

	# now figure out sign of answer, and make n & d positive
	sign = 1;			# init to positive
	if (num < 0) {		# reverse if numerator neg
		sign = -sign;
		num = -(num);
	}
	if (den < 0) {		# reverse if denominator neg
		sign = -sign;
		den = -(den);
	}

	# now check whether numerator or denominator are equal
	if (num == den) {		# if so, answer is +1 or -1
		result[1] = sign;
		result[2] = 1;
		return;
	}

	if (num < den) {		# set so that c > b
		c = den;
		b = num;
	} else {
		c = num;
		b = den;
	}

	# use Euclidean Algorithm to find greatest common divisor of c & b
	do {
		r = c % b;
#printf "reduce: r = %d, c = %d, b = %d\n", r, c, b;
		c = b;
		b = r;
	} while (r != 0);

	# now c is the greatest common divisor

	result[1] = num / c;	# divide out greatest common divisor
	result[2] = den / c;	# divide out greatest common divisor

	if (sign < 0)		# put sign in if result should be negative
		result[1] = -(result[1]);

	return;
}
function round(num,
	rounder)
{
	rounder = num >= 0 ? +0.5 : -0.5;
	return int(num+rounder);
}
function delete_array(bf,
	i)
{
	for (i = 1; i <= bf[0]; ++i)
		delete bf[i];

	delete bf[0];
}
BEGIN {
	FS="/";
	_delta_ = 0.1;
	_delta_mm_ = 20;
	_best_num_ = _best_den_ = 10000000000000000000000000;
}
{
	r[0] = 0;
	r[1] = 1000000;
	_n_ = $1;
	_d_ = $2;
	_high_ = $1 + _delta_mm_;
	_low_ = $1 - _delta_mm_;

	while (!(r[1] < 8 && r[2] < 2))
	{
		delete_array(r);
		r[0] = 0;
		reduce(_n_, _d_, r);

		if (r[1] < _best_num_ || r[2] < _best_den_)
		{
			printf "%8.4f/%8.4f=%8.4f/%8.4f\n", r[3], r[4],
				r[1], r[2];
			_best_num_ = r[1];
			_best_den_ = r[2];
		}

		_n_ += _delta_;
		if (_n_ > _high_)
		{
			_delta_ = -_delta_;
			_n_ = $1;
		}
		else if (_n_ < _low_)
			print "sorry, can't find anything better!";
	}
}
#
# $Log: reduce.awk,v $
# Revision 0.0  1997/06/01 13:49:22  nicb
# Initial Revision - functional
#
