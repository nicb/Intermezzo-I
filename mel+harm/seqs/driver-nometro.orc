;
; $Id$
;
	sr=44100
	kr=441
	ksmps=100
	nchnls=1

	instr 1
	igain=ampdb(p7)
	iamp=ampdb(p4)
asig	soundin	p5,p6
aout	linen	asig, 0.01,p3,0.01
	out	aout*iamp*igain
	endin

	instr 2
	igain=ampdb(p7)
	iamp=ampdb(p4)
	iloopstart=p8
	iloopend=p9
	ifno=200
iofno	ftgen	ifno,0,0,-1,p5,0,0,0,0,0
print iamp,iofno
asig	loscil	iamp,440,ifno,440,1,iloopstart,iloopend
aout	linen	asig, 0.01,p3,0.01
	out	aout*iamp*igain
	endin

	instr 100
	endin
;
; $Log$
;
