#!/usr/bin/perl
#
# $Id$
#
# this perl script picks up a csound score and modifies the tempo line
# of a given factor (default = 0.25)
#
# the score is picked up from STDIN
#

use strict;

my $fact = 0.5;
my $line = "";

while ($line = <>)
{
	if ($line =~ /^t\s+/)
	{
		$line = transform_tempo_line($line);
	}

	print $line;
}

#
# end of main
#

sub transform_tempo_line($)
{
	my $line = shift;
	my $result = "";
	my @fields = split(/\s+/, $line);
	my $i = 0;

	$result = shift(@fields);

	for ($i = 0; $i < scalar(@fields); $i += 2)
	{
		$result .= " " . $fields[$i] . " " . $fields[$i+1]*$fact;
	}

	return $result;
}
#
# $Log$
