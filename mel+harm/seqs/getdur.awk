#
# $Id$
#
# get duration of soundfiles from sndinfo output
#
function calc_dur(dur_line,
	temp)
{
	temp[0] = split(dur_line, temp, " ");
	print temp[1];
}
BEGIN {
	FS=",";
}
$4 ~ /seconds$/ {
	calc_dur($4);
}
#
# $Log$
