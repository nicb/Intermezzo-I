#
# $Id: csseq.awk,v 0.7 1998/03/01 19:38:49 nicb Exp nicb $
#
# a sequencer for csound
#
# accepts data in the following format:
#
# metro|<qt metronome data>
#
# | ac. time | dur (opt) |
# <num>|<div>|<num>|<div>|<instr>|<dyn>
# = bar separator
#
# if dur is omitted, the duration of the sample is used.
# dyn goes from fff to ppp in 6 dB steps
# ac. time numerator may be a floating point number; the fractional part
# is a fraction of the divisor added to the numerator (displacement)
#
function now()
{
	return _now_;
}
function print_tempo_line(\
	i, temp, metro, at)
{
	for (i = 1; i <= _tempos_[0]; ++i)
	{
		temp[0] = split(_tempos_[i], temp, "|");
		at = temp[1];
		metro = temp[2];
		if (metro != _last_metro_)
		{
			if (i == 1)
				printf("t ");
			if (_last_metro_idx >= 1)
				printf("%8.4f %8.4f ", at, _last_metro_);
			printf("%8.4f %8.4f ", at, metro);
			_last_metro_ = metro;
			_last_metro_idx = i;
		}
		delete_array(temp);
	}
	printf("\n");
}
function save_tempo_line(at, metro)
{
	append_to_array(sprintf("%-.4f|%.4f", at, metro), _tempos_);
}
function set_rhythm(num, div,
	rdiv)
{
	printf("; set rhythm to %d/%d\n", num, div);
	append_to_array(sprintf("%.4f|%d", _bar_dur_, _num_bars_), _bar_list_);
	_num_bars_ = 0;

	rdiv = div/4;
	_bar_dur_ = num/rdiv;
}
function end_bar()
{
	++_num_bars_;
	_now_ += _bar_dur_;
}
function get_instrno(instr)
{
	return get_data_from_sample(instr, 5);
}
function get_extra_info(instr)
{
	return get_data_from_sample(instr, 6);
}
function get_data_from_sample(instr, field,
	command, sfile)
{
	sfile = 0;
	command = sprintf("grep %s instr.data | awk -F\"|\" '{ print $%d }'\n",
		instr, field);
	command | getline sfile;
	close(command);

	return sfile;
}
function get_sample(instr,
	sfile)
{
	sfile = get_data_from_sample(instr, 1);
	if (sfile == 0)
		warning(sprintf("WARNING! line %d: file %s reference not found\n", NR, instr));

	return sfile;
}
function get_gain(instr)
{
	return get_data_from_sample(instr, 4);
}
function get_dur_from_file(instr,
	command, sfile, result)
{
	sfile = get_sample(instr);
	command = sprintf("sndinfo soundfiles/soundin.%d | awk -f getdur.awk\n",
		sfile);
	command | getline result;
	return result;
}
function get_at(num, div,
	rnum, rnumint, rnumfrac, rdiv)
{
	rdiv = 4/div;
	rnum = num - 1;
	rnumint = int(rnum);
	rnumfrac = rnum-rnumint;

	return now()+(rnumint*rdiv)+(rnumfrac*rdiv);
}
function get_dur(num, div, instr,
	result, rdiv)
{
	if (!num || !div)
		result = get_dur_from_file(instr);
	else
	{
		rdiv = 4/div;
		result = num*rdiv;
	}

	return result;
}
function get_amp(dynamics)
{
	return _dyn_[dynamics];
}
function get_data(note, field,
	command, nstring)
{
	command = sprintf("grep -h '^%s' slaps.data", note);
	command | getline nstring;
	close(command);
	return get_field(nstring, field, ":");
}
function get_skiptime(instr, note,
	result)
{
	result = 0;

	if (note)
		result = get_data(note, 2);

	return result;
}
function output_csound_line(instrno, at, dur, amp, sample, skip, gain, extra,
	extra_string)
{
	extra_string = extra != 0 ? extra "" : "";
	printf("i%d %8.4f %8.4f %8.4f %3d %8.4f %8.4f %s\n", instrno, at, dur,
			amp, sample, skip, gain, extra_string);
}
function print_csound_line(atnum, atdiv, durnum, durdiv, instr, dyn, note,
	at, dur, amp, skip, instrno, sample, gain, extra)
{
	at = get_at(atnum, atdiv);
	dur = get_dur(durnum, durdiv, instr);
	instrno = get_instrno(instr);
	extra = get_extra_info(instr);

	if (!index(instr, "trem"))
	{
		skip = get_skiptime(instr, note);
		sample = get_sample(instr);
		gain = get_gain(instr);
		amp = get_amp(dyn);
		output_csound_line(instrno, at, dur, amp, sample, skip, gain,
			extra);
	}
	else
		produce_tremolo(at, dur, instr, dyn);
}
#
# tremolo functions
#
function get_amp_start(dyn,
	temp, result)
{
	temp[0] = split(dyn, temp, "-");
	result = get_amp(temp[1]);
	delete_array(temp);
	return result;
}
function get_amp_delta(dyn, dur, tdur,
	nsteps, temp, result, start, end)
{
	temp[0] = split(dyn, temp, "-");
	if (temp[0] == 1)
		result = 0;
	else
	{
		start = get_amp(temp[1]);
		end = get_amp(temp[2]);
		nsteps = (dur/tdur)+1;
		result = (end-start)/nsteps;
	}
	delete_array(temp);
	return result;
}
function tremolo_rand(\
	width)
{
	width=0.1;
	return (rand()*width)+(1-(width/2));
}
function init_tremolo_dur(dur)
{
	_tremolo_end_ = 1;
	_tremolo_const_a_ = ((_tremolo_tau_**_tremolo_end_)+\
		_tremolo_const_b_)/dur;
}
function tremolo_func(cur_at,
	result)
{
	result = _tremolo_log_conv_ * \
			log(((_tremolo_const_a_*cur_at)+_tremolo_const_b_));
	return result;
}
function produce_tremolo(at, dur, instr, dyn,
	i, ti, end, trem_instr, curamp, ampdelta, _act_dur_, cur_dur,
	start_dur, tremolo_dur, idur, gain, sample, trem_dur, instrno, extra)
{
	trem_dur[0] = split(instr, trem_dur, ":");
	trem_instr[0] = split(trem_dur[1], trem_instr, ",");
	if (trem_dur[0] > 1)
		tremolo_dur = get_dur(1, trem_dur[2], 0);
	else
		tremolo_dur = trem_instr[2] != trem_instr[3] ? \
			_tremolo_dur_ : _tremolo_dur_*4;
	ti = 2;
	curamp = get_amp_start(dyn);
	ampdelta = get_amp_delta(dyn, dur, tremolo_dur);
	_act_dur_ = tremolo_dur*1.2;
	start_dur = tremolo_dur*tremolo_rand();
	end = at+dur;

	sample[2] = get_sample(trem_instr[2]);
	sample[3] = get_sample(trem_instr[3]);
	gain[2]   = get_gain(trem_instr[2]);
	gain[3]   = get_gain(trem_instr[3]);
	idur[2]   = get_dur_from_file(trem_instr[2]);
	idur[3]   = get_dur_from_file(trem_instr[3]);
	instrno[2] = get_instrno(trem_instr[2]);
	instrno[3] = get_instrno(trem_instr[3]);
	extra[2] = get_extra_info(trem_instr[2]);
	extra[3] = get_extra_info(trem_instr[3]);

	init_tremolo_dur(dur);
	printf("; start tremolo (%s, %s, start=%.4f, end=%.4f)\n; n. instr1=%s, instr2=%s, dur= %.4f\n",
		instr, dyn, at, end, trem_instr[2], trem_instr[3], start_dur);
	for (i = at; i < end; i += cur_dur)
	{
		output_csound_line(instrno[ti], i, idur[ti], curamp, sample[ti],
			skip, gain[ti], extra[ti]);
		ti = ti == 2 ? 3 : 2;	# toggle beween 2 and 3
		curamp += ampdelta;
		if (trem_dur[0] > 1)
			cur_dur = start_dur;
		else
			cur_dur = start_dur * tremolo_func(i-at);
	}
	printf("; end tremolo\n");

	delete_array(trem_instr);
	delete_array(trem_dur);
}
function print_header()
{
	print "; Produced by $RCSfile: csseq.awk,v $ $Revision: 0.7 $";
	print "f1 0 4096 10 1";
}
function print_metronome_line(at)
{
	printf("i100 %8.4f 0.1\n", at);
}
function print_metronome(end,
	at, temp, cur_bar_dur, cur_num_bars, i, j, k, start_bar, end_bar)
{
	at = 0;
	print ";\n; Metronome line\n;";
	if (!_bar_list_[0])
	{
		print "; default metronome line";
		while (at <= end)
			print_metronome_line(at++);
	}
	else
	{
		for (i = 1; i <= _bar_list_[0]; ++i)
		{
			printf("; metro %d of %d (%s)\n", i, _bar_list_[0],
				_bar_list_[i]);
			temp[0] = split(_bar_list_[i], temp, "|");
			cur_bar_dur = temp[1];
			cur_num_bars = temp[2];
			delete_array(temp);

			printf("; bar dur %8.4f for %d bars\n", cur_bar_dur,
				cur_num_bars);
			for (j = 0; j < cur_num_bars; ++j)
			{
				end_bar = start_bar + cur_bar_dur;
				for (k = start_bar; k < end_bar; ++k)
					print_metronome_line(k);
				start_bar = end_bar;
			}
		}
	}
}
BEGIN {
	FS = "|";
	_tremolo_dur_ = _tremolo_dur_ == "" ? 0.07 : _tremolo_dur_;
	_tremolo_start_ = _tremolo_start_ == "" ? 0.5 : _tremolo_start_;
	_tremolo_tau_ = _tremolo_tau_ == "" ? 2500 : _tremolo_tau_;
	_tremolo_const_b_ = _tremolo_tau_**_tremolo_start_;
	_tremolo_log_conv_ = 1/log(_tremolo_tau_);
	metronome = 0;
	_dyn_["fff"] = +1.5;
	_dyn_["ff"]  = 0;
	_dyn_["f"]   = -3;
	_dyn_["mf"]  = -6;
	_dyn_["mp"]  = -9;
	_dyn_["p"]   = -12;
	_dyn_["pp"]  = -15;
	_dyn_["ppp"] = -18;
	_dyn_["nulla"] = -60;
	_tempos_[0] = 0;

	_now_ = 0;
	_bar_dur_ = 4;
	_last_metro_idx = 0;
	_bar_list_[0] = 0;
	_num_bars_ = 0;
	print_header();
}
END {
	set_rhythm(4, 4);
	print_tempo_line();
	if (metronome)
		print_metronome(_now_);
}
/^#/ {
	sub(/^#/, ";");
	print;
	next;
}
/^metro/ {
	save_tempo_line(_now_, $2);
	next;
}
/^rhythm/ {
	set_rhythm($2, $3);
	next;
}
/^=/ {
	end_bar();
	next;
}
{
	print_csound_line($1, $2, $3, $4, $5, $6, $7);
}
#
# $Log: csseq.awk,v $
# Revision 0.7  1998/03/01 19:38:49  nicb
# added nulla entry to the dynamics
#
# Revision 0.6  1998/02/22 16:14:44  nicb
# added looping instruments (tremolos on loscil)
#
# Revision 0.5  1998/02/16 05:02:02  nicb
# added measured tremolo
#
# Revision 0.4  1998/02/15 16:04:50  nicb
# added gain handling for different samples
#
# Revision 0.3  1998/02/14 11:08:58  nicb
# better tremolo handling
#
# Revision 0.2  1998/02/04 19:42:08  nicb
# added tremolo handling
#
# Revision 0.1  1998/02/03 21:26:06  nicb
# added tremolo handling
# added variable dynamics handling
#
# Revision 0.0  1998/02/01 20:46:56  nicb
# Initial Revision, working
#
