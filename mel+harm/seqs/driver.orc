;
; $Id: driver.orc,v 0.1 1998/02/16 04:49:35 nicb Exp nicb $
;
	sr=44100
	kr=441
	ksmps=100
	nchnls=1

	instr 1
	igain=ampdb(p7)
	iamp=ampdb(p4)
asig	soundin	p5,p6
aout	linen	asig, 0.01,p3,0.01
	out	aout*iamp*igain
	endin

	instr 2
	igain=ampdb(p7)
	iamp=ampdb(p4)
	iloopstart=p8
	iloopend=p9
	ifno=200
print iamp
iofno	ftgen	ifno,0,0,-1,p5,0,0,0,0,0
print iofno
asig	loscil	iamp,440,ifno,440,1,iloopstart,iloopend
aout	linen	asig, 0.01,p3,0.01
	out	aout*iamp*igain
	endin

	instr 100
	iamp=ampdb(72)
aout	oscil	iamp,440,1
	out aout
	endin
;
; $Log: driver.orc,v $
; Revision 0.1  1998/02/16 04:49:35  nicb
; added metronome instrument (100)
;
; Revision 0.0  1998/02/04 19:38:24  nicb
; Initial Revision
;
