#
# $Id$
#
function convert_at(at,
	temp, result)
{
	temp[0] = split(at, temp, "/");
	result = sprintf("%d|%d", temp[1],temp[2]);
	delete_array(temp);

	return result;
}
function convert(at, dyn, instrno,
	atstring)
{
	atstring = convert_at(at);
	printf("%s|||%s|%s\n", atstring, instr[instrno], dyn);
}
function print_header()
{
	print "#\nmetro|72\nrhythm|3|4\n#";
}
BEGIN {
	barno = 243;
	instr[1] = "tamb";
	instr[2] = "sorffhi";
	instr[3] = "sizzle";
	instr[4] = "tomhi";
	instr[5] = "sonagli";
	instr[6] = "wblockslo";
	instr[7] = "incu";
	instr[8] = "gcassa";
	instr[9] = "crash";

	print_header();
}
/^#/ {
	print;
	next;
}
/^===/ {
	++barno;
	print;
	printf("#\n# bar %d\n#\n", barno);
	next;
}
{
	convert($1,$4,$5);
}
#
# $Log$
