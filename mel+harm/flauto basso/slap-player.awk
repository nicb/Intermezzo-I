#
# $Id$
#
function get_data(note, field,
	command, nstring)
{
	command = sprintf("grep -h '^%s' slaps.data", note);
	command | getline nstring;
	close(command);
	return get_field(nstring, field, ":");
}
function get_offset(note)
{
	return get_data(note, 2);
}
function get_duration(note,
	at, end, result)
{
	at = get_offset(note);
	end = get_data(note, 3);

	return end-at;
}
function new_at(cur, num, div,
	period)
{
	period = ((_qtperiod_*4)/div)*num;
	return cur + period;
}
function write_score_line(cur, seqline, soundinno,
	temp, foff, dur, note, result, rhythm_num, rhythm_div)
{
#print "cur =" cur ", seqline=" seqline ", soundinno=" soundinno;
	temp[0] = split(seqline, temp, ":");
	rhythm_num = temp[1];
	rhythm_div = temp[2];
	note = temp[3];
	delete_array(temp);

	dur = get_duration(note);
	foff = get_offset(note);

	printf("i2 %8.4f %8.4f %d %8.4f\n", cur, dur, soundinno, foff);

	result = new_at(cur, rhythm_num, rhythm_div);

	return result;
}
function write_score(end, off, soundinno,
	cur, i, nnotes)
{
	cur = off;
	nnotes = _seq_[0];
	i = 1;

	while (cur < end)
	{
		cur = write_score_line(cur, _seq_[i++], soundinno);
		i = i > nnotes ? 1 : i;
	}
}
BEGIN \
{
	FS=":";
	totdur = totdur == "" ? 6.29728 : totdur;
	metro = metro == "" ? 51 : metro;
	offset = offset == "" ? 0 : offset;
	soundinno = soundinno == "" ? 1 : soundinno;
	_qtperiod_ = 60 / metro;
	_seq_[0] = 0;
}
END \
{
	write_score(totdur, offset, soundinno);
}
/^#/ {
	next;
}
{
	append_to_array($0, _seq_);
}
#
# $Log$
