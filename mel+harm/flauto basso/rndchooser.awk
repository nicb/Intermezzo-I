#
# $Id$
#
function redirected_output(string, file)
{
	printf("%s", string) > file;
}
function csound_output(string)
{
	redirected_output(string, csound_file);
}
function script_output(string)
{
	redirected_output(string, link_script);
}
function print_start_header(seed,
	string)
{
	string = "%s\n%s melodies with seed %s\n%s\n";
	csound_output(sprintf(string, _csound_comment_, _csound_comment_,
		seed, _csound_comment_));
	script_output(sprintf(string, _script_comment_, _script_comment_,
		seed, _script_comment_));
}
function print_header(n,
	string)
{
	string = "%s melody n.%d\n";
	csound_output(sprintf(string, _csound_comment_, n));
	script_output(sprintf(string, _script_comment_, n));
}
function chmod_link_file(mode, file,
	command)
{
	command = sprintf("chmod %s %s", mode, file);
	system(command);
}
function output_note(nfile,
	cur_link, at, dur)
{
	cur_link = ++_nlinks_;
	at = (_csound_at_ != 0) ? _csound_at_-_csound_legato_ : 0;
	dur = _csound_dur_ + _csound_legato_;
	script_output(sprintf("ln -sf %-51s soundin.%-3d # (%d)\n", _file_[nfile],
		cur_link, nfile));
	csound_output(sprintf("i2 %5.2f %5.2f %d\t; (%d)\n", at, dur,
		cur_link, nfile));
	_csound_at_ += _csound_dur_;
}
function output_notes(file_array, n, randseed, ncalcs,
	nnotes, i, cur, j)
{
	nnotes = file_array[0];
	srand(randseed);

	print_start_header(randseed);
	for (j = 0; j < ncalcs; ++j)
	{
		print_header(j);
		csound_output(sprintf("i1 %5.2f %5.2f %d\t; sequence %d\n",
			_csound_at_, 0.5, j, j));
		for (i = 0; i < n; ++i)
		{
			cur = 1+round(rand()*(nnotes-1));
			output_note(cur, j);
		}
		_csound_at_ += (2 * _csound_dur_);
	}
}
BEGIN {
	numnotes = numnotes == "" ? 4 : numnotes;
	_file_[0] = 0;
	_nlinks_ = 0;
	_csound_at_ = 0;
	_csound_dur_ = 2;
	_csound_legato_= _csound_dur_ * .05;
	_csound_comment_ = ";";
	_script_comment_ = "#";
	first_randseed = first_randseed == "" ? 32352 : first_randseed;
	numcalcs = numcalcs == "" ? 10 : numcalcs;
	link_script = link_script == "" ? \
		sprintf("output-%d.link", first_randseed) : link_script;
	csound_file = csound_file == "" ? \
		sprintf("output-%d.sco", first_randseed) : csound_file;
}
END {
	output_notes(_file_, numnotes, first_randseed, numcalcs);
	chmod_link_file("755", link_script);
}
{
	append_to_array($0, _file_)
}
#
# $Log$
