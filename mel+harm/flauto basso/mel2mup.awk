#
# $Id: mel2mup.awk,v 0.1 1998/01/26 11:38:50 nicb Exp $
#
# converts a melody file into a mup score
#
# input file format:
#
# <tremolo set>:dur
#
function get_tag()
{
	return sprintf("_t%d", __tag_no__++);
}
function get_note_spec(note_num,
	command, result)
{
	command = sprintf("awk -f spec_extract.awk -v note_num=%d set-score.data",
		note_num);
	command | getline result;
	
	close(command);
	return result;
}
function save_info(tag, specs)
{
	append_to_array(sprintf("%s:%s", tag, specs), _bar_info_);
}
function save_dyn(tag, dyn)
{
	append_to_array(sprintf("%s:%s", tag, dyn), _chord_dyn_);
}
function print_vertical_above(tag, line,
	temp, elev, delta, offset)
{
	delta = 2;
	offset = _vert_offset_;
	temp[0] = split(line, temp, ",");
	elev = (temp[0]*delta)+offset;
	
	for (i = 1; i <= temp[0]; ++i)
	{
		printf("center (%s.x, %s.n+%d) \"\\s(-2)%s\\s(PV)\";\n", tag,
			tag, elev, temp[i]);
		elev -= delta;
	}

	delete_array(temp);
}
function output_info_line(info_line,
	temp, i, tag)
{
	temp[0] = split(info_line, temp, ":");
	tag = temp[1];
#print "***** info_line=" info_line ", tag=" tag ", temp[4]=" temp[4];

	if (temp[4] != "")
		printf("center (%s.x, %s.n+%d) \"%s\";\n", tag, tag,
			_vert_offset_, temp[4]);

	if (temp[5] != "")
		printf("center (%s.x, %s.s-%d) %s \"%s\";\n", tag, tag,
			_vert_offset_, temp[5]);
	
	if (temp[6] != "")
		print_vertical_above(tag, temp[6]);

	delete_array(temp);
}
function output_dyn_line(dyn_spec,
	temp, tag, dyn)
{
	temp[0] = split(dyn_spec, temp, ":");
	tag = temp[1];
	dyn = temp[2];

	printf("center (%s.x, %s.s-%d) \"\\f(TX)%s\\f(PV)\";\n", tag, tag,
		_vert_offset_-4, dyn);
}
function output_info(\
	i)
{
	for (i = 1; i <= _bar_info_[0]; ++i)
		output_info_line(_bar_info_[i]);
}
function output_dyn(\
	i)
{
	for (i = 1; i <= _chord_dyn_[0]; ++i)
		output_dyn_line(_chord_dyn_[i]);
}
function output_note(set, dur,
	temp, nspec, dyn, i, command, info, notename, nsplit, tags,
	nfields)
{
	temp[0] = split(set, temp, "-");
	dyn = temp[3];
	for (i = 0; i < 2; ++i)
		nspec[i] = get_note_spec(temp[i+1]);

	for (i = 0; i < 2; ++i)
	{
		nfields[i] = nsplit[0] = split(nspec[i], nsplit, ":");
		notename[i] = nsplit[2];
		tags[i] = get_tag();
#print "**** set=" set ", dur=" dur ", nspec[" i "]=" nspec[i] ", nfields[" i "]=" nfields[i] ", notename[" i "]=" notename[i];
		delete_array(nsplit);
	}

	printf("%s%s = %s alt 2; %s%s = %s; ", dur, notename[0], tags[0], dur, notename[1], tags[1]);

	save_dyn(tags[0], dyn);
	for (i = 0; i < 2; ++i)
		if (nfields[i] > 2)
			save_info(tags[i], nspec[i]);

	delete_array(temp);
}
function convert_frac_dur(frac,
	result)
{
	result = "";

	if (frac == 0.5)
		result = ".";
	else if (frac == 0.75)
		result = "..";
	else if (frac == 0.875)
		result = "...";
	
	return result;
}
function convert_dur(abs,
	result, integ, remain)
{
	if (!(4 % abs))
		result = sprintf("%d", 4/abs);
	else
	{
		integ = int(4/abs)+1;
		remain = (abs % integ)/integ;
		result = sprintf("%d%s", integ, convert_frac_dur(remain));
	}

	return result;
}
function output_bar(\
	i, dur, temp, set)
{
	printf("1: ");
	for (i = 1; i <= _bar_content_[0]; ++i)
	{
		temp[0] = split(_bar_content_[i], temp, "|");
		dur = convert_dur(temp[1]);
		set = temp[2];
		output_note(set, dur);
		delete_array(temp);
	}

	printf("\n");
	output_dyn();
	output_info();

	print "bar";
	reset_tables();
}
function save(set, dur)
{
	append_to_array(sprintf("%f|%s", dur, set), _bar_content_);
	_cur_at_ += dur;
}
function convert(set, dur,
	rdur, remain_dur)
{
	if (_cur_at_ + dur < bar_num)
		save(set, dur);
	else
	{
		rdur = bar_num - _cur_at_;
		remain_dur = dur - rdur;
		if (rdur > 0)
			save(set, rdur);
		output_bar();
		if (remain_dur > 0)
			save(set, remain_dur);
	}

	delete_array(temp);
}
function print_header(title)
{
	print "// file created by $RCSfile: mel2mup.awk,v $ $Revision: 0.1 $";
	print "include \"mup.h\"";
	print "header";
	printf("\ttitle bold (20) \"%s\"\n", title);
	print "music";
}
function reset_tables()
{
	delete_array(_bar_content_);
	delete_array(_bar_info_);
	delete_array(_chord_dyn_);
	_bar_content_[0] = _bar_info_[0] = _chord_dyn_[0] = _cur_at_ = 0;
}
BEGIN {
	FS=":";
	bar_num = bar_num == "" ? 3 : bar_num;
	_cur_at_ = 0;
	_bar_content_[0] = 0;
	_bar_info_[0] = 0;
	_chord_dyn_[0] = 0;
	_vert_offset_ = _vert_offset_ == "" ? 9 : _vert_offset_;

	print_header(t);
}
{
	convert($1, $2);
}
#
# $Log: mel2mup.awk,v $
# Revision 0.1  1998/01/26 11:38:50  nicb
# added dynamics printing
#
# Revision 0.0  1998/01/26 11:06:51  nicb
# Initial Revision
#
