;
; $Id: driver.orc,v 0.0 1998/01/25 17:49:51 nicb Exp $
;
	sr=44100
	kr=441
	ksmps=100
	nchnls=1


	instr 1
	print p4
	endin

	instr 2
asig	soundin	p4
aout	linen	asig, 0.01,p3,0.01
	out	aout
	endin
;
; $Log: driver.orc,v $
; Revision 0.0  1998/01/25 17:49:51  nicb
; Initial Revision
;
