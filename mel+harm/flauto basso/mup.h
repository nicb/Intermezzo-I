// $Id$
//
// Header files for melody score
//
score
//
// a4 size
//
//pagewidth=11.6944
//pageheight=16.5278
//packfact=2.8
//scoresep=15,35
//staffsep=28			// separation between staffs
//scorepad=28			// separation between staff systems
time=3/4
//
//
beamstyle=4,4,4
staffs=1
staff 1
clef=bass
footer2
	title "- \% -"
//
// $Log$
