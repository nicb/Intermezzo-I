#
# $Id$
#
# converts melody file duration into actual score durations
#
# input file format:
#
# <tremolo set>:dur
#
function convert(value,
	period)
{
	period = 60/tempo;

	return value/period;
}
BEGIN {
	FS=":";
	tempo = tempo == "" ? 60 : tempo;
	mul = mul == "" ? 1 : mul
}
{
	converted = convert($2*mul);
	if (mul != 1)
		print $1, converted, "($2*mul=" $2*mul ")";
	else
		print $1, converted;
}
#
# $Log$
