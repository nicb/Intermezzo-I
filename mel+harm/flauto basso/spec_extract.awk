#
# $Id$
#
# note spec extractor
#
BEGIN {
	FS=":";
	note_num = note_num == "" ? 1 : note_num;
}
/^#/ {
	next;
}
$1 == note_num {
	print;
}
#
# $Log$
