#
# $Id$
#
# options:
#
# 	[-v cur=<n>]		offset time (default: 0)
#	[-v metro=<n>]		metro (default: 51)
#	[-v qtperbar=<n>]	quarters per bar (default:4)
#	[-v spaceconvt=<n>]	space conversion (default: 5/70)
#	[-v endtime=<n>]	loop end time (default: 70)
#	[-v dur=<n>]		delta duration (default: 6.29728)
#
#
BEGIN {
	cur = cur == "" ? 0 : cur;
	metro = metro == "" ? 51 : metro;
	qtperbar = qtperbar == "" ? 4 : qtperbar;
	spaceconvt = spaceconvt == "" ? 5/70 : spaceconvt;
	endtime = endtime == "" ? 70 : endtime;
	dur = dur == "" ? 6.29728 : dur;

	qt = (60/metro)*qtperbar;

	printf("%7s\t%7s\t%7s\n", "cur time", "qt notes", "space");
	while (cur < endtime)
	{
		printf("%7.3f\t\t%7.2f\t\t%7.2f\n", cur, cur/qt,
			cur*spaceconvt);
		cur += dur;
	}
}
#
# $Log$
