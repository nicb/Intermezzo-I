#
# $Id$
#
BEGIN {
	FS = "|"
	_n_rec_ = 1;
}
/^=/ {
	++_n_rec_;
}
$1 ~ /^mup/ {
	if (req == _n_rec_)
		print $2;
}
#
# $Log$
