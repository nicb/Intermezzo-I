#
# $Id: danze-tbl.awk,v 0.3 1997/01/26 15:28:19 nicb Exp $
#
function delete_array(a,
	i)
{
	for (i = 1; i < a[0]; ++i)
		delete a[i];

	delete a[0];
}
function set_nome(val)
{
	_nome_ = val;
}
function set_origine(val)
{
	_origine_ = val;
}
function set_ritmo(val)
{
	_ritmo_ = val;
}
function set_tempo(val)
{
	_tempo_ = val;
}
function set_array_val(array, val,
	n)
{
	n = array[0];
	array[++n] = val;
	array[0] = n;
}
function set_note(val,
	n)
{
	set_array_val(_note_, val);
}
function set_mupfile(val,
	n)
{
	set_array_val(_mupfile_, val);
}
function print_header(musicwidth)
{
	print ".\\\" created automagically by $RCSfile: danze-tbl.awk,v $ $Revision: 0.3 $\n.\\\" DO NOT EDIT!";
	# nome ritmo tempo origini note() esempi()
	print ".TS H\nexpand tab(|) allbox;\ncf3 cf3 cf3 cf3 cf3 cf3";
	printf "cf3 c c c l lw(%dc).\n", musicwidth;
	print "Nome|Ritmo|Tempo|Origine|Note|Esempi\n=\n.TH";
}
function pspic_string(file,
	filename)
{
	filename = file
	sub(/\.mup/, ".eps", filename)
	return sprintf("T{\n.PSPIC \"mupfiles/%s\"\nT}", filename);
}
function spaced_string(string,
	result)
{
	result = string != "" ? sprintf("T{\n%s\nT}", string) : string;

	return result; 
}
function print_record(\
	nline)
{
	printf("%s|%s|%s|%s|", _nome_, spaced_string(_ritmo_),
		spaced_string(_tempo_), _origine_);

	if (_note_[0] >= 1)
		printf("%s|", spaced_string(_note_[1]));
	else
		printf("|");

	if (_mupfile_[0] >= 1)
		printf("%s\n", pspic_string(_mupfile_[1]));
	else
		printf("\n");

	for (nline = 2; nline <= _note_[0]; ++nline)
	{
		printf("\\^|\\^|\\^|\\^|%s|", spaced_string(_note_[nline]));
		if (nline <= _mupfile_[0])
			printf("%s\n", pspic_string(_mupfile_[nline]));
		else
			printf("\\^\n");
	}
	for (; nline <= _mupfile_[0]; ++nline)
		printf("\\^|\\^|\\^|\\^|\\^|%s\n",
			pspic_string(_mupfile_[nline]));
}
function reset_values()
{
	set_nome("");
	set_origine("");
	set_ritmo("");
	set_tempo("");
	
	delete_array(_note_);
	delete_array(_mupfile_);
}
BEGIN {
	FS="|";
	OFS="|";
	mwidth= (mwidth == 0) ? 8 : mwidth;
	reset_values();
	print_header(mwidth);
}
END {
	print_record();
	print ".TE";
}
/^\./ {				# whatever begins by a dot is a troff command
	print;
	next;
}
/^#/ {
	gsub(/^#/, ".\\\" ");
	print;
	next;
}
/^=/ {
	print_record();
	reset_values();
	print;
	next;
}
/^nome/ {
	set_nome($2);
	next;
}
/^origine/ {
	set_origine($2);
	next;
}
/^ritmo/ {
	set_ritmo($2);
	next;
}
/^tempo/ {
	set_tempo($2);
	next;
}
/^note/ {
	set_note($2);
	next;
}
/^mupfile/ {
	set_mupfile($2);
	next;
}
#
# $Log: danze-tbl.awk,v $
# Revision 0.3  1997/01/26 15:28:19  nicb
# added parametrized music width
#
# Revision 0.2  1997/01/26 10:06:09  nicb
# added separated functions for pspic_string and spaced_string
#
# Revision 0.1  1997/01/02 18:49:14  nicb
# added direct troff command handling
#
# Revision 0.0  1996/12/31 14:34:38  nicb
# Initial Revision
#
#
