;
; $Id: driver.orc,v 0.0 1997/02/05 16:21:26 nicb Exp nicb $
;
	sr=22050
	kr=2205
	ksmps=10
	nchnls=2

	instr 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16
	iampscale=0.5
	iamp=(ampdb(2.1463*20*(1/log(10))*log(p4)))*iampscale
	ifreq=p5
	i1sdur=0.6*p3
	i2sdur=0.4*p3
	i1slev=iamp
	ileft=(1/p1)
	iright=1-ileft;
kamp	linseg	iamp,i1sdur,i1slev,i2sdur,0
aout	oscil	kamp,ifreq,p1
	outs aout*ileft,aout*iright
	endin
;
; $Log: driver.orc,v $
; Revision 0.0  1997/02/05 16:21:26  nicb
; Initial Revision
;
;
