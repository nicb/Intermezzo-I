#
# $Id: mup2csound.awk,v 0.8 1997/01/25 16:53:42 nicb Exp $
#
function delete_array(a,
	i)
{
	for (i = 1; i < a[0]; ++i)
		delete a[i];

	delete a[0];
}
function print_header()
{
	print "; produced automagically by $RCSfile: mup2csound.awk,v $ $Revision: 0.8 $";
	print "f1 0 1024 10 1 0.25 0.1111 0.0625";
	print "f2 0 1024 7 1 1023 -1";
	print "f3 0 1024 10 1 0.5 0.333 0.25 0.125";
}
function get_note(name,
	pos, result)
{
	pos = match(name, /[0-9+-]/);
	result = pos ? substr(name, 0, pos-1) : name;

	return result;
}
function get_octave(name, defoct,
	pos, result)
{
	pos = match(name, /[0-9+-]/);
	result = pos ? substr(name, pos) : defoct;
	if (result == "+")
		result = defoct + 1;
	else if (result == "-")
		result = defoct - 1;

	return result;
}
function reset_local_accidentals(\
	temp, note, i)
{
	temp[0] = split(_fifths_up_, temp, ":");

	for (i = 1; i <= temp[0]; ++i)
	{
		note = temp[i];
		if (_local_accidental_[note])
			delete _local_accidental_[note];
	}
}
function alterate(note,
	acc, result, n)
{
	result = 0;
	n = substr(note,1,1);
	
	if (_local_accidental_[n] == "n")
		return result;

	if (_key_notes_[n])
		result = (_key_notes_[n] == "#") ? +1 : -1;
	else if (_local_accidental_[n])
		result = (_local_accidental_[n] == "#") ? +1 : -1;

	return result;
}
function set_local_accidental(note,
	n, acc)
{
	n = substr(note,1,1);
	acc = substr(note,2,1);
	_local_accidental_[n] = acc;
}
function key_filter(note,
	result)
{
	result = _note_[note];

	if (match(note, /[n#&]/))
		set_local_accidental(note);
	else
		result += alterate(note);

	return result;
}
function convert_note(name, defoct, returned,
	octave, note, notenum)
{
	octave = get_octave(name, defoct);
	note = get_note(name);
	notenum = key_filter(note);
	octave = (note == "c&") ? --octave : octave;
	octave = (note == "b#") ? ++octave : octave;

	returned[0] = notenum;
	returned[1] = octave + 4;
}
function get_pulse_dur()
{
	return 60/metro;
}
function calculate_time(rhythm,
	i, cur_time, num, npos, fractime, result, l)
{
	result = (4/rhythm)*get_pulse_dur();
	npos = match(rhythm, /\./);
	l = length(rhythm);
	if (npos)
	{
		cur_time = result;
		for (i = npos; i <= l; ++i)
		{
			cur_time /= 2;
			result += cur_time;
		}
	}

	return result;
}
function advance_action_time(rhythm)
{
	_cur_at_ += calculate_time(rhythm);
#print "advance at called: _cur_at_ = " _cur_at_;
}
function reset_action_time()
{
	_cur_at_ = 0;
#print "reset at called";
}
function get_action_time(rhythm)
{
#print "get at called: returning " _bar_at_ + _cur_at_;
	return _bar_at_ + _cur_at_;
}
function advance_bar_time(\
	rdiv)
{
	rdiv = 4/_time_divisor_;
	_bar_at_ += (_time_numerator_*rdiv*get_pulse_dur());
#print "advance bar time called: (_time_numerator_, _time_divisor_, rdiv, _bar_at_): " _time_numerator_, _time_divisor_, rdiv, _bar_at_, get_pulse_dur();
}
function print_note(at, note, dur, dyn, fn)
{
	printf("i1 %8.3f %8.3f %8.3f %4s %d\n", at, dur, dyn, note, fn);
}
function analyze_note(notestring, nvoice,
	defoct, notearray, result)
{
	defoct = (nvoice == 1) ? 4 : 3; # gross hack here
	convert_note(notestring, defoct, notearray);

	if (notearray[0] != -1)
		result = sprintf("%d.%02d", notearray[1], notearray[0]);
	else
		result = sprintf("%d", notearray[0]);

	return result;
}
function produce_note_line(rhythm, notestring, nvoice,
	pchnote, dur)
{
	pchnote = analyze_note(notestring, nvoice);
	dur = calculate_time(rhythm)*0.8;

	if (pchnote != "-1")	# pause or space
		print_note(get_action_time(), pchnote, dur, def_dynamics, nvoice);
}
function produce_note_group(ngroup, nvoice,
	oct, rhythm, npos, i, remstring, notestring, nnpos)
{
	gsub(/ */, "", ngroup);
	npos = match(ngroup, /[a-grs]/);
	if (!npos)
	{
		rhythm = ngroup;
		_last_rhythm_value_ = rhythm;
	}
	else if (npos > 1)
	{
		rhythm = substr(ngroup, 1, npos-1);
		_last_rhythm_value_ = rhythm;
	}
	else					# if rhythm is missing
		rhythm = _last_rhythm_value_;

#print "*(ngroup, npos, rhythm, length) " ngroup, npos, rhythm, length(ngroup);

	i = npos;
	while(i <= length(ngroup))
	{
		if (i == 0)			# if note is missing
			notestring = _last_note_;
		else
		{
			remstring = substr(ngroup, i);
#print "**(i, ngroup, remstring) " i, ngroup, remstring;
			nnpos = match(substr(remstring, 2), /[a-grs]/);
#print "***(nnpos) " nnpos;
			if (nnpos)
				notestring = substr(remstring, 1, nnpos);
			else
				notestring = remstring;
#print "****(i, nnpos, ngroup, notestring, length) " npos, nnpos, ngroup, notestring, length(notestring);
		}

		produce_note_line(rhythm, notestring, nvoice);
		_last_note_ = notestring;
		if (i)
			i += length(notestring);
		else
			i += (length(notestring) + 1);
	}

	advance_action_time(rhythm);
}
function produce_notes(nstring, nvoice,
	temp, i, last_note_group, current_note_group)
{
	temp[0] = split(nstring, temp, ";");

	for (i = 1; i < temp[0]; ++i)
	{
#print "**** numgroups: " temp[0] ", current group: \"" temp[i] "\"";
		current_note_group = (temp[i] !~ /^ *$/) ? temp[i] :\
			last_note_group;
		produce_note_group(current_note_group, nvoice);
		last_note_group = current_note_group;
	}

	delete_array(temp);
}
function multi_remover(string, fromchar, tochar,
	result, startidx, endidx, pre_substr, post_substr)
{
	result = string;
#print "******* multi_remover before: " result;
	while(startidx = match(result, fromchar))
	{
		endidx = match(result, tochar);
#print "******* multi_remover during (startidx, endidx): " startidx, endidx;
		pre_substr = substr(result, 1, startidx-1);
		post_substr = substr(result, endidx+1);
		result = sprintf("%s%s", pre_substr, post_substr);
#print "******* multi_remover during (result, pre_substr, post_substr): " result,pre_substr, post_substr;
	}
#print "******* multi_remover after: " result;

	return result;
}
function condition_note_line(line)
{
#print "******* unconditioned: " line;
	gsub(/ebm/, "", line);
	gsub(/bm/, "", line);
	line = multi_remover(line, "\\[", "\\]");
	line = multi_remover(line, "<", ">");
	gsub(/~/, "", line); 		# to be removed in the future

#print "******* conditioned: " line;

	return line;
}
function analyze_line(line,
	rline, note_string, colon_pos, voicenum)
{
	rline = condition_note_line(line);
	colon_pos = match(rline, /:/);
	note_string = substr(rline, colon_pos+1);
	voicenum = substr(rline, 0, 1) + 0;
	produce_notes(note_string, voicenum);
}
function set_key(keystring,
	num, acc, fors, i, temp, note)
{
	num = substr(keystring, 1, 1);
	acc = substr(keystring, 2);
	fors = (acc == "#") ? _fifths_up_ : _fifths_down_;

	temp[0] = split(fors, temp, ":");

	for (i = 1; i <= num; ++i)
	{
		note = temp[i];
		_key_notes_[note] = acc;
#print "set_key: setting key for " keystring ": " _key_notes_[note] " (fors, temp[0]) " fors, temp[0];
	}

	delete_array(temp);
}
function set_time(timestring,
	temp)
{
	if (timestring == "common" || timestring == "cut")
		return;

	temp[0] = split(timestring, temp, "/");
	_time_numerator_ = temp[1];
	_time_divisor_ = temp[2];
}
BEGIN {
	metro=60;
	def_dynamics=80;
	_cur_at_ = _bar_at_ = 0;
	_last_rhythm_value_ = "4";
	_time_numerator_ = 4;
	_time_divisor_ = 4;

#notes

	_note_["b#"]  = _note_["c"]  = _note_["cn"] = _note_["d&&"] = 0;
	_note_["b##"] = _note_["c#"] = _note_["d&"] = 1;
	_note_["c##"] = _note_["d"]  = _note_["dn"] = _note_["e&&"] = 2;
	_note_["d#"]  = _note_["e&"] = 3;
	_note_["d##"] = _note_["e"]  = _note_["en"] = _note_["f&"] = 4;
	_note_["e#"]  = _note_["f"]  = _note_["fn"] = _note_["g&&"] = 5;
	_note_["e##"] = _note_["f#"] = _note_["g&"] = 6;
	_note_["f##"] = _note_["g"]  = _note_["gn"] = _note_["a&&"] = 7;
	_note_["g#"]  = _note_["a&"] = 8;
	_note_["g##"] = _note_["a"]  = _note_["an"] = _note_["b&&"] = 9;
	_note_["a#"]  = _note_["b&"] = 10;
	_note_["a##"] = _note_["b"]  = _note_["bn"] = _note_["c&"] = 11;
	_note_["s"]   = _note_["r"]  = -1;

#	keys

	_fifths_up_ = "f:c:g:d:a:e:b";
	_fifths_down_ = "b:e:a:d:g:c:f";

	print_header();
}
/^\/\// {
	sub(/\/\//, ";");
	print;
	next;
}
/^key=/ {
	keypos = match($0, /\=/);
	set_key(substr($0, keypos+1));
}
/^time=/ {
	keypos = match($0, /\=/);
	set_time(substr($0, keypos+1));
}
/^bar$/ || /^repeat[se][te]/ || /^invisbar$/ {
	print "; " $0;
	advance_bar_time();
	reset_local_accidentals();
	next;
}
/^[0-9]/ && /m[sr];/ {
	reset_action_time();
	next;
}
/^[0-9]/ {
#print "analyzing line " $0;
	analyze_line($0);
	reset_action_time();
}
#
# $Log: mup2csound.awk,v $
# Revision 0.8  1997/01/25 16:53:42  nicb
# added handling of + and - octave marks
#
# Revision 0.7  1997/01/04 11:28:30  nicb
# added multi_remover facility to substitute gsub
#
# Revision 0.6  1997/01/02 12:42:50  nicb
# added facility for full bar rests or spaces (m[sr])
#
# Revision 0.5  1997/01/02 12:35:35  nicb
# corrected bug of c& and b# octave displacement
#
# Revision 0.4  1997/01/01 16:51:40  nicb
# corrected incomplete note groups
#
# Revision 0.3  1997/01/01 14:25:52  nicb
# better chord analysis
#
# Revision 0.2  1997/01/01 12:32:08  nicb
# better time division handling
#
# Revision 0.1  1997/01/01 10:20:56  nicb
# accidentals handling added
#
# Revision 0.0  1996/12/31 18:56:05  nicb
# Initial Revision; functional but highly incomplete
#
