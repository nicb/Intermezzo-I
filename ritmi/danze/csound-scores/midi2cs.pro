# MIDI2CS PROJECT FILE 0.94 - rubo@berlin.snafu.de
#
# This projectfile shows default options
# Change settings however you need
# (deleting semicolons, adding samplefiles, ...)

# --- PROJECT GLOBAL VALUES SECTION ---

midifile	midi2cs.mid	; midifile (will be read)
;nameofscore	midi2cs.sco	; score file (will be written)
scoreheader	driver.hdr	; file to be included into scorefile (readonly)
nameoforc	off		; orchestra file (will be written)

# samplerate, mono/stereo, master volume, tempo
sr	44100	; samplerate
nchnls	2	; 1=mono 2=stereo
givol	0.2	; master volume
;tempo	groovesample	beats	; easy looping and syncing!
;tempo	120

# all tracks will be initialized by the following values

firstbar 	0
lastbar 	1000
transpose -24
; notrackpreselected
; lesscomments
; separators

# - define default parameters and succession -

; p_pch
p_midivelocity
; p_midinote
; p_midirelease
; p_fixeddB -12
p_cps midi2cs.cps
; p_maxamplitude 32000

# --- TRACK SECTION ---
# define individiual values for each track

miditrack	1	;off

	; sound <file>	; soundpath (rel. or absolute WAV-, AIF- or ORCfile)
	; name dummy	; soundname
	; relvolume 1.0	; relative volume
	; instrument 1	; number of Csound instrument

	score	; all score parameters and order are optional

		; firstbar   0
		; lastbar  1000
		; duration 1000	; fixed duration in milliseconds
		; duration sample	; fixed duration of used sample
		; transpose +12	; up or down in n halftone steps
		; lowest c3	; notes less than this will be transposed up
		; highest c4	; notes higher than this will be transposed down
		; delay -0.3	; the start parameter (p2) will be delayed

		# --- parameters and succession ---
		; p_pch
		; p_midinote
		; p_midivelocity
		; p_midirelease
		; p_maxamplitude 32000
		; p_fixeddB -6
		; p_cps midi2cs.cps

		; channelsplitting	instr will be instr + channel
		# - if you want to use default parameters set limit with parameters -
		; parameters 10	; parameters has to be the last entry
		               	; in the score section (instr + start +
		               	; duration + the defined parameters)
	endscore
	; separator
endtrack

miditrack	2	;off

	; sound <file>	; soundpath (rel. or absolute WAV-, AIF- or ORCfile)
	; name dummy	; soundname
	; relvolume 1.0	; relative volume
	instrument 16	; number of Csound instrument

	score	; all score parameters and order are optional

		; firstbar   0
		; lastbar  1000
		; duration 1000	; fixed duration in milliseconds
		; duration sample	; fixed duration of used sample
		; transpose 0	; up or down in n halftone steps
		; lowest c3	; notes less than this will be transposed up
		; highest c4	; notes higher than this will be transposed down
		; delay -0.3	; the start parameter (p2) will be delayed

		# --- parameters and succession ---
		; p_pch
		; p_midinote
		; p_midivelocity
		; p_midirelease
		; p_maxamplitude 32000
		; p_fixeddB -6
		; p_cps midi2cs.cps

		; channelsplitting	instr will be instr + channel
		# - if you want to use default parameters set limit with parameters -
		; parameters 10	; parameters has to be the last entry
		               	; in the score section (instr + start +
		               	; duration + the defined parameters)
	endscore
	; separator
endtrack

miditrack	3	;off

	; sound <file>	; soundpath (rel. or absolute WAV-, AIF- or ORCfile)
	; name dummy	; soundname
	; relvolume 1.0	; relative volume
	instrument 6	; number of Csound instrument

	score	; all score parameters and order are optional

		; firstbar   0
		; lastbar  1000
		; duration 1000	; fixed duration in milliseconds
		; duration sample	; fixed duration of used sample
		; transpose 0	; up or down in n halftone steps
		; lowest c3	; notes less than this will be transposed up
		; highest c4	; notes higher than this will be transposed down
		; delay -0.3	; the start parameter (p2) will be delayed

		# --- parameters and succession ---
		; p_pch
		; p_midinote
		; p_midivelocity
		; p_midirelease
		; p_maxamplitude 32000
		; p_fixeddB -6
		; p_cps midi2cs.cps

		; channelsplitting	instr will be instr + channel
		# - if you want to use default parameters set limit with parameters -
		; parameters 10	; parameters has to be the last entry
		               	; in the score section (instr + start +
		               	; duration + the defined parameters)
	endscore
	; separator
endtrack

miditrack	4	;off

	; sound <file>	; soundpath (rel. or absolute WAV-, AIF- or ORCfile)
	; name dummy	; soundname
	; relvolume 1.0	; relative volume
	instrument 12	; number of Csound instrument

	score	; all score parameters and order are optional

		; firstbar   0
		; lastbar  1000
		; duration 1000	; fixed duration in milliseconds
		; duration sample	; fixed duration of used sample
		; transpose 0	; up or down in n halftone steps
		; lowest c3	; notes less than this will be transposed up
		; highest c4	; notes higher than this will be transposed down
		; delay -0.3	; the start parameter (p2) will be delayed

		# --- parameters and succession ---
		; p_pch
		; p_midinote
		; p_midivelocity
		; p_midirelease
		; p_maxamplitude 32000
		; p_fixeddB -6
		; p_cps midi2cs.cps

		; channelsplitting	instr will be instr + channel
		# - if you want to use default parameters set limit with parameters -
		; parameters 10	; parameters has to be the last entry
		               	; in the score section (instr + start +
		               	; duration + the defined parameters)
	endscore
	; separator
endtrack

endofproject
