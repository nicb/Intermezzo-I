f1 0 2048 10 1.0000 0.0313 0.0010 
f2 0 2048 10 1.0000 0.0333 0.0011 
f3 0 2048 10 1.0000 0.0357 0.0013 
f4 0 2048 10 1.0000 0.0385 0.0015 
f5 0 2048 10 1.0000 0.0417 0.0017 
f6 0 2048 10 1.0000 0.0455 0.0021 
f7 0 2048 10 1.0000 0.0500 0.0025 0.0001 
f8 0 2048 10 1.0000 0.0556 0.0031 0.0002 
f9 0 2048 10 1.0000 0.0625 0.0039 0.0002 
f10 0 2048 10 1.0000 0.0714 0.0051 0.0004 
f11 0 2048 10 1.0000 0.0833 0.0069 0.0006 
f12 0 2048 10 1.0000 0.1000 0.0100 0.0010 0.0001 
f13 0 2048 10 1.0000 0.1250 0.0156 0.0020 0.0002 
f14 0 2048 10 1.0000 0.1667 0.0278 0.0046 0.0008 0.0001 
f15 0 2048 10 1.0000 0.2500 0.0625 0.0156 0.0039 0.0010 0.0002 
f16 0 2048 10 1.0000 0.5000 0.2500 0.1250 0.0625 0.0313 0.0156 0.0078 0.0039 0.0020 0.0010 0.0005 0.0002 0.0001 
; /U/USER/NICB/ME/MUSIC/INTERMEZZO-I/RITMI/DANZE/CSOUND-SCORES/ALLEMANDA-J.S.BACH.MIDI - Csound score
; created by Midi2Cs 0.94
; Sun Feb  9 16:56:02 1997

; qticks: 192
; Inizio dell'"Allemanda" della Prima Partita in si minore per violino di J.S.Bach
; key signature sf=0 mi=2
;	(sf: -7=7flats  -1=1flat  0=key of C  1=1sharp  7=7sharps
;	mi: 0=major key  1=minor key)
; time: 04/04 beat
; time: 24 clocks per quarter note
; time: 08 notated 32-nd notes in a MIDI quarter-note
; tempo
t 0 120.0000 0.00 46.0000 
;--------------------------------------------
;      miditrack 1     instrument 1
;--------------------------------------------
; p1 = instr
; p2 = start
; p3 = duration
; p4 = midivelocity
; p5 = cps
; cpstable midi2cs.cps
i01    0.00000   0.2500  64  1479.97769 ; F#4 ch01 
i01    0.25000   0.7500  64  1479.97769 ; F#4 ch01 
i01    0.25000   0.7500  64  1174.65907 ;  D4 ch01 
i01    1.00000   0.2500  64  1975.53321 ;  B4 ch01 
i01    1.25000   0.7500  64  1567.98174 ;  G4 ch01 
i01    2.00000   0.1250  64  1479.97769 ; F#4 ch01 
i01    2.12500   0.1250  64  1567.98174 ;  G4 ch01 
i01    2.25000   0.7500  64  1479.97769 ; F#4 ch01 
i01    2.25000   0.7500  64  1318.51023 ;  E4 ch01 
i01    3.00000   0.1250  64  1174.65907 ;  D4 ch01 
i01    3.12500   0.1250  64  1108.73052 ; C#4 ch01 
i01    3.25000   0.3750  64  1174.65907 ;  D4 ch01 
i01    3.62500   0.1250  64   493.88330 ;  B2 ch01 
i01    3.75000   0.3750  64   440.00000 ;  A2 ch01 
; bar 1
i01    4.12500   0.1250  64  1479.97769 ; F#4 ch01 
i01    4.25000   0.7500  64  1479.97769 ; F#4 ch01 
i01    5.00000   0.1250  64  1567.98174 ;  G4 ch01 
i01    5.12500   0.1250  64  1479.97769 ; F#4 ch01 
i01    5.25000   0.3750  64  1318.51023 ;  E4 ch01 
i01    5.62500   0.1250  64  1174.65907 ;  D4 ch01 
i01    5.75000   0.3750  64  1108.73052 ; C#4 ch01 
i01    6.12500   0.1250  64   987.76660 ;  B3 ch01 
i01    6.25000   0.3750  64   932.32752 ; A#3 ch01 
i01    6.62500   0.1250  64   783.99087 ;  G3 ch01 
i01    6.75000   0.3750  64   739.98885 ; F#3 ch01 
i01    7.12500   0.1250  64   659.25511 ;  E3 ch01 
i01    7.25000   0.3750  64   587.32954 ;  D3 ch01 
i01    7.62500   0.1250  64   987.76660 ;  B3 ch01 
i01    7.75000   0.3750  64   554.36526 ; C#3 ch01 
; bar 2
i01    8.12500   0.1250  64  1108.73052 ; C#4 ch01 

; noteoff =    30   noteon  =    30   polyaft =     0
; control =     0   program =     0   chnpres =     0
; pchbend =     0   systems =     7   defs    =     0

; end of track


;--------------------------------------------
;      miditrack 2     instrument 16
;--------------------------------------------
; p1 = instr
; p2 = start
; p3 = duration
; p4 = midivelocity
; p5 = cps
; cpstable midi2cs.cps
i16    0.25000   0.7500  64   739.98885 ; F#3 ch01 
i16    0.25000   0.7500  64   493.88330 ;  B2 ch01 
i16    1.25000   0.7500  64  1318.51023 ;  E4 ch01 
i16    2.25000   0.7500  64   739.98885 ; F#3 ch01 
i16    2.25000   0.7500  64   466.16376 ; A#2 ch01 
; bar 1
i16    4.25000   0.7500  64   987.76660 ;  B3 ch01 
i16    4.25000   0.7500  64   587.32954 ;  D3 ch01 
i16    4.25000   0.7500  64   391.99544 ;  G2 ch01 

; noteoff =     8   noteon  =     8   polyaft =     0
; control =     0   program =     0   chnpres =     0
; pchbend =     0   systems =     1   defs    =     0

; end of track

; conversion took 0 seconds

; end of score
e
