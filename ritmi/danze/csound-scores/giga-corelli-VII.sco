f1 0 2048 10 1.0000 0.0313 0.0010 
f2 0 2048 10 1.0000 0.0333 0.0011 
f3 0 2048 10 1.0000 0.0357 0.0013 
f4 0 2048 10 1.0000 0.0385 0.0015 
f5 0 2048 10 1.0000 0.0417 0.0017 
f6 0 2048 10 1.0000 0.0455 0.0021 
f7 0 2048 10 1.0000 0.0500 0.0025 0.0001 
f8 0 2048 10 1.0000 0.0556 0.0031 0.0002 
f9 0 2048 10 1.0000 0.0625 0.0039 0.0002 
f10 0 2048 10 1.0000 0.0714 0.0051 0.0004 
f11 0 2048 10 1.0000 0.0833 0.0069 0.0006 
f12 0 2048 10 1.0000 0.1000 0.0100 0.0010 0.0001 
f13 0 2048 10 1.0000 0.1250 0.0156 0.0020 0.0002 
f14 0 2048 10 1.0000 0.1667 0.0278 0.0046 0.0008 0.0001 
f15 0 2048 10 1.0000 0.2500 0.0625 0.0156 0.0039 0.0010 0.0002 
f16 0 2048 10 1.0000 0.5000 0.2500 0.1250 0.0625 0.0313 0.0156 0.0078 0.0039 0.0020 0.0010 0.0005 0.0002 0.0001 
; /U/USER/NICB/ME/MUSIC/INTERMEZZO-I/RITMI/DANZE/CSOUND-SCORES/GIGA-CORELLI-VII.MIDI - Csound score
; created by Midi2Cs 0.94
; Sun Feb  9 19:54:38 1997

; qticks: 192
; giga dalla Sonata VII per violino e basso continuo dall'op.5 di A.Corelli
; key signature sf=0 mi=255
;	(sf: -7=7flats  -1=1flat  0=key of C  1=1sharp  7=7sharps
;	mi: 0=major key  1=minor key)
; time: 06/09 beat
; time: 12 clocks per quarter note
; time: 08 notated 32-nd notes in a MIDI quarter-note
; tempo
t 0 120.0000 0.00 144.0002 
;--------------------------------------------
;      miditrack 1     instrument 1
;--------------------------------------------
; p1 = instr
; p2 = start
; p3 = duration
; p4 = midivelocity
; p5 = cps
; cpstable midi2cs.cps
i01    1.00000   0.5000  64  1174.65907 ;  D4 ch01 
i01    1.50000   0.5000  64  1108.73052 ; C#4 ch01 
i01    2.00000   0.5000  64   880.00000 ;  A3 ch01 
i01    2.50000   0.5000  64  1108.73052 ; C#4 ch01 
i01    3.00000   0.5000  64  1174.65907 ;  D4 ch01 
i01    3.50000   0.5000  64   880.00000 ;  A3 ch01 
i01    4.00000   0.5000  64  1174.65907 ;  D4 ch01 
i01    4.50000   0.5000  64  1108.73052 ; C#4 ch01 
i01    5.00000   0.5000  64   880.00000 ;  A3 ch01 
i01    5.50000   0.5000  64  1108.73052 ; C#4 ch01 
; bar 1
i01    6.00000   1.0000  64  1174.65907 ;  D4 ch01 

; noteoff =    11   noteon  =    11   polyaft =     0
; control =     0   program =     0   chnpres =     0
; pchbend =     0   systems =     7   defs    =     0

; end of track


;--------------------------------------------
;      miditrack 2     instrument 16
;--------------------------------------------
; p1 = instr
; p2 = start
; p3 = duration
; p4 = midivelocity
; p5 = cps
; cpstable midi2cs.cps

; noteoff =     0   noteon  =     0   polyaft =     0
; control =     0   program =     0   chnpres =     0
; pchbend =     0   systems =     1   defs    =     0

; end of track

; conversion took 0 seconds

; end of score
e
