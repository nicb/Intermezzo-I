f1 0 2048 10 1.0000 0.0313 0.0010 
f2 0 2048 10 1.0000 0.0333 0.0011 
f3 0 2048 10 1.0000 0.0357 0.0013 
f4 0 2048 10 1.0000 0.0385 0.0015 
f5 0 2048 10 1.0000 0.0417 0.0017 
f6 0 2048 10 1.0000 0.0455 0.0021 
f7 0 2048 10 1.0000 0.0500 0.0025 0.0001 
f8 0 2048 10 1.0000 0.0556 0.0031 0.0002 
f9 0 2048 10 1.0000 0.0625 0.0039 0.0002 
f10 0 2048 10 1.0000 0.0714 0.0051 0.0004 
f11 0 2048 10 1.0000 0.0833 0.0069 0.0006 
f12 0 2048 10 1.0000 0.1000 0.0100 0.0010 0.0001 
f13 0 2048 10 1.0000 0.1250 0.0156 0.0020 0.0002 
f14 0 2048 10 1.0000 0.1667 0.0278 0.0046 0.0008 0.0001 
f15 0 2048 10 1.0000 0.2500 0.0625 0.0156 0.0039 0.0010 0.0002 
f16 0 2048 10 1.0000 0.5000 0.2500 0.1250 0.0625 0.0313 0.0156 0.0078 0.0039 0.0020 0.0010 0.0005 0.0002 0.0001 
; /U/USER/NICB/ME/MUSIC/INTERMEZZO-I/RITMI/DANZE/CSOUND-SCORES/CHACONNE-J.S.BACH.MIDI - Csound score
; created by Midi2Cs 0.94
; Tue Feb 11 22:45:56 1997

; qticks: 192
; J.S.Bach "Chaconne" dalla Partita n.2 in re minore
; key signature sf=0 mi=255
;	(sf: -7=7flats  -1=1flat  0=key of C  1=1sharp  7=7sharps
;	mi: 0=major key  1=minor key)
; time: 03/04 beat
; time: 24 clocks per quarter note
; time: 08 notated 32-nd notes in a MIDI quarter-note
; tempo
t 0 120.0000 0.00 72.0000 
;--------------------------------------------
;      miditrack 1     instrument 1
;--------------------------------------------
; p1 = instr
; p2 = start
; p3 = duration
; p4 = midivelocity
; p5 = cps
; cpstable midi2cs.cps
i01    0.00000   1.5000 105   880.00000 ;  A3 ch01 
i01    0.00000   1.5000 105   698.45646 ;  F3 ch01 
i01    1.50000   0.5000 105   880.00000 ;  A3 ch01 
i01    2.00000   1.0000 105  1318.51023 ;  E4 ch01 
i01    2.00000   1.0000 105   932.32752 ; A#3 ch01 
; bar 1
i01    3.00000   1.5000 105  1318.51023 ;  E4 ch01 
i01    3.00000   1.5000 105   880.00000 ;  A3 ch01 
i01    4.50000   0.5000 105  1318.51023 ;  E4 ch01 
i01    5.00000   1.0000 105  1396.91293 ;  F4 ch01 
i01    5.00000   1.0000 105   880.00000 ;  A3 ch01 
; bar 2
i01    6.00000   1.5000 105  1174.65907 ;  D4 ch01 
i01    6.00000   1.5000 105   698.45646 ;  F3 ch01 
i01    7.50000   0.5000 105   523.25113 ;  C3 ch01 
i01    8.00000   1.0000 105   932.32752 ; A#3 ch01 
i01    8.00000   1.0000 105   783.99087 ;  G3 ch01 
; bar 3
i01    9.00000   1.0000 105   880.00000 ;  A3 ch01 
i01    9.00000   1.0000 105   698.45646 ;  F3 ch01 
i01   10.00000   0.2500 105   783.99087 ;  G3 ch01 
i01   10.25000   0.2500 105   698.45646 ;  F3 ch01 
i01   10.50000   0.2500 105   659.25511 ;  E3 ch01 
i01   10.75000   0.2500 105   698.45646 ;  F3 ch01 
i01   11.00000   0.2500 105   783.99087 ;  G3 ch01 
i01   11.25000   0.2500 105   659.25511 ;  E3 ch01 
i01   11.50000   0.2500 105   698.45646 ;  F3 ch01 
i01   11.75000   0.2500 105   587.32954 ;  D3 ch01 
; bar 4
i01   12.00000   1.5000 105   880.00000 ;  A3 ch01 
i01   12.00000   1.5000 105   698.45646 ;  F3 ch01 
i01   13.50000   0.5000 105   880.00000 ;  A3 ch01 
i01   14.00000   1.0000 105  1318.51023 ;  E4 ch01 
i01   14.00000   1.0000 105   932.32752 ; A#3 ch01 
; bar 5
i01   15.00000   1.5000 105  1318.51023 ;  E4 ch01 
i01   15.00000   1.5000 105   880.00000 ;  A3 ch01 
i01   16.50000   0.5000 105  1318.51023 ;  E4 ch01 

; noteoff =    33   noteon  =    33   polyaft =     0
; control =     0   program =     0   chnpres =     0
; pchbend =     0   systems =     7   defs    =     0

; end of track


;--------------------------------------------
;      miditrack 2     instrument 16
;--------------------------------------------
; p1 = instr
; p2 = start
; p3 = duration
; p4 = midivelocity
; p5 = cps
; cpstable midi2cs.cps
i16    0.00000   0.5000  64   587.32954 ;  D3 ch01 
i16    2.00000   0.5000  64   783.99087 ;  G3 ch01 
i16    2.00000   0.5000  64   587.32954 ;  D3 ch01 
; bar 1
i16    3.00000   0.5000  64   783.99087 ;  G3 ch01 
i16    3.00000   0.5000  64   554.36526 ; C#3 ch01 
i16    5.00000   0.5000  64   698.45646 ;  F3 ch01 
i16    5.00000   0.5000  64   587.32954 ;  D3 ch01 
; bar 2
i16    6.00000   0.5000  64   466.16376 ; A#2 ch01 
i16    8.00000   0.5000  64   391.99544 ;  G2 ch01 
; bar 3
i16    9.00000   0.5000  64   440.00000 ;  A2 ch01 
i16   10.00000   0.2500  64   554.36526 ; C#3 ch01 
i16   11.00000   0.2500  64   587.32954 ;  D3 ch01 
; bar 4
i16   12.00000   0.5000  64   587.32954 ;  D3 ch01 
i16   14.00000   0.5000  64   783.99087 ;  G3 ch01 
i16   14.00000   0.5000  64   587.32954 ;  D3 ch01 
; bar 5
i16   15.00000   0.5000  64   783.99087 ;  G3 ch01 
i16   15.00000   0.5000  64   554.36526 ; C#3 ch01 

; noteoff =    17   noteon  =    17   polyaft =     0
; control =     0   program =     0   chnpres =     0
; pchbend =     0   systems =     1   defs    =     0

; end of track

; conversion took 0 seconds

; end of score
e
