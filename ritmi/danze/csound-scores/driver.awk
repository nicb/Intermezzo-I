BEGIN {
	MAX_INSTRUMENTS = 32;
	NUM_HARMONICS = 20;
	MIN_AMPLITUDE = 0.0001;
	ntracks = 16;

	create_csound_tables(ntracks);
}
function create_csound_tables(ntracks,
	i, j, base_decr, base, val)
{
	i = 0; j = 0;
	base_decr = MAX_INSTRUMENTS/ntracks;
	base = (MAX_INSTRUMENTS-base_decr)+2;

	for (i = 1; i <= ntracks; ++i)
	{
		printf("f%d 0 2048 10 ", i);
		for (j = 0; j < NUM_HARMONICS; ++j)
		{
			val = base**(-j);
			if (val >= MIN_AMPLITUDE)
				printf("%6.4f ", val);
			else
				break;
		}
		printf("\n");
		base -= base_decr;
	}
}
