#
# $Id: mkwindow-E.awk,v 0.1 1997/09/19 06:34:34 nicb Exp $
#
# mkwindow-E.awk: this is a preprocessor that builds a window between
#	two adjacent slices L-He and, also,
#	the closest slice to a selected golden section point and
#	the golden section itself; its output replaces both the slices
#	and the golden section with window data to be processed further
#
# PLEASE NOTE: the important thing is that slice and golden section data
#	come ALL before the note data
#
function delete_array(bf,
	i)
{
	for (i = 1; i <= bf[0]; ++i)
		delete bf[i];

	delete bf[0];
}
function abs(num)
{
	return num < 0 ? -num : num;
}
#
#
#
function save_slice(slice_string,
	idx)
{
	idx = slice[0];
	slice[++idx] = slice_string;
	slice[0] = idx;
}
function save_gdsect(gdsect_string,
	idx)
{
	idx = gdsect[0];
	gdsect[++idx] = gdsect_string;
	gdsect[0] = idx;
}
function get_field(string,field)
{
	temp[0] = split(string, temp, "|");
	result = temp[field];
	delete_array(temp);
	return result;
}
function get_action_time(string,
	result)
{
	return get_field(string,2);
}
function get_info(string,
	result)
{
	return get_field(string,3);
}
function find_closest_slice(gds, sa,
	i, gdsat, saat, closest, cmp, result, second_result, second_closest)
{
	closest=100;
	gdsat = get_action_time(gds);

	for (i = 1; i <= sa[0]; ++i)
	{
		saat = get_action_time(sa[i]);
		cmp = abs(gdsat-saat);
#print "*** sa[" i "]=" sa[i] ", saat=" saat ", cmp=" cmp ", closest=" closest ", result=" result ", second result=" second_result;
		if (cmp < closest)
		{
			second_closest=closest;
			closest = cmp;
			second_result=result;
			result = i;
		}
		else if (cmp < second_closest)
		{
			second_closest=cmp;
			second_result=i;
		}
	}
	result = slice_not_valid(sa[result]) ? second_result : result;
#print "*** returning result " result;
	return result;
}
function erase_slice(slice)
{
	return sprintf("# (REMOVED) %s", slice);
}
function build_level(gd,s,
	gdi, si)
{
	return sprintf("%d%c", get_info(gd), get_info(s));
}
function output_window(start,end,level)
{
	return sprintf("AAAwindow|%8.5f|%8.5f|%s",start,end,level);
}
function build_window(gdsect,slice,winarray,level,
	gdsat,saat,winidx,start,end)
{
	winidx = winarray[0];
	gdsat = get_action_time(gdsect);
	saat = get_action_time(slice);
	if (gdsat > saat)
	{
		start = saat;
		end = gdsat;
	}
	else
	{
		start = gdsat;
		end = saat;
	}
	winarray[++winidx]=output_window(start,end,level);
	winarray[0] = winidx;
}
function slice_not_valid(slice)
{
	return index(slice, "REMOVED");
}
function build_gdsect_windows(sa, gda, wa,
	i, saidx, level)
{
	for (i = 1; i <= gda[0]; ++i)
	{
		saidx = find_closest_slice(gda[i],sa);
#print "*** saidx=" saidx ", sa[saidx]=" sa[saidx];
		if (!slice_not_valid(sa[saidx]))
		{
			level = build_level(gda[i],sa[saidx]);
			build_window(gda[i],sa[saidx],wa,level);
			sa[saidx] = erase_slice(sa[saidx]);
		}
	}
}
function build_slice_windows(sa, wa,
	i, saidx, acct, level)
{
	for (i = 1; i <= sa[0]; ++i)
	{
		acct = get_info(sa[i]);
		if (acct == "L")
		{
			saidx = i+1;
			level = "3L";
			build_window(sa[i],sa[saidx],wa,level);
			sa[i] = erase_slice(sa[i]);
			sa[saidx] = erase_slice(sa[saidx]);
		}
	}
}
function output_window_data(\
	temp_data, i)
{
	temp_data[0] = 0;
	build_slice_windows(slice, temp_data);
	build_gdsect_windows(slice, gdsect, temp_data);
	for (i = 1; i <= slice[0]; ++i)
		print slice[i];
	for (i = 1; i <= temp_data[0]; ++i)
		print temp_data[i];

	delete_array(temp_data);
}
BEGIN \
{
	FS="|";
	OFS="|";
	doneflag = 0;
	slice[0] = 0;
	gdsect[0] = 0;
	save_slice("AAAslice|0|H");
}
/^#/ \
{
	# these are comments and are passed on
	print
	next;
}
/^AAAslice/ \
{
	save_slice($0);
	next;
}
/^AAAgdsect/ \
{
	save_gdsect($0);
	next;
}
{
	if (!doneflag)
	{
		output_window_data();
		doneflag = 1;
	}
	print;
}
#
# $Log: mkwindow-E.awk,v $
# Revision 0.1  1997/09/19 06:34:34  nicb
# added second chance capability for golden section windows
#
# Revision 0.0  1997/09/18 06:27:47  nicb
# Initial Revision (incomplete)
#
# Revision 0.0  1997/08/16 12:34:49  nicb
# Initial Revision
#
