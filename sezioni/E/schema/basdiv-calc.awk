#
# $Id: basdiv-calc.awk,v 0.1 1997/09/17 10:03:44 nicb Exp nicb $
#
# Intermezzo I: E section basic division calculator
#
function output(acct_string, t, stringstart, stringend,
	mht)
{
	if (acct_string == "L")
		mht = "mhtl";
	else if (acct_string == "He")
		mht = "mhtm";
	else
		mht = "mhth";

	printf("\"%s\" %10s %10s %8.5f rshortlow units %s\n", acct_string,
		stringstart, stringend, t, mht);
}
function produce_output(div, acct, x0,x1,y0,y1,a,b,
	tempt, tempa, i, t, delta_t, n, ss, se, next_t, this_acct)
{
	tempt[0] = split(div,tempt, "|");
	tempa[0] = split(acct,tempa, "|");
	t = next_t = x0;
	T = y0;
	n = 2;
	ss = "Start";
	delta_t = (60*tempt[1])/exp((a*next_t)+b);
	this_acct = tempa[1];
	next_t += delta_t;
	i = 2;

	while (next_t <= x1)
	{
		for (; i <= tempt[0]; ++i)
		{
			this_acct = tempa[i];
			t = next_t;
			delta_t = (60*tempt[i])/exp((a*next_t)+b);
			next_t += delta_t;
			se = sprintf("Start_Met%d", n);
			output(this_acct, t, ss, se);
			ss = se;
			++n;
			if (next_t >= x1)
				break;
		}
		i = 1;
	}
}
BEGIN \
{
	E_bdiv = "1.5|.5|1";
	E_acct = "H|L|He";
	x0=0;
	x1=71;
	y1= y1 == "" ? 81.932999  : y1;
	y0=10.28571;
	bnat=log(y0);
	anat=(log(y1)-bnat)/x1;

	produce_output(E_bdiv, E_acct, x0,x1,y0,y1,anat,bnat);
}
#
# $Log: basdiv-calc.awk,v $
# Revision 0.1  1997/09/17 10:03:44  nicb
# functional version, in conjunction with E.pic 0.0
#
# Revision 0.0  1997/09/15 17:02:38  nicb
# Initial Revision (output not correct)
#
