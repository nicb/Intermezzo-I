#
#
x0=0
x1=70
y1=81.932999 
y0=10.28571
bnat=log(y0)
anat=(log(y1)-bnat)/x1

plot [x0-4:x1+4][y0-4:y1+4] exp((anat*x)+bnat), "tempo.out" using 2:3 \
	with impulses
pause -1
