#
# $Id: rnd-select.awk,v 0.1 1997/05/27 19:14:22 nicb Exp $
#
function delete_array(bf,
	i)
{
	for (i = 1; i <= bf[0]; ++i)
		delete bf[i];

	delete bf[0];
}
function selector(time, n,
	result)
{
	result = lastrand+(weights[n]/den);
	result = (result > 0.75) ? 1 : 0;

	return result;
}
function calc_startframe(time,
	result)
{
	result = int(time/period);
	result *= period;

	return result;
}
function calc_endframe(time,
	result)
{
	result = int(time/period)+1;
	result *= period;

	return result;
}
function frame_advance(time)
{
	if (time < startframe || time > endframe)
	{
		startframe = calc_startframe(time);
		endframe = calc_endframe(time);
		lastrand = rand();
	}
}
function print_header(seed, metro, weights)
{
	printf("# $RCSfile: rnd-select.awk,v $ $Revision: 0.1 $\n# (seed=%d, metro=%f, weights=\"%s\")\n",
		seed, metro, weights) > "/dev/stderr";
}
BEGIN {
	rndseed = rndseed != "" ? rndseed : 101001;
	srand(rndseed);
	weight_string = weight_string != "" ? weight_string :\
					"3:4:3:2:1:2:3:4:5";
	metro = metro != "" ? metro : 51;
	period = 1/metro;
	startframe = 0;
	endframe = period;
	lastrand = 0;

	weights[0] = split(weight_string,weights,":");
	den = 10;
	print_header(rndseed,metro,weight_string);
}
{
	frame_advance($1);
	if (selector($1, $2))
		print;
}
#
# $Log: rnd-select.awk,v $
# Revision 0.1  1997/05/27 19:14:22  nicb
# corrected header small bug
#
# Revision 0.0  1997/04/20 18:18:57  nicb
# Initial Revision
#
