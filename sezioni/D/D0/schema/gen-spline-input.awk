#
# $Id$
#
# This is to give the points to create a spline as follows:
#
#  |            2            3
#  |            o------------o
#  |           /              \
#  |          /                \
#  |         /                  \
#  |        /                    \
#  |       /                      \
# -o------o------------------------o---------o-->
#  0      1                        4         5
#  |
#
#
#
#
#
function flat_output(x0,x1,y,
	x, delta)
{
	x=x0;
	delta=1
	
	while (x < x1)
	{
		printf("%7.4f %7.4f\n", x, y);
		x += delta;
	}
}
function output(x, y)
{
	printf("%7.4f %7.4f\n", x, y);
}
BEGIN {
	phi=(sqrt(5)+1)/2;
	invphi=1/phi;

	prop[0]=total*invphi;		# this is the start descending (3)
	prop[1]=total-prop[0];		# this is the end ascending (2)
	prop[2]=prop[1]*invphi;		# this is the start ascending (1)
	prop[3]=total-prop[2];		# this is the end descending (4)

	p[0] = 0;			# this is p(0)
	p[1] = prop[2];			# this is p(1)
	p[2] = prop[1];			# ecc..
	p[3] = prop[0];
	p[4] = prop[3];
	p[5] = total;

	flat_output(p[0],p[1],bot);
	flat_output(p[2],p[3],top);
	flat_output(p[4],p[5],bot);
}
#
# $Log$
