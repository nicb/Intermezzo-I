#
# $Id$
#
# this transforms, via the MATLAB program cubic.m, the initial data
# for the cubic function in actual constant parameters to be applied
# to it
#
function delete_array(a,
	i)
{
	for (i=a[0]; i>=0; --i)
		delete a[i];
}
function get_params(result, start, end, y0, y1, y2,
	command, halfway)
{
	halfway = ((end-start)/2)+start;
	command = sprintf("echo %f %f %f %f %f %f | octave -q cubic.m",
		start, halfway, end, y0, y1, y2);
	command | getline result_string;
	result[0] = split(result_string,result,"|");
	delete_array(temp);
}
function output(type, start, end, pars)
{
	printf("%8.5f %8.5f %8.5f %8.5f %8.5f %8.5f\n", start, end, pars[1],
		pars[2], pars[3], pars[4]) > extra_file;
}
BEGIN {
	FS="|";
	extra_file = "cubic-input.data";
}
$2 == "lcubeinit" {
	get_params(returned, $3, $4, $5, $6, $7);
	output($1, $3, $4, returned);
	delete_array(returned);
}
#
# $Log$
