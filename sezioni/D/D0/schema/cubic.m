#
# $Id: cubic.m,v 0.1 1997/05/25 17:53:08 nicb Exp $
#
#
# function system to be fit:
#
# y = (a*x**3)+(b*x**2)+(c*x)+d
#
# where a b c and d are the unknown values and the additional condition
# that
#	y[1] = (3*a*x**2)+(2*b*x)+c
#
# we keep y[1] and the odd power of x (by default, 3) in a separate value
# so that they are easily movable up and down
#
1;		# this is not a function file

[ x0, x1, x2, y0, y1, y2 ]=scanf("%f %f %f %f %f %f", "C");
p=3;		# this is the odd power of x
x=[x0,x1,x2];
y=[y0,y1,y2];

sys=[x(1)**p,x(1)**2,x(1),1,y(1);\
     x(2)**p,x(2)**2,x(2),1,y(2);\
     x(3)**p,x(3)**2,x(3),1,y(3);\
     p*(x(2)**(p-1)),2*x(2),1,0,0];

deter=det(sys(:,1:4));
da=det(sys(:,[5,2,3,4]));
db=det(sys(:,[1,5,3,4]));
dc=det(sys(:,[1,2,5,4]));
dd=det(sys(:,[1,2,3,5]));

a=da/deter;
b=db/deter;
c=dc/deter;
d=dd/deter;

#X=[0:0.01:1];
#Y=[(a*(X.**p))+(b*(X.**2))+(c*X)+d];
#X=rot90(X,3);
#Y=rot90(Y,3);
#data=[X,Y];

printf("%f|%f|%f|%f\n", a, b, c, d);

#gplot [-0.5:1.5][-1:15] data with lines
#pause
#
# $Log: cubic.m,v $
# Revision 0.1  1997/05/25 17:53:08  nicb
# added external handling capabilities
#
# Revision 0.0  1997/05/25 10:27:34  nicb
# Initial Revision
#
