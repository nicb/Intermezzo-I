#
# $Id: mupper.awk,v 0.5 1997/06/15 10:48:31 nicb Exp nicb $
#
# This does only a gross transformation of rfit files into mup files;
# these must be refined by hand afterwards
#
function delete_array(a,
	i)
{
	for (i = a[0]; i >= 0; --i)
		delete a[i];
}
function save_note(at, numnote, dyn,
	atidx, dynidx)
{
	atidx = at_array[numnote,0];
	dynidx = dyn_array[numnote,0];

	at_array[numnote, ++atidx] = at;
	dyn_array[numnote, ++dynidx] = dyn;

	at_array[numnote,0] = atidx;
	dyn_array[numnote,0] = dynidx;
}
function clear_arrays(nstaffs,
	i)
{
	for (i = 1; i <= nstaffs; ++i)
	{
		delete_array(at_array[i]);
		delete_array(dyn_array[i]);
		at_array[i,0] = dyn_array[i,0] = 0;	# reset counters
		_last_at_[i] = 0;
	}
}
function get_part(at, n,
	result, temp)
{
	temp[0] = split(at, temp, "/");
	result = temp[n];
	delete_array(temp);

	return result;
}
function try_convert_rhythm(num, div,
	result) 
{
	result = sprintf("%d/%d", num, div);

	if (num == 1)
		result = div;
	else if (result in rhythm_convert)
		result = rhythm_convert[result]; 

	return result;
}
function get_numerator(at)
{
	return get_part(at, 1);
}
function get_divisor(at)
{
	return get_part(at, 2);
}
function get_output_string(n, cur_at,
	result, num_cur_at, div_cur_at, converted)
{
	num_cur_at = get_numerator(cur_at);
	div_cur_at = get_divisor(cur_at);

	if (cur_at == _last_at_[n])
		result = sprintf("%s", notes[n]);
	else
	{
		if (_last_at_[n] == 0)
		{
			if (num_cur_at == 1)
				result = sprintf("%d%s", div_cur_at, notes[n]);
			else
			{
				converted = try_convert_rhythm(num_cur_at-1,
					div_cur_at);
				result = sprintf("%sr; %d%s", converted,
					div_cur_at, notes[n]);
			}
		}
		else
			result = sprintf("%s%s", cur_at, notes[n]);
	}

	_last_at_[n] = cur_at;
	return result;
}
function set_all_notes(n, returned,
	i)
{
	if (at_array[n, 0])
	{
		for (i = 1; i <= at_array[n,0]; ++i)
		{
			returned[i] = get_output_string(n, at_array[n,i]);
			returned[0] = i;
		}
	}
}
function _produce_staff(n, string_array,
	i, result)
{
	result = sprintf("%d: ", n);
	if (at_array[n,0])
	{
		for (i = 1; i <= string_array[0]; ++i)
			result = sprintf("%s%s%s; ", result, 
				string_array[i], _notes_[n]);

	}
	else
		result = sprintf("%smr;", result);

	return result;
}
function produce_staff(n,
	i, returned, string)
{
	returned[0] = 0;
	set_all_notes(n, returned);
	string = _produce_staff(n, returned);

	print string;		# to be substituted by output_staff()

	delete_array(returned);
}
function get_dyn_pos(at,
	result, temp, at_num)
{
	temp[0] = split(at, temp, "/");
	at_num = temp[2] <= bar_div ? temp[1] : temp[1]+1;
	result = (bar_div/temp[2])*at_num;
#print "***** at=" at ", bar_div=" bar_div ", result=" result;
	delete_array(temp);
	return result;
}
function get_str_dyn(dyn,
	idyn)
{
	idyn = int(dyn);
	return dynamics[idyn,0];
}
function get_midi_dyn(dyn,
	idyn)
{
	idyn = int(dyn);
	return dynamics[idyn,1];
}
function output_dynamics(n, at, string, midi)
{
	printf("boldital (12) below %d: %5.3f \"%s\";\n", n, at, string);
	printf("midi %d: %5.3f \"onvelocity=%d\";\n", n, at, midi);
}
function produce_dynamics(n,
	i, str_dyn, midi_dyn)
{
	if (dyn_array[n,0])
	{
		for (i = 1; i <= dyn_array[n,0]; ++i)
		{
			dyn_pos = get_dyn_pos(at_array[n,i]);
			str_dyn = get_str_dyn(dyn_array[n,i]);
			midi_dyn = get_midi_dyn(dyn_array[n,i]);
			output_dynamics(n, dyn_pos, str_dyn, midi_dyn);
		}
	}
}
function produce_bar(nstaffs,
	i)
{
	for (i = 1; i <= nstaffs; ++i)
		produce_staff(i);
	for (i = 1; i <= nstaffs; ++i)
		produce_dynamics(i);

	printf "bar\n// bar n.%d\n", ++_barno_;
}
function print_mup_header(title, bar_num, bar_div, beamstyle, nstaffs, labels, labels2, stafflines, clefs, notes, tempo,
	i, curclef, idstring)
{
	idstring = "%Id%";
	revstring= "%Revision%";
	gsub(/\%/, "$", idstring);
	gsub(/\%/, "$", revstring);
	lab[0] = split(labels, lab, ":");
	lab2[0] = split(labels2, lab2, ":");
	slines[0] = split(stafflines, slines, ":");
	clfs[0] = split(clefs, clfs, ":");

	print "// Produced by $RCSfile: mupper.awk,v $ $Revision: 0.5 $";
	print "// (to be edited by hand afterwards)";
	printf "// %s\nheader\ntitle \"%s\"\n", idstring, title;
	printf "header2\ntitle \"\\s(-2)%s\\s(+2) - \\%% - \\s(-3)%s\\s(+3)\"\n",
		title, revstring;
	printf "score\ntime=%d/%d; measnum=y; dist=4\n", bar_num, bar_div;
	printf "beamstyle=%s\n", beamstyle;
	printf "staffsep=12\nstaffs=%d\n", nstaffs;
	printf "bracket=1-%d\nbarstyle=1-%d\n", nstaffs, nstaffs;
	for (i = 1; i <= lab[0]; ++i)
	{
		curclef = clfs[i] != "" ? sprintf("; clef=%s\n", clfs[i]) :\
			sprintf("\n");
		printf "staff %d\nlabel=\"%s\"; label2=\"%s\"; stafflines=%s%s",
			i, lab[i], lab2[i], slines[i], curclef;
	}

	print "music"
	printf "midi all: 0 \"tempo=%7.4f\";\n", tempo;

	_notes_[0] = split(notes,_notes_,":");	# initialization

	delete_array(lab);
	delete_array(lab2);
	delete_array(slines);
	delete_array(clfs);
}
function print_mup_trailer(\
	logstring)
{
	logstring = "%Log%";
	gsub(/\%/, "$", logstring);

	printf("// %s\n", logstring);
}
BEGIN \
{
	_barno_ = 1;
	title = title == "" ? "Section D1" : title;
	bar_num = bar_num == "" ? 4 : bar_num;
	bar_div = bar_div == "" ? 4 : bar_div;
	beamstyle = beamstyle == "" ? "4,4,4,4" : beamstyle;
	nstaffs = nstaffs == "" ? 10 : nstaffs;
	labels = labels == "" ? \
		"sonagli:roto-toms:cassa:snare:ride:tam-tam:t.blocks:timp.:crash:marimba" : labels;
	labels2 = labels2 == "" ? \
		"son.:rt.:gc.:sn.:ride:tt:t.b.:timp.:crash:mar." : labels2;
	stafflines = stafflines == "" ? \
		"1n:5n:1n:1n:1n:1n:5n:5:1n:5" : stafflines;
	clefs = clefs == "" ? \
		":::::::bass::treble" : clefs;
	notes = notes == "" ? "c:c:c:c:c:c:c+:g-:c:c#+" : notes;
	tempo = tempo == "" ? 156 : tempo;	# 4/4 tempo mark

	dynamics[60,0] = "pp"; dynamics[60,1] = 16;
	dynamics[66,0] = "p"; dynamics[66,1] = 32;
	dynamics[69,0] = "mp"; dynamics[69,1] = 64;
	dynamics[72,0] = "f"; dynamics[72,1] = 80
	dynamics[75,0] = "ff"; dynamics[75,1] = 100;
	dynamics[78,0] = "fff"; dynamics[78,1] = 127;

	rhythm_convert["3/32"] = "16.";
	rhythm_convert["3/16"] = "8.";
	rhythm_convert["5/16"] = "4r; 16";
	rhythm_convert["7/16"] = "4.r; 16";
	rhythm_convert["11/16"] = "4.r; 4r; 16";
	rhythm_convert["3/8"] = "4.";
	rhythm_convert["4/8"] = "4.r; 8";
	rhythm_convert["3/4"] = "2.";

	print_mup_header(title, bar_num, bar_div, beamstyle, nstaffs, labels,
		labels2, stafflines, clefs, notes, tempo);
}
END \
{
	print_mup_trailer();
}
#
# comment
#
/^#/ \
{
	gsub(/^#/, "//");
	print;
	next;
}
#
# bar divisor
#
/^===/ \
{
	produce_bar(nstaffs);		# nstaffs is global
	clear_arrays(nstaffs);
	next;
}
#
# anything else is a note line (ordered in time)
#
{
	save_note($1, $5, $4);
}
#
# $Log: mupper.awk,v $
# Revision 0.5  1997/06/15 10:48:31  nicb
# added a few more rhythms into the rhythm_convert database
# (to be completely overhauled)
#
# Revision 0.4  1997/06/14 17:20:26  nicb
# added header 2 messages and titles
#
# Revision 0.3  1997/06/13 11:38:27  nicb
# added more header-type stuff
# better placing of dynamics
#
# Revision 0.2  1997/06/12 02:17:54  nicb
# added bar number
#
# Revision 0.1  1997/06/11 11:03:41  nicb
# first functional attempt; not really debugged;
#
# Revision 0.0  1997/06/11 10:26:31  nicb
# Initial Revision
# still does not do a good job
#
