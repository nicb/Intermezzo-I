#
# $Id$
#
# dur.awk: this program is supposed to pickup a file produced with
# rfit and calculate duration by considering the time lag (expressed
# in rational form) from one note to the other.
#
# There are several possible situations:
#
# 1) action times differ: ex.
#
#	at1 note1 dur1
#	at2 note2 dur2
#	  .... etc.
#
#    dur1 is calculated as at2-at1
#
# 2) action times are identical (chords): ex.
#
#	at1 note1 dur1
#	at1 note2 dur1
#	at2 note3 dur2
#	  .... etc.
#
#    in this case, both durations of note1 and note2 are calculated as
#    at2-at1
#
function delete_array(a,
	i)
{
	for (i = a[0]; i >= 0; --i)
		delete a[i];
}
function abs(num)
{
	return num < 0 ? -num : num;
}
function save_note(at, note, dyn, instr,
	idx)
{
	idx = _last_[0];
	_last_[++idx] = sprintf("%s:%10.8f:%10.8f:%d", at, note, dyn,
		instr);
	_last_[0] = idx;
}
function reset_save_array()
{
	delete_array(_last_);
	_last_[0] = 0;
}
function pop_value(i,
	temp, idx, result)
{
	idx = _last_[0];
	temp[0] = split(_last_[idx],temp,":");
	result = temp[i];
	delete_array(temp);
	return result;
}
function pop_at()
{
	return pop_value(1);
}
function rational_value(frac,
	result, temp, num)
{
	temp[0] = split(frac,temp,"/");
	num = temp[1]-1;
	result = num/temp[2];
	delete_array(temp);

	return result;
}
function at_differs(at,
	result, , r_at_last, r_at, eps)
{
	eps = 0.0001;
	r_at_last = rational_value(pop_at());
	r_at = rational_value(at);

	return (abs(r_at-r_at_last) < eps);
}
function get_mcd(d1, d2,
	result, a, b)
{
	if (d1 > d2)
	{
		a = d1;
		b = d2;
	}
	else
	{
		a = d2;
		b = d1;
	}

	if (!(a % b))
}
function subtract(num1,div1, num2,div2,
	mcd, mul1, mul2, nm1, nm2)
{
	mcd = get_mcd(div1, div2);
	mul1 = mcd/div1;
	mul2 = mcd/div2;
	nm1 = (num1-1)*mul1;
	nm2 = (num2-1)*mul2;

	return reduce(nm2-nm1,mcd);
}
function calc_dur(at,
	result, at_temp, l_at_temp)
{
	at_temp[0] = split(at,at_temp,"/");
	l_at_temp[0] = split(pop_at(),l_at_temp,"/");

	result = subtract(at_temp[1],at_temp[2],l_at_temp[1],l_at_temp[2]);

	delete_array(at_temp);
	delete_array(l_at_temp);
	return result;
}
function end_calc(\
	dur)
{
	if (_last_[0])
	{
		dur = calc_end_dur();
		output_last_note(dur);
		reset_save_array();
	}
}
function calc(at, note, dyn, instr,
	dur)
{
	if (_last_[0])
	{
		if (at_differs(at))
		{
			dur = calc_dur(at);
			output_last_note(dur);
			reset_save_array();
		}
	}
	save_note(at, note, dyn, instr);
}
BEGIN \
{
	_last_[0] = 0;
	bar_num = bar_num == "" ? 4 : bar_num;
	bar_div = bar_div == "" ? 4 : bar_div;
	
}
/^#/ \
{
	print;
	next;
}
/^===/ \
{
	end_calc();
	next;
}
{
	calc($1, $2, $4, $5);
	next;
}
# $Log$
