#
# $Id$
#
# pause.awk: sets up specific lenght pauses over on specific points.
# 	     input comes from the reiterator.awk program
#
#
function init(pstring)
{
	pauses[0] = split(pstring, pauses, ":");
}
function should_play(at,
	result, i, plen)
{
	result = 1;
	plen=pauselen/2;

	for (i = 1; i <= pauses[0]; ++i)
		if (at >= (pauses[i]-plen) && at <= (pauses[i]+plen))
			result = 0;

	return result;
}
function output(at, note, dyn, info)
{
	if (should_play(at))
		print at,note,dyn,info;
}
BEGIN {
	phi = 2/(sqrt(5) + 1);
	totdur = totdur == "" ? 43 : totdur;
	pauselen= pauselen == "" ? 1.490937 : pauselen;
	pauses_string= pauses_string == "" ? "26.737621:16.524758" :\
		pauses_string;

	init(pauses_string);
}
{
	output($1, $2, $3, $4);
}
#
# $Log$
