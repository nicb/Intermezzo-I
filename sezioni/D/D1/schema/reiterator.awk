#
# reiterator.awk,v 0.3 1997/06/04 17:58:21 nicb Exp
#
# use in pipe to awk -f window.awk ../../A0/schema/A0-selected.data |\
#	awk -f reducer.awk | awk -f reiterator.awk [ -v start_string="m:n:..." ]
#
function round(num,
	result)
{
	result = int(num < 0 ? num-0.5 : num+0.5);
	return result;
}
function first_at(at, type, modulo,
	result)
{
	result = at;
	type = round(type);

	if (type <= start_mod[0])
		result += modulo * start_mod[type];	
#print "**** " type ", start[" type "]=" start_mod[type] ", modulo=" modulo ", result=" result;

	return result;
}
function output(at, notenum, dyn, type, modulo, off,
	now, noff)
{
	now = first_at(at, notenum, modulo);
	noff = now+off;
	while (noff < totdur)
	{
		printf("%6.4f %3.1f %d %s\n", noff, notenum, dyn, type);
		now += modulo;
		noff = now+off;
	}
}
function init(ss)
{
	start_mod[0] = split(ss, start_mod, ":");
}
BEGIN {
	FS="|";
	if (totdur == "")
		totdur = 27;
	start_string = start_string == "" ? "0" : start_string;
	offset = offset == "" ? 0 : offset;
	init(start_string);
}
{
	output($1,$2,$3,$4,$5,offset);
}
#
# reiterator.awk,v
# Revision 0.3  1997/06/04 17:58:21  nicb
# added offset variable
#
# Revision 0.2  1997/06/03 20:33:57  nicb
# added delayed modulo entrance of elements
#
# Revision 0.1  1997/04/13 15:12:04  nicb
# changed output format to suit rfit software
#
# Revision 0.0  1997/04/13 11:18:34  nicb
# Initial Revision
# (using absolute timings)
#
