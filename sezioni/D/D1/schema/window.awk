#
# window.awk,v 0.4 1997/06/02 19:49:37 nicb Exp
#
# use with ../../A0/schema/A0-selected.data
#
function round(num)
{
	return int(num > 0 ? num+0.5 : num-0.5);
}
function _quantize(num, qt,
	invqt, result)
{
	invqt = 1/qt;
	result = round(num*qt);
	result /= qt;

	return result;
}
function quantize(num, qt, period,
	result)
{
	result = _quantize(num/period,qt)*period;
	return result;
}
function window_min(lev)
{
	return quantize(win_center-((lev*_alpha_)+win_bot_x),_subdiv_,
		_t_period_);
}
function window_max(lev)
{
	return quantize(win_center+((lev*_alpha_)+win_bot_x),_subdiv_,
		_t_period_);
}
function select(at, lev,
	result)
{
	result = (at >= wbot[lev] && at <= wtop[lev]) ? 1 : 0;
		
	return result;
}
function _output(at, lev, dyn, type, rlev)
{
	printf "%7.4f %3.1f %d %s|%7.4f|%7.4f\n", at, lev, dyn, type,
		wbot[lev], wtop[lev];
}
function output(at, lev, dyn, type,
	rlev)
{
	rlev = round(lev);

	if (select(at, rlev))
		_output(at, lev, dyn, type, rlev);
}
BEGIN {
	globdur= globdur == "" ? 43 : globdur;
	phi = 2/(sqrt(5) + 1);
	_subdiv_= _subdiv_ == "" ? 4 : _subdiv_;
	metro= metro == "" ? 51 : metro;
	_t_period_=60/metro;
	win_center=quantize(globdur*phi, _subdiv_,_t_period_);
	top_x = top_x == "" ? 10.150923 : top_x;
	bot_x = bot_x == "" ? 2.396308 : bot_x;
	top_y = top_y == "" ? 9 : top_y;
	bot_y = bot_y == "" ? 1 : bot_y;

	win_top_x=quantize(top_x,_subdiv_,_t_period_)/2;
	win_top_y=top_y-1;
	win_bot_x=quantize(2.396308,_subdiv_,_t_period_)/2;
	win_bot_y=bot_y-1;
	_alpha_=(win_top_x-win_bot_x)/(win_top_y-win_bot_y);

	for (i = win_bot_y; i <= win_top_y; ++i)
		wbot[i+1] = window_min(i);

	for (i = win_bot_y; i <= win_top_y; ++i)
		wtop[i+1] = window_max(i);
}
{
	output($1,$2,$3,$4);
}
# window.awk,v
# Revision 0.4  1997/06/02 19:49:37  nicb
# generalized to use it in other sections and contexts
#
# Revision 0.3  1997/04/13 15:12:04  nicb
# better quantize function
#
# Revision 0.2  1997/04/13 14:26:35  nicb
# added simple quantizer
#
# Revision 0.1  1997/03/31 17:07:04  nicb
# produces window selection with window size
#
# Revision 0.0  1997/03/31 16:26:33  nicb
# Initial Revision; incomplete
#
