#
# $Id: reducer.awk,v 0.0 1997/04/13 11:18:34 nicb Exp $
#
# use in pipe to awk -f window.awk ../../A0/schema/A0-selected.data |\
#	awk -f reducer.awk
#
function delete_array(a,
	i)
{
	for (i = 1; i <= a[0]; ++i)
		delete a[i];

	delete a[0];
}
function output(line, startwindow, endwindow,
	windur, splitted_line, rat)
{
	splitted_line[0] = split(line, splitted_line, " ");
	windur = endwindow-startwindow;
	rat = splitted_line[1] - startwindow;

	printf("%6.4f|%3.1f|%d|%s|%-6.4f\n", rat, splitted_line[2],
		splitted_line[3], splitted_line[4], windur);

	delete_array(splitted_line);
}
BEGIN {
	FS="|";
}
{
	output($1,$2,$3);
}
#
# $Log: reducer.awk,v $
# Revision 0.0  1997/04/13 11:18:34  nicb
# Initial Revision
# (using absolute timings)
#
