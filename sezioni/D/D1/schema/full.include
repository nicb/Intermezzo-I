#
# full.include,v 0.9 1997/08/16 12:55:30 nicb Exp
#
scale=2.54
maxpswid=37.5
maxpsht=27
timescale=maxpswid/maxtime
botht=0
lowline=botht-2
shortlow=lowline/2
rshortlow=lowline/8
ccurx=0
curx=0
mins=0
secs=0
deltaht=0.8
phi=(sqrt(5)+1)/2
time=0
maxht=18
strdistance=0.3				# distance of caption from axes
curtime=0
#
# bottom line
#
Bot: line from 0,botht to maxtime*timescale,botht
define ssect
{
	curtime:=$2+timeoffset
	ccurx:=$2*timescale
	mins:=int(curtime/60)
	secs:=int((((curtime/60)-mins)*60)+0.5)
	$1: line from ccurx,$3 to ccurx,$4
	sprintf("%1.0f'%02.0f\"", mins, secs) at $1.n + (0,1)
	$5 at $1.n + (0,0.45)
}
define topstring
{
	line invis from $1.s.x,maxht to $2.s.x,maxht $3
}
vcurx=0;
vylow=0;
vyhigh=0;
vyline=rshortlow;
define vertstring
{
	vcurx:=($2.s.x-$1.s.x)/2
	vylow:=$4;
	vyhigh:=$5;
	line invis from $1.s.x+vcurx,vyline+vylow \
		to $1.s.x+vcurx,vyline+vyhigh $3 aligned
}
define dline
{
	ccurx:=$2*timescale
	$1: line dashed 0.3 from ccurx,$3 to ccurx,$4
}
define dsect
{
	dline($1,$2,$3,$4)
	curtime:=$2+timeoffset
	mins:=int(curtime/60)
	secs:=((curtime/60)-mins)*60
	sprintf("\s[-1]%1.0f'%04.1f\"\s0", mins, secs) at $1.n + (0,0.45)
}
define bigquote
{
	time:=($2.s.x-$1.s.x)/timescale
	mins:=int(time/60)
	secs:=int((((time/60)-mins)*60)+0.5)

	sprintf("%1.0f'%02.0f\"", mins, secs) at 1/2 of the way \
		between $1.s and $2.s
	arrow from 1/2 of the way between $1.s and $2.s - (1,0) to \
		$1.s
	arrow from 1/2 of the way between $1.s and $2.s + (1,0) to \
		$2.s
}
yplace=-0.2
define smallquote
{
	time:=($2.s.x-$1.s.x)/timescale
	mins:=int(time/60)
	secs:=((time/60)-mins)*60
	yplace:=$3

	line <-> dashed from ($1.s.x,yplace) to ($2.s.x,yplace) \
		sprintf("\s-3%1.0f'%04.1f\"\s0", mins, secs) below;
}
ylowplace=-0.4
define vsmallquote
{
	time:=($2.s.x-$1.s.x)/timescale
	mins:=int(time/60)
	secs:=((time/60)-mins)*60
	yplace:=$3
	ylowplace:=$3-0.1;

	line <-> dashed from ($1.s.x,yplace) to ($2.s.x,yplace);
	vertstring($1,$2,sprintf("\s-5%1.0f'%04.1f\"\s0", mins, secs), ylowplace,yplace);
}
#
# metric division functions
#
numunits=0
units=0
mhth=0
mhtl=0
last_x=0
tunum=0
tudiv=1
subu=0
start=0
tx=0
define takt
{
	tunum:=$3
	tudiv:=$4
	subu:=$6*((4*tunum)/tudiv)
	tx:=last_x+subu;
	dsect($2, tx, $5, $7);
	vsmallquote($1,$2,$5);
	last_x=tx;
}
#
# various beats
#
beatht=0
tperiod=0
chstart=0
chnum=0
chdiv=0
ch_at=0
ch_atpos=0
chwid=0.06
chht=0.25
nbeats=0;
ntimes=0;
choff1=0;
choff2=0;
define c_crosshair
{
	chnum:=$2
	chdiv:=$3

	line from $1-chwid,$4 to $1+chwid,$4
	line from $1,$4-chht to $1,$4+chht
}
define crosshair
{
	ch_at:=chstart;
	ch_atpos:=ch_at*timescale;

	if ch_at >= 0 && ch_at <= maxtime then
	{
		c_crosshair(ch_atpos,$1,$2,$4);
		print "note|" ch_at "|" $4;
	}
	chstart=ch_at+$3*((4*chnum)/chdiv)
}
define crossacct
{
	ch_at:=chstart;
	ch_atpos:=ch_at*timescale;

	if ch_at >= 0 && ch_at <= maxtime then
	{
		c_crosshair(ch_atpos,$1,$2,$4);
		"\(ah" at ch_atpos,$4+chht-0.06
		print "acctnote|" ch_at "|" $4;
	}
	chstart=ch_at+$3*((4*chnum)/chdiv)
}
define qline
{
	ch_at:=chstart;
	ch_atpos:=ch_at*timescale;

	if ch_at >= 0 && ch_at <= maxtime then
	{
		line dashed 0.1 thickness 0.5 from ch_atpos,botht \
			to ch_atpos,botht+10.5;
	}
	chstart=ch_at+($3*((4*$1)/$2));
}
#
# side headers
#
define side_header
{
	sprintf($1, $2) at -strdistance,$3 rjust;
}
#
# allemanda rameau
#
#
# allemanda rf
#
define allemanda_rf_beat
{
	crossacct(3,16,$1,$2);
	crosshair(1,16,$1,$2);
	crossacct(3,16,$1,$2);
	crosshair(1,16,$1,$2);
	crossacct(3,16,$1,$2);
	crosshair(1,16,$1,$2);
	crosshair(3,32,$1,$2);
	crosshair(1,32,$1,$2);
	crosshair(3,32,$1,$2);
	crosshair(1,32,$1,$2);
}
define all_allemanda_rf_beats
{
	beatht:=botht+$1;
	tperiod:=60/$2;
	ntimes:=(maxtime/(tperiod*4))+1;
	chstart=0;

	side_header("allemanda rf (%.1f)", $2, beatht);
	#
	crosshair(1,16,tperiod,beatht); # upbeat
	for i = 1 to ntimes do {
		allemanda_rf_beat(tperiod,beatht);
	}
}
#
# allemanda jsbach
#
define allemanda_jsbach_beat
{
	crossacct(3,16,$1,$2);
	crosshair(1,16,$1,$2);
	crossacct(3,16,$1,$2);
	crosshair(1,32,$1,$2);
	crosshair(1,32,$1,$2);
	crossacct(3,16,$1,$2);
	crosshair(1,32,$1,$2);
	crosshair(1,32,$1,$2);
	crosshair(3,32,$1,$2);
	crosshair(1,32,$1,$2);
	crosshair(3,32,$1,$2);
	crosshair(1,32,$1,$2);
	#
	crossacct(3,16,$1,$2);
	crosshair(1,32,$1,$2);
	crosshair(1,32,$1,$2);
	crosshair(3,32,$1,$2);
	crosshair(1,32,$1,$2);
	crosshair(3,32,$1,$2);
	crosshair(1,32,$1,$2);
	crosshair(3,32,$1,$2);
	crosshair(1,32,$1,$2);
	crosshair(3,32,$1,$2);
	crosshair(1,32,$1,$2);
	crosshair(3,32,$1,$2);
	crosshair(1,32,$1,$2);
	crosshair(3,32,$1,$2);
	crosshair(1,32,$1,$2);
}
define all_allemanda_jsbach_beats
{
	beatht:=botht+$1;
	tperiod:=60/$2;
	ntimes:=(maxtime/(tperiod*8))+1;
	chstart:=0;

	side_header("allemanda j.s.bach (%.1f)", $2, beatht);
	#
	crosshair(1,16,tperiod,beatht); # upbeat
	for i = 1 to ntimes do {
		allemanda_jsbach_beat(tperiod,beatht);
	}
}
#
# allemanda corelli
#
define allemanda_corelli_beat
{
	crossacct(1,8,$1,$2);
	crosshair(1,8,$1,$2-$3);
	crosshair(1,8,$1,$2-$4);
	crosshair(1,8,$1,$2);
	crossacct(1,8,$1,$2);
	crosshair(1,4,$1,$2-$4);
	crosshair(1,8,$1,$2);
}
define all_allemanda_corelli_beats
{
	tperiod:=60/102;
	ntimes:=(maxtime/(tperiod*4))+1;
	chstart:=0;
	choff1:=0.1;
	choff2:=0.2;
	beatht:=botht+$1+choff1;

	side_header("allemanda corelli (%.1f)", $2, beatht);
	#
	crosshair(1,8,tperiod,beatht-choff1); # upbeat
	for i = 1 to ntimes do {
		allemanda_corelli_beat(tperiod,beatht,choff1,choff2);
	}
}
#
# allemanda rameau
#
define allemanda_rameau_beat
{
	crossacct(5,16,$1,$2);
	crosshair(1,16,$1,$2);
	crosshair(1,16,$1,$2);
	crosshair(1,16,$1,$2);
	crossacct(3,16,$1,$2);
	crosshair(1,16,$1,$2);
	crossacct(3,16,$1,$2);
	crosshair(1,16,$1,$2);
	#
	crossacct(3,16,$1,$2);
	crosshair(1,16,$1,$2);
	crosshair(1,16,$1,$2);
	crosshair(1,16,$1,$2);
	crosshair(1,16,$1,$2);
	crosshair(1,16,$1,$2);
	crossacct(1,16,$1,$2);
	crosshair(1,16,$1,$2);
	crosshair(1,8,$1,$2);
	crossacct(1,16,$1,$2);
	crosshair(1,16,$1,$2);
	crosshair(1,8,$1,$2);
}
define all_allemanda_rameau_beats
{
	tperiod:=60/68;
	ntimes:=(maxtime/(tperiod*8))+1;
	chstart:=-0.147058;
	beatht:=botht+$1;

	side_header("allemanda rameau (%.1f)", $2, beatht);
	#
	crosshair(1,8,tperiod,beatht); # upbeat
	for i = 1 to ntimes do {
		allemanda_rameau_beat(tperiod,beatht);
	}
}
#
# corrente zipoli
#
define corrente_zipoli_beat
{
	crossacct(3,8,$1,$2);
	crosshair(1,8,$1,$2);
	crosshair(1,8,$1,$2);
	crosshair(1,8,$1,$2);
	#
	crossacct(1,8,$1,$2);
	crosshair(1,8,$1,$2);
	crosshair(1,8,$1,$2);
	crosshair(1,8,$1,$2);
	crosshair(1,8,$1,$2);
	crosshair(1,8,$1,$2);
}
define all_corrente_zipoli_beats
{
	tperiod:=60/$2;
	ntimes:=(maxtime/(tperiod*6))+1;
	chstart:=0;
	beatht:=botht+$1;

	side_header("corrente zipoli (%.1f)", $2, beatht);
	#
	crosshair(1,8,tperiod,beatht); # upbeat
	for i = 1 to ntimes do {
		corrente_zipoli_beat(tperiod,beatht);
	}
}
#
# sarabanda rf
#
define sarabanda_rf_beat
{
	crosshair(1,4,$1,$2);
	crossacct(3,8,$1,$2);
	crosshair(1,8,$1,$2);
	#
	crossacct(1,2,$1,$2);
	crosshair(1,4,$1,$2);
}
define all_sarabanda_rf_beats
{
	tperiod:=60/$2;
	ntimes:=(maxtime/(tperiod*6))+1;
	chstart:=0;
	beatht:=botht+$1;

	side_header("sarabanda rf (%.1f)", $2, beatht);
	#
	for i = 1 to ntimes do {
		sarabanda_rf_beat(tperiod,beatht);
	}
}
#
# sarabanda corelli
#
define sarabanda_corelli_beat
{
	crosshair(1,4,$1,$2);
	crossacct(3,8,$1,$2);
	crosshair(1,8,$1,$2);
	#
	crossacct(1,2,$1,$2);
	crosshair(1,4,$1,$2);
	#
	crosshair(1,8,$1,$2);
	crosshair(1,8,$1,$2);
	crossacct(3,8,$1,$2);
	crosshair(1,8,$1,$2);
	#
	crossacct(1,4,$1,$2);
	crosshair(1,2,$1,$2);
	#
	crossacct(1,4,$1,$2);
	crosshair(3,8,$1,$2);
	crosshair(1,8,$1,$2);
	#
	crossacct(1,4,$1,$2);
	crosshair(1,4,$1,$2);
	crosshair(1,4,$1,$2);
	#
	crossacct(1,4,$1,$2);
	crosshair(3,8,$1,$2);
	crosshair(1,8,$1,$2);
	#
	crossacct(3,4,$1,$2);
}
define all_sarabanda_corelli_beats
{
	tperiod:=60/$2;
	ntimes:=(maxtime/(tperiod*24))+1;
	chstart:=0;
	beatht:=botht+$1;

	side_header("sarabanda corelli (%.1f)", $2, beatht);
	#
	for i = 1 to ntimes do {
		sarabanda_corelli_beat(tperiod,beatht);
	}
}
#
# giga rf
#
define giga_rf_beat
{
	crossacct(1,8,$1,$2);
	crosshair(1,8,$1,$2);
	crosshair(1,8,$1,$2);
	crosshair(1,4,$1,$2);
	crosshair(1,8,$1,$2);
	#
	crossacct(1,4,$1,$2);
	crosshair(1,8,$1,$2);
	crosshair(1,4,$1,$2);
	crosshair(1,8,$1,$2);
}
define all_giga_rf_beats
{
	tperiod:=60/$2;
	ntimes:=(maxtime/(tperiod*6))+1;
	chstart:=0;
	beatht:=botht+$1;

	side_header("giga rf (%.1f)", $2, beatht);
	#
	crosshair(1,8,tperiod,beatht); # upbeat
	for i = 1 to ntimes do {
		giga_rf_beat(tperiod,beatht);
	}
}
#
# giga corelli_VII
#
define giga_corelli_VII_beat
{
	crossacct(1,4,$1,$2);
	crosshair(1,8,$1,$2);
	crossacct(1,8,$1,$2);
	crosshair(1,8,$1,$2);
	crosshair(1,8,$1,$2);
	#
	crossacct(1,8,$1,$2);
	crosshair(1,8,$1,$2);
	crosshair(1,8,$1,$2);
	crosshair(1,8,$1,$2);
	crosshair(1,8,$1,$2);
	crosshair(1,8,$1,$2);
}
define all_giga_corelli_VII_beats
{
	tperiod:=60/$2;
	ntimes:=(maxtime/(tperiod*6))+1;
	chstart:=0;
	beatht:=botht+$1;

	side_header("giga corelli_VII (%.1f)", $2, beatht);
	#
	for i = 1 to ntimes do {
		giga_corelli_VII_beat(tperiod,beatht);
	}
}
#
# giga corelli_IX
#
define giga_corelli_IX_beat
{
	crossacct(1,4,$1,$2);
	crosshair(1,8,$1,$2);
	crossacct(1,4,$1,$2);
	crosshair(1,8,$1,$2);
	crossacct(5,8,$1,$2);
	crosshair(1,8,$1,$2);
	#
	crossacct(1,8,$1,$2);
	crosshair(1,8,$1,$2);
	crosshair(1,8,$1,$2);
	crossacct(1,8,$1,$2);
	crosshair(1,8,$1,$2);
	crosshair(1,8,$1,$2);
	crossacct(1,8,$1,$2);
	crosshair(1,8,$1,$2);
	crosshair(1,8,$1,$2);
	crossacct(1,4,$1,$2);
	crosshair(1,8,$1,$2);
}
define all_giga_corelli_IX_beats
{
	tperiod:=60/$2;
	ntimes:=(maxtime/(tperiod*12))+1;
	chstart:=0;
	beatht:=botht+$1;

	side_header("giga corelli_IX (%.1f)", $2, beatht);
	#
	crosshair(1,8,tperiod,beatht); # upbeat
	for i = 1 to ntimes do {
		giga_corelli_IX_beat(tperiod,beatht);
	}
}
#
# chaconne jsbach
#
define chaconne_jsbach_beat
{
	crosshair(1,4,$1,$2);
	crossacct(3,8,$1,$2);
	crosshair(1,8,$1,$2);
	#
	crosshair(1,4,$1,$2);
	crossacct(3,8,$1,$2);
	crosshair(1,8,$1,$2);
	#
	crosshair(1,4,$1,$2);
	crosshair(1,4,$1,$2);
	crossacct(1,16,$1,$2);
	crosshair(1,16,$1,$2);
	crosshair(1,16,$1,$2);
	crosshair(1,16,$1,$2);
	#
	crosshair(1,16,$1,$2);
	crosshair(1,16,$1,$2);
	crosshair(1,16,$1,$2);
	crosshair(1,16,$1,$2);
	crossacct(3,8,$1,$2);
	crosshair(1,8,$1,$2);
	#
	crosshair(1,4,$1,$2);
	crossacct(3,8,$1,$2);
	crosshair(1,8,$1,$2);
}
define all_chaconne_jsbach_beats
{
	tperiod:=60/$2;
	ntimes:=(maxtime/(tperiod*15))+1;
	chstart:=0;
	beatht:=botht+$1;

	side_header("chaconne jsbach (%.1f)", $2, beatht);
	#
	crossacct(3,8,tperiod,beatht); # upbeats
	crosshair(1,8,tperiod,beatht); 
	for i = 1 to ntimes do {
		chaconne_jsbach_beat(tperiod,beatht);
	}
}
#
# gavotte (unknown author)
#
define gavotte_beat
{
	crossacct(1,4,$1,$2);
	crosshair(1,4,$1,$2);
	crosshair(1,8,$1,$2);
	crosshair(1,8,$1,$2);
	crosshair(1,8,$1,$2);
	crosshair(1,8,$1,$2);
	#
	crossacct(1,2,$1,$2);
	crosshair(1,4,$1,$2);
	crosshair(1,8,$1,$2);
	crosshair(1,8,$1,$2);
	#
	crossacct(1,4,$1,$2);
	crosshair(1,8,$1,$2);
	crosshair(1,8,$1,$2);
	crosshair(1,4,$1,$2);
	crosshair(1,8,$1,$2);
	crosshair(1,8,$1,$2);
	#
	crossacct(1,4,$1,$2);
	crosshair(1,4,$1,$2);
	crosshair(1,4,$1,$2);
	crosshair(1,4,$1,$2);
}
define all_gavotte_beats
{
	tperiod:=60/$2;
	ntimes:=(maxtime/(tperiod*16))+1;
	chstart:=0;
	beatht:=botht+$1;

	side_header("gavotte (%.1f)", $2, beatht);
	#
	crosshair(1,4,tperiod,beatht); # upbeats
	crosshair(1,4,tperiod,beatht); 
	for i = 1 to ntimes do {
		gavotte_beat(tperiod,beatht);
	}
}
#
# passepied ritmo fundamental
#
define passepied_rf_beat
{
	crossacct(1,8,$1,$2);
	crosshair(1,8,$1,$2);
	crosshair(1,8,$1,$2);
	#
	crossacct(1,8,$1,$2);
	crosshair(1,16,$1,$2);
	crosshair(1,16,$1,$2);
	crosshair(1,16,$1,$2);
	crosshair(1,16,$1,$2);
	#
	crossacct(1,8,$1,$2);
	crosshair(1,16,$1,$2);
	crosshair(1,16,$1,$2);
	crosshair(1,16,$1,$2);
	crosshair(1,16,$1,$2);
}
define all_passepied_rf_beats
{
	tperiod:=60/$2;
	ntimes:=(maxtime/(tperiod*4.5))+1;
	chstart:=0;
	beatht:=botht+$1;

	side_header("passepied rf (%.1f)", $2, beatht);
	#
	crosshair(1,8,tperiod,beatht); 
	for i = 1 to ntimes do {
		passepied_rf_beat(tperiod,beatht);
	}
}
#
# awk header and trailer
#
define header
{
	print "#";
	print "# Portion automatically created from " $1;
	print "#";
}
define trailer
{
	print "#";
	print "# End of pic data";
	print "#";
}
#
# processing functions
#
pfmaxy=9
pfminy=1
pfoff=0
apar=0
bpar=0
powpar=0
pfdeltax=0.005
numx=0
dur=0
pfxstart=0
pfxend=0
pfystart=0
pfyend=0
pfyoff=0.5
#
# func_a_quadratic
# parameters: y0, y1, x0, x1, pow
# 
a_quad_start=0
a_quad_end=0
define func_a_quadratic
{
	a_quad_start:=$3.x/timescale;
	a_quad_end:=$4.x/timescale;
	dur:=a_quad_end-a_quad_start;
	pfoff=(dur/2)+a_quad_start;
	powpar:=$5;
	bpar:=$1
	apar:=($2-$1)/((a_quad_start-pfoff)^powpar)
	numx:=dur/pfdeltax
	pfxstart:=a_quad_start
	pfxend:=a_quad_start+pfdeltax

	print "andfunchard|quad|" a_quad_start "|" a_quad_end "|" pfoff "|" powpar "|" apar "|" bpar;

	for i=0 to numx do
	{
		pfystart:=(apar*((pfxstart-pfoff)^powpar))+bpar
		pfyend:=(apar*((pfxend-pfoff)^powpar))+bpar

		line thick 1.2 from (pfxstart*timescale),(pfystart+pfyoff) to \
			(pfxend*timescale),(pfyend+pfyoff)
		# print "func_a|" pfxstart "|" pfxend "|" pfystart "|" pfyend;

		pfxstart:=pfxstart+pfdeltax;
		pfxend:=pfxend+pfdeltax;
	}
}
#
# func_b_limited_quad
# parameters: y0, y1, x0, x1, x2, pow
# 
b_quad_start=0
b_quad_end=0
define func_b_limited_quad
{
	b_quad_start:=$3.x/timescale;
	b_quad_end:=$4.x/timescale;
	dur:=(b_quad_end-b_quad_start)*2;
	pfoff:=(dur/2)+b_quad_start;
	powpar:=$6;
	bpar:=$1;
	apar:=($2-$1)/((b_quad_start-pfoff)^powpar);
	numx:=(dur/2)/pfdeltax
	pfxstart:=b_quad_start
	pfxend:=b_quad_start+pfdeltax

	print "andfuncsoft|lquad|" b_quad_start "|" b_quad_end "|" $5 "|" pfoff "|" powpar "|"\
		 apar "|" bpar;

	for i=0 to numx do
	{
		pfystart:=(apar*((pfxstart-pfoff)^powpar))+bpar
		pfyend:=(apar*((pfxend-pfoff)^powpar))+bpar

		line from (pfxstart*timescale),(pfystart+pfyoff) to \
			(pfxend*timescale),(pfyend+pfyoff)
		# print "func_b|" pfxstart "|" pfxend "|" pfystart "|" pfyend;

		pfxstart:=pfxstart+pfdeltax;
		pfxend:=pfxend+pfdeltax;
	}

	line from (pfxstart*timescale),(pfystart+pfyoff) to \
		($5*timescale),(pfystart+pfyoff)
}
#
# func_b_limited_cube
#
# divides in two, init and realization
#	- init is used to give parameters to an external program in
#         order to calculate the appropriate constants and produce
#         a file used by
#	- the realization, which completes all the curves reading
#	  the parameter file
# 
b_cube_start=0
b_cube_end=0
cpar=0
dpar=0
#
# func_b_limited_cube_init
# arguments: x0, x2, y1, y2, y3
#
define func_b_limited_cube_init
{
	print "andfuncsoft|lcubeinit|" $1.x/timescale "|" $2.x/timescale \
		"|" $3 "|" $4 "|" $5;
}
#
# func_b_limited_cube_macro (called by func_b_limited_cube() with data
# 	produced by Matlab and saved on a separate file
#
define func_b_limited_cube_macro
{
	b_cube_start:=$1;
	b_cube_end:=$2;
	dur:=(b_cube_end-b_cube_start)*2;
	apar:=$3
	bpar:=$4;
	cpar:=$5;
	dpar:=$6;
	numx:=(dur/2)/pfdeltax
	pfxstart:=b_cube_start
	pfxend:=b_cube_start+pfdeltax

	print "andfuncsoft|lcube|" b_cube_start "|" b_cube_end "|" apar \
		"|" bpar "|" cpar "|" dpar;

	for i=b_cube_start to b_cube_end by pfdeltax do
	{
		pfxend:=i+pfdeltax;
		pfystart:=(apar*(i^3))+(bpar*(i^2))+(cpar*i)+dpar;
		pfyend:=(apar*(pfxend^3))+(bpar*(pfxend^2))+\
			(cpar*pfxend)+dpar;

		line from (i*timescale),(pfystart) to \
			(pfxend*timescale),(pfyend)
	}
}
#
define func_b_limited_cube
{
	sh {
		awk -f cubic-input.awk $1
	}
	copy $2 thru func_b_limited_cube_macro;
}
#
# quarter note division
#
define qline_division
{
	tperiod:=60/$1;
	ntimes:=(maxtime/tperiod);
	chstart=0;

	for i = 1 to ntimes do {
		qline($2,$3,tperiod);
	}
}
#
# spline OR functions
#
define splineseg
{
	line dashed thick 1.8 from $1*timescale,$2 to $3*timescale,$4
}
#
# spline driver
#
define fudgit_spline
{
	copy $1 thru splineseg
	print "orfunc" $3 "|1|spline|" $2;	# fudgit file
}
#
define higher_spline
{
	fudgit_spline($1,$2,"h");
}
#
define lower_spline
{
	fudgit_spline($1,$2,"l");
}
#
# full.include,v
# Revision 0.9  1997/08/16 12:55:30  nicb
# corrected bug in allemanda_rf_beats
#
# Revision 0.8  1997/08/10 09:17:36  nicb
# added passepied beats
#
# Revision 0.7  1997/08/09 18:21:24  nicb
# added side header macros
#
# Revision 0.6  1997/05/25 20:29:59  nicb
# added gavotte beats
#
# Revision 0.5  1997/05/25 16:44:19  nicb
# added cube functions
#
# Revision 0.4  1997/04/20 17:10:32  nicb
# corrected lower/higher spline function bug
#
# Revision 0.3  1997/04/20 16:39:05  nicb
# added all mathematical functions
#
# Revision 0.2  1997/04/20 14:50:53  nicb
# b_func tied to boundaries
#
# Revision 0.1  1997/04/16 09:23:38  nicb
# adapted func_a_quadratic to be controlled with labeled segments
#
# Revision 0.0  1997/04/16 09:08:56  nicb
# Initial Revision (stolen from A0 formal drawing)
#
