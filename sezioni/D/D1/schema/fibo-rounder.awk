#
# $Id$
#
# this is used to round up the pure fibonacci proportions to
# a quantized approximation in order to use metrical rhythms.
# It should be used with the output of fibo (fibo.data)
#
function round(num)
{
	return int((num >= 0) ? num+0.5 : num-0.5);
}
function quantize(num, qt,
	invqt, result)
{
	invqt = 1/qt;
	result = round(num*invqt);
	result /= invqt;

	return result;
}
function output(time, diff)
{
	printf("%7.2f\t\t(%5.2f)\t%s\n", quantize(time/_t_period_, quantum),
		time, diff);
}
BEGIN {
	if (tempo == "")
		tempo = 51;
	if (quantum == "")
		quantum = 1/4;

	_t_period_ = 60/51;
}
/end/ {			# this is a comment, just re-print it
	print;
	next;
}
{
	output($1, $2);
}
#
# $Log$
