#
# $Id: simple-conversion.awk,v 0.0 1997/02/23 09:56:37 nicb Exp nicb $
#
function round(num,
	rounder)
{
	rounder = num >= 0 ? +0.5 : -0.5;
	return int(num+rounder);
}
function convert_sample(n)
{
	return round(n);
}
function convert_instr(n)
{
	return 1;
}
function convert_frq(n,
	frac, base)
{
	frac = (n-1)/100;
	base = 10.00;

	return base+frac;
}
function convert_amp(n,
	result,conv20,max16,off)
{
	off = 96;
	max16 = 32768;
	conv20 = 20/log(10);
	result = (conv20*log(n/max16))+off;

	return result;
}
function convert_dur(n)
{
	return 0.1;
}
function print_header()
{
	print "; created automagically by $RCSfile: simple-conversion.awk,v $ $Revision: 0.0 $";
	print "f1 0 4096 10 1";
}
BEGIN {
	_ampdb_[1] = 60;
	_ampdb_[100] = 66;
	_ampdb_[101] = 69;
	_ampdb_[1000] = 72;
	_ampdb_[1001] = 75;
	_ampdb_[1100] = 78;
	_ampdb_[1101] = 81;
	_ampdb_[2000] = 84;
	_ampdb_[2001] = 87;

	print_header();
}
/^#/ {
	gsub(/^#/, ";");
	print;
	next;
}
{
	at=$1;
	sample=convert_sample($2);
	instr=convert_instr($2);
	frq=convert_frq($2);
	amp=convert_amp($3);
	dur=convert_dur($2);

	printf "i%d %8.5f %8.5f %f %f %d\n", instr, at, dur, amp, frq, sample;
}
#
# $Log: simple-conversion.awk,v $
# Revision 0.0  1997/02/23 09:56:37  nicb
# Initial Revision
#
