sr = 44100
kr = 4410
ksmps = 10
nchnls = 1

; global volume
givol = 0.2


;--------------------------------------------
;      instrument for miditrack 1
;--------------------------------------------

	instr 1

; soundfile >"soundfiles/"< should be an absolute path or a relative path to your sampledirectory

	endin


;--------------------------------------------
;      instrument for miditrack 2
;--------------------------------------------

	instr 2

; sinewave instruments need two parameters
; a) p4 - cycles per second
; b) p5 - amplitude
; not specified but set automatically by MIDI2CS

idur	= p3
icps	= p4
iamp	= p5

amp	linen iamp, -1, idur, .02
asig	oscil	amp, icps, 100
a1	= asig * givol

	out a1

	endin


;--------------------------------------------
;      instrument for miditrack 3
;--------------------------------------------

	instr 3

; sinewave instruments need two parameters
; a) p4 - cycles per second
; b) p5 - amplitude
; not specified but set automatically by MIDI2CS

idur	= p3
icps	= p4
iamp	= p5

amp	linen iamp, -1, idur, .02
asig	oscil	amp, icps, 99
a1	= asig * givol

	out a1

	endin


;--------------------------------------------
;      instrument for miditrack 4
;--------------------------------------------

	instr 4

; sinewave instruments need two parameters
; a) p4 - cycles per second
; b) p5 - amplitude
; not specified but set automatically by MIDI2CS

idur	= p3
icps	= p4
iamp	= p5

amp	linen iamp, -1, idur, .02
asig	oscil	amp, icps, 98
a1	= asig * givol

	out a1

	endin


;--------------------------------------------
;      instrument for miditrack 5
;--------------------------------------------

	instr 5

; sinewave instruments need two parameters
; a) p4 - cycles per second
; b) p5 - amplitude
; not specified but set automatically by MIDI2CS

idur	= p3
icps	= p4
iamp	= p5

amp	linen iamp, -1, idur, .02
asig	oscil	amp, icps, 97
a1	= asig * givol

	out a1

	endin


;--------------------------------------------
;      instrument for miditrack 6
;--------------------------------------------

	instr 6

; sinewave instruments need two parameters
; a) p4 - cycles per second
; b) p5 - amplitude
; not specified but set automatically by MIDI2CS

idur	= p3
icps	= p4
iamp	= p5

amp	linen iamp, -1, idur, .02
asig	oscil	amp, icps, 96
a1	= asig * givol

	out a1

	endin


;--------------------------------------------
;      instrument for miditrack 7
;--------------------------------------------

	instr 7

; sinewave instruments need two parameters
; a) p4 - cycles per second
; b) p5 - amplitude
; not specified but set automatically by MIDI2CS

idur	= p3
icps	= p4
iamp	= p5

amp	linen iamp, -1, idur, .02
asig	oscil	amp, icps, 95
a1	= asig * givol

	out a1

	endin


;--------------------------------------------
;      instrument for miditrack 8
;--------------------------------------------

	instr 8

; sinewave instruments need two parameters
; a) p4 - cycles per second
; b) p5 - amplitude
; not specified but set automatically by MIDI2CS

idur	= p3
icps	= p4
iamp	= p5

amp	linen iamp, -1, idur, .02
asig	oscil	amp, icps, 94
a1	= asig * givol

	out a1

	endin


;--------------------------------------------
;      instrument for miditrack 9
;--------------------------------------------

	instr 9

; sinewave instruments need two parameters
; a) p4 - cycles per second
; b) p5 - amplitude
; not specified but set automatically by MIDI2CS

idur	= p3
icps	= p4
iamp	= p5

amp	linen iamp, -1, idur, .02
asig	oscil	amp, icps, 93
a1	= asig * givol

	out a1

	endin


;--------------------------------------------
;      instrument for miditrack 10
;--------------------------------------------

	instr 10

; sinewave instruments need two parameters
; a) p4 - cycles per second
; b) p5 - amplitude
; not specified but set automatically by MIDI2CS

idur	= p3
icps	= p4
iamp	= p5

amp	linen iamp, -1, idur, .02
asig	oscil	amp, icps, 92
a1	= asig * givol

	out a1

	endin

