# MIDI2CS PROJECT FILE 0.95 - rubo@berlin.snafu.de
#
# This projectfile shows default options
# Change settings however you need
# (deleting semicolons, adding samplefiles, ...)

# --- PROJECT GLOBAL VALUES SECTION ---

midifile	D1.midi		; midifile (will be read)
nameofscore	D1-midi.sco	; score file (will be written)
;scoreheader	midi2cs.hdr	; file to be included into scorefile (readonly)
nameoforc	D1-midi.orc 	;off	; orchestra file (will be written)

# samplerate, mono/stereo, master volume, tempo
sr	22050	; samplerate
nchnls	1	; 1=mono 2=stereo
givol	0.8	; master volume
;tempo	groovesample	beats	; easy looping and syncing!
;tempo	120

# all tracks will be initialized by the following values

firstbar 	0
lastbar 	1000
; drumchannel 10 ; off
notrackpreselected
; duration [+/-]milliseconds
; transpose -24
; lesscomments
; separators

# - define default parameters and succession -

; p_pch
; p_midinote
; p_midivelocity p4
; p_midirelease
p_maxamplitude 32000
p_cps midi2cs.cps p5
; p_fixeddB -6


# --- TRACK SECTION ---
# define individiual values for each track

miditrack	1	;off
	name Marimba
	sound soundfiles/marimba.orc
	relvolume 3
endtrack

miditrack	2	;off
	name Crash
	sound soundfiles/crash.orc
	relvolume 0.5
endtrack

miditrack	3	;off
	name Timpani
	sound soundfiles/soundin.22
	score
		duration sample
	endscore
endtrack

miditrack	4	;off
	name Temple_Blocks
	sound soundfiles/soundin.17
	relvolume 3
	score
		duration sample
	endscore
endtrack

miditrack	5	;off
	name Tam_Tam
	sound soundfiles/soundin.24
	score
		duration sample
	endscore
endtrack

miditrack	6	;off
	name Ride
	sound soundfiles/ride.orc
endtrack

miditrack	7	;off
	name Snare
	sound soundfiles/snare_al_centro.orc
endtrack

miditrack	8	;off
	name Cassa
	sound soundfiles/cassa.orc
	relvolume 4
endtrack

miditrack	9	;off
	name Roto-toms
	sound soundfiles/soundin.20
	score
		duration sample
	endscore
endtrack

miditrack	10	;off
	name Sonagli
	sound soundfiles/soundin.13
	score
		duration sample
	endscore
endtrack

endofproject
