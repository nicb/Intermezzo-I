sr = 22050
kr = 2205
ksmps = 10
nchnls = 1

; global volume
givol = 0.8


;--------------------------------------------
; Marimba     instrument for miditrack 1
;--------------------------------------------

	instr 1

; ------- included from soundfiles/marimba.orc ------
iamp	=	(p4/32767)
aright	soundin	"soundfiles/soundin.10",5.616182
kenv	linseg	0,0.01,iamp,0.249000-0.02,iamp,0.01,0
aright	=	aright*kenv
; ------- end include ------

	out aright*(givol*3.00)

	endin


;--------------------------------------------
; Crash     instrument for miditrack 2
;--------------------------------------------

	instr 2

; ------- included from soundfiles/crash.orc ------
iamp	=	(p4/32767)
aright	soundin	"soundfiles/soundin.7",0.996190
kenv	linseg	0.0001,0.01,iamp,p3-0.01,0.0001
aright	=	aright*kenv
; ------- end include ------

	out aright*(givol*0.50)

	endin


;--------------------------------------------
; Timpani     instrument for miditrack 3
;--------------------------------------------

	instr 3

; WAV sample instrument of track 3 needs parameter amplitude
; not specified but set automatically by MIDI2CS

iamp	= givol*(p4/32767)

kgate	linseg	iamp*1.00,p3*.8,iamp*1.00,p3*.2,.001

a1	soundin	"soundfiles/soundin.22", 0, 0

	aright	= a1*kgate
	out	aright

	endin


;--------------------------------------------
; Temple_Blocks     instrument for miditrack 4
;--------------------------------------------

	instr 4

; WAV sample instrument of track 4 needs parameter amplitude
; not specified but set automatically by MIDI2CS

iamp	= givol*(p4/32767)

kgate	linseg	iamp*3.00,p3*.8,iamp*3.00,p3*.2,.001

a1	soundin	"soundfiles/soundin.17", 0, 0

	aright	= a1*kgate
	out	aright

	endin


;--------------------------------------------
; Tam_Tam     instrument for miditrack 5
;--------------------------------------------

	instr 5

; WAV sample instrument of track 5 needs parameter amplitude
; not specified but set automatically by MIDI2CS

iamp	= givol*(p4/32767)

kgate	linseg	iamp*1.00,p3*.8,iamp*1.00,p3*.2,.001

a1	soundin	"soundfiles/soundin.24", 0, 0

	aright	= a1*kgate
	out	aright

	endin


;--------------------------------------------
; Ride     instrument for miditrack 6
;--------------------------------------------

	instr 6

; ------- included from soundfiles/ride.orc ------
iamp	=	(p4/32767)
aright	soundin	"soundfiles/soundin.1",13.592426
kenv	linseg	0,0.01,iamp,2.307754-0.02,iamp,0.01,0
aright	=	aright*kenv
; ------- end include ------

	out aright*(givol*1.00)

	endin


;--------------------------------------------
; Snare     instrument for miditrack 7
;--------------------------------------------

	instr 7

; ------- included from soundfiles/snare_al_centro.orc ------
iamp	=	(p4/32767)
aright	soundin	"soundfiles/soundin.4",1.687256
kenv	linseg	0,0.01,iamp,0.090476-0.02,iamp,0.01,0
aright	=	aright*kenv
; ------- end include ------

	out aright*(givol*1.00)

	endin


;--------------------------------------------
; Cassa     instrument for miditrack 8
;--------------------------------------------

	instr 8

; ------- included from soundfiles/cassa.orc ------
iamp	=	(p4/32767)
aright	soundin	"soundfiles/soundin.3",0.269372
kenv	linseg	0,0.01,iamp,0.640952-0.02,iamp,0.01,0
aright	=	aright*kenv
; ------- end include ------

	out aright*(givol*4.00)

	endin


;--------------------------------------------
; Roto-toms     instrument for miditrack 9
;--------------------------------------------

	instr 9

; WAV sample instrument of track 9 needs parameter amplitude
; not specified but set automatically by MIDI2CS

iamp	= givol*(p4/32767)

kgate	linseg	iamp*1.00,p3*.8,iamp*1.00,p3*.2,.001

a1	soundin	"soundfiles/soundin.20", 0, 0

	aright	= a1*kgate
	out	aright

	endin


;--------------------------------------------
; Sonagli     instrument for miditrack 10
;--------------------------------------------

	instr 10

; WAV sample instrument of track 10 needs parameter amplitude
; not specified but set automatically by MIDI2CS

iamp	= givol*(p4/32767)

kgate	linseg	iamp*1.00,p3*.8,iamp*1.00,p3*.2,.001

a1	soundin	"soundfiles/soundin.13", 0, 0

	aright	= a1*kgate
	out	aright

	endin

