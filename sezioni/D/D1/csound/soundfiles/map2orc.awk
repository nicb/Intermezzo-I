#
# $Id$
#
function output_dur_sound(sdin,name,skip,dur,
	filename)
{
	gsub(/ /, "_", name);
	filename = sprintf("%s.orc", name);
	printf(";ORC ----- %s ------\n", name) > filename;
	print "iamp\t=\t(p4/32767)" > filename;
	printf("aright\tsoundin\t\"soundfiles/soundin.%d\",%f\n",
		sdin, skip) > filename;
	printf("kenv\tlinseg\t0,0.01,iamp,%f-0.02,iamp,0.01,0\n",
		dur) > filename;
	print "aright\t=\taright*kenv" > filename;
	close(filename);
}
function calc_skip(frq,start,mod,dur,
	log2,n,result)
{
	log2 = 1/log(2);
	n = 12*log2*log(frq/start);
	result = n * mod;
	result = result >= dur ? result - modulo : result;

	return result;
}
function output_range_sound(sdin,name,modulo,dur,start_frq,frq,
	skip)
{
	skip = calc_skip(frq,start_frq,modulo,dur)
	output_dur_sound(sdin,name,skip,modulo);
}
BEGIN \
{
	FS="|";
	freq = freq == "" ? 523.25 : freq;
}
$3 == "1_hit" && $4 != 0 \
{
	output_dur_sound($1,$2,$4,$5)
}
$3 == "range" \
{
	output_range_sound($1,$2,$4,$5,$6, freq)
}
#
# $Log$
