#
# $Id$
#
function delete_array(a,
	i)
{
	for (i = 1; i <= a[0]; ++i)
		delete a[i];

	delete a[0];
}
function within_window(at,
	result, i)
{
	result = 0;

	for (i = 0; i < _ndx_; ++i)
		if (at >= _start_[i] && at <= _end_[i])
		{
#print "**** within window --> ok: " at, _start_[i], _end_[i];
			result = 1;
			break;
		}

	return result;
}
function matching(at,
	result, i, eps, sup, inf)
{
	result = 0;
	eps = 0.01;

	for (i = 0; i < _n_; ++i)
	{
		sup = _win_[i]+eps;
		inf = _win_[i]-eps;
		if (at >= inf && at <= sup)
		{
#print "**** matching --> ok: " at, inf, sup;
			result = 1;
			break;
		}
	}

	return result;
}
function set_time_window(off, end, tempo, rhythms,
	at, rtemp, i, tperiod)
{
	tperiod = 60/tempo;
	rtemp[0] = split(rhythms, rtemp, ",");
	
	at = _start_[_ndx_] = off;
	_end_[_ndx_++] = end;

	while (at <= end)
	{
		for (i = 1; i <= rtemp[0]; ++i)
		{
			_win_[_n_++] = at;
			at += (rtemp[i] * tperiod);
			if (at > end)
				break;
		}
	}
		
	delete_array(rtemp);
}
function print_time_window(\
	i, last)
{
	last = 0;
	for (i = 0; i < _n_; ++i)
	{
		# print "_win_[" i "]=" _win_[i] ", diff: " _win_[i]-last;
		print _win_[i],_win_[i]-last;
		last = _win_[i];
	}
}
function build_time_window(data,
	temp)
{
	temp[0] = split(data, temp, "|");

	set_time_window(temp[3], temp[4], temp[2], temp[6]);

	delete_array(temp);
}
function initialize_structs(data,
	i)
{
	for (i = 1; i <= data[0]; ++i)
		build_time_window(data[i]); 
}
BEGIN {
	_n_ = _ndx_ = 0;

	ins[0]= 3;
	ins[1]= "pavana|96|43.125|69.9|8|1.5,.5,.5,.5,.5,.5,1,.5,.5,.5,.5,.5,.5";
	ins[2]= "siciliana|96|86.25|96.4|1.5|.75,.25,.5";
	ins[3]= "forlana|192|106.25|113|6|2,1,1.5,.5,1";

	initialize_structs(ins);
#	print_time_window();
}
{
	if (within_window($1))
	{
		if(matching($1))
			print;
	}
	else
		print;
}
#
# $Log$
