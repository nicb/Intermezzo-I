#
# dynamics.awk,v 0.0 1997/08/15 17:56:59 nicb Exp
#
# dynamics.awk: converts field 4 (which is in dBs) in musical dynamics
#
# Usage:
#
#	awk -f dynamics.awk [-v zero=<value>] [-v _delta_=<value>] [file ...]
#
# where zero=<value> is the zero dB value (default: 90)
# and _delta_=<value> is the step to jump from one dynamic value to the
#	next
#
function convert_amp(amp,
	result,cur_amp,i)
{
	cur_amp = zero-(_delta_*num_amps);
	i = 0;
	result = _amps_[0];

	while(amp > cur_amp && i < num_amps)
	{
		result = sprintf("%-3s", _amps_[i++]);
		cur_amp += _delta_;
	}

	return result;
}
function header(off,namps,delta)
{
	printf("# dynamics.awk,v 0.0 1997/08/15 17:56:59 nicb Exp params:\n# off=%6.3f, namps=%d, delta=%6.3f\n",
		off, namps, delta);
}
BEGIN \
{
	zero= zero == "" ? 90 : zero;
	_amps_[0] = "ppp";
	_amps_[1] = "pp";
	_amps_[2] = "p";
	_amps_[3] = "mp";
	_amps_[4] = "mf";
	_amps_[5] = "f";
	_amps_[6] = "ff";
	_amps_[7] = "fff";
	num_amps = 8;
	_delta_ = _delta_ == "" ? zero/num_amps : _delta_;

	header(zero,num_amps,_delta_);
}
/^[0-9]/ \
{
	$4=convert_amp($4);
	$1=sprintf("%-6s", $1);
}
{
	print;
}
#
# dynamics.awk,v
# Revision 0.0  1997/08/15 17:56:59  nicb
# Initial Revision
#
