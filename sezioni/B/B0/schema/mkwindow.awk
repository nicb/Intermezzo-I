#
# $Id: mkwindow.awk,v 0.0 1997/08/16 12:34:49 nicb Exp $
#
# mkwindow.awk: this is a preprocessor that builds a window between
#	the closest slice to a selected golden section point and
#	the golden section itself; its output replaces both the slice
#	and the golden section with window data to be processed further
#
# PLEASE NOTE: the important thing is that slice and golden section data
#	come ALL before the note data
#
function delete_array(bf,
	i)
{
	for (i = 1; i <= bf[0]; ++i)
		delete bf[i];

	delete bf[0];
}
function abs(num)
{
	return num < 0 ? -num : num;
}
#
#
#
function save_slice(slice_string,
	idx)
{
	idx = slice[0];
	slice[++idx] = slice_string;
	slice[0] = idx;
}
function save_gdsect(gdsect_string,
	idx)
{
	idx = gdsect[0];
	gdsect[++idx] = gdsect_string;
	gdsect[0] = idx;
}
function get_field(string,field)
{
	temp[0] = split(string, temp, "|");
	result = temp[field];
	delete_array(temp);
	return result;
}
function get_action_time(string,
	result)
{
	return get_field(string,2);
}
function get_info(string,
	result)
{
	return get_field(string,3);
}
function find_closest_slice(gds, sa,
	i, gdsat, saat, closest, cmp, result)
{
	closest=100;
	gdsat = get_action_time(gds);

	for (i = 1; i <= sa[0]; ++i)
	{
		saat = get_action_time(sa[i]);
		cmp = abs(gdsat-saat);
#print "*** sa[" i "]=" sa[i] ", saat=" saat ", cmp=" cmp ", closest=" closest ", result=" result;
		if (cmp < closest)
		{
			closest = cmp;
			result = i;
		}
	}
	return result;
}
function erase_slice(slice)
{
	return sprintf("# (REMOVED) %s", slice);
}
function build_level(gd,s,
	gdi, si)
{
	return sprintf("%d%c", get_info(gd), get_info(s));
}
function output_window(start,end,level)
{
	return sprintf("AAAwindow|%8.5f|%8.5f|%s",start,end,level);
}
function build_window(gdsect,slice,winarray,
	gdsat,saat,winidx,start,end,level)
{
	winidx = winarray[0];
	gdsat = get_action_time(gdsect);
	saat = get_action_time(slice);
	if (gdsat > saat)
	{
		start = saat;
		end = gdsat;
	}
	else
	{
		start = gdsat;
		end = saat;
	}
	level = build_level(gdsect,slice);
	winarray[++winidx]=output_window(start,end,level);
	winarray[0] = winidx;
}
function build_windows(sa, gda, wa,
	i, saidx)
{
	for (i = 1; i <= gda[0]; ++i)
	{
		saidx = find_closest_slice(gda[i],sa);
#print "*** saidx=" saidx ", sa[saidx]=" sa[saidx];
		build_window(gda[i],sa[saidx],wa);
		sa[saidx] = erase_slice(sa[saidx]);
	}
}
function output_window_data(\
	temp_data, i)
{
	temp_data[0] = 0;
	build_windows(slice, gdsect, temp_data);
	for (i = 1; i <= slice[0]; ++i)
		print slice[i];
	for (i = 1; i <= temp_data[0]; ++i)
		print temp_data[i];

	delete_array(temp_data);
}
BEGIN \
{
	FS="|";
	OFS="|";
	doneflag = 0;
	slice[0] = 0;
	gdsect[0] = 0;
	save_slice("AAAslice|0|H");
}
/^#/ \
{
	# these are comments and are passed on
	print
	next;
}
/^AAAslice/ \
{
	save_slice($0);
	next;
}
/^AAAgdsect/ \
{
	save_gdsect($0);
	next;
}
{
	if (!doneflag)
	{
		output_window_data();
		doneflag = 1;
	}
	print;
}
#
# $Log: mkwindow.awk,v $
# Revision 0.0  1997/08/16 12:34:49  nicb
# Initial Revision
#
