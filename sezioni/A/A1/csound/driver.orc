;
; driver.orc,v 0.0 1997/02/23 09:56:37 nicb Exp
;
	sr=44100
	kr=4410
	ksmps=10
	nchnls=1
	

	instr 1
	iamp=ampdb(p4)
	ipitch=cpspch(p5)
	idur=p3/2
	iamphalf=ampdb(p4-2)
kamp	linseg	iamp,idur,iamphalf,idur,0
aout	oscil	kamp,ipitch,1
	out	aout
	endin
;
; driver.orc,v
; Revision 0.0  1997/02/23 09:56:37  nicb
; Initial Revision
;
