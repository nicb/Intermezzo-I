#
# $Id: soundin-conversion.awk,v 0.0 1997/03/23 11:06:58 nicb Exp nicb $
#
function delete_array(bf,
	i)
{
	for (i = 1; i <= bf[0]; ++i)
		delete bf[i];

	delete bf[0];
}
function round(num,
	rounder)
{
	rounder = num >= 0 ? +0.5 : -0.5;
	return int(num+rounder);
}
function convert_sample(n, result,
	nat_instr, command, instr, instrname)
{
	nat_instr = round(n);
	instrname = _sample_[nat_instr];
	command = sprintf("awk -F\"|\" 'name == $2 { print }' name=\"%s\" soundfiles/map.data", instrname);

	command | getline instr;

	close(command);

	result[0] = split(instr, result, "|");
	return;
}
function get_sample_dur(sample,
	result)
{
	result = (sample[3] == "1_hit") ? sample[5] : sample[4];

	return result;
}
function calc_frq_distance(curr, start,
	int_part_curr, frac_part_curr, int_part_start, frac_part_start,
	int_part_res, frac_part_res, result)
{
	int_part_curr = int(curr);
	frac_part_curr = (curr-int_part_curr)*100;
	int_part_start = int(start);
	frac_part_start = (start-int_part_start)*100;

	int_part_res = int_part_curr - int_part_start;
	frac_part_res = frac_part_curr-frac_part_start;
	if (frac_part_res < 0)
	{
		frac_part_res += 12;
		--int_part_res;
	}

	result = (int_part_res*12) + frac_part_res;

	return result;
}
function get_range_sample_start_time(sample, frq,
	start_frq, frq_distance, dur, result)
{
	start_frq = sample[5]; 
	dur = sample[4];
	frq_distance = calc_frq_distance(frq, start_frq);

	result = dur*frq_distance;

	return result;
}
function get_sample_start_time(sample, frq,
	result)
{
	if (sample[3] == "range")
		result = get_range_sample_start_time(sample, frq);
	else
		result = sample[4];

	result = result < 0 ? 0 : result;

	return result;
}
function convert_instr(n)
{
	return 1;
}
function convert_frq(n,
	frac)
{
	frac = (n-1)/100;

	return frq_base+frac;
}
function convert_amp(n)
{
	return _ampdb_[n];
}
function convert_dur(n)
{
	return 0.1;
}
function print_header()
{
	print "; created automagically by $RCSfile: soundin-conversion.awk,v $ $Revision: 0.0 $";
#	print "f1 0 4096 10 1";
	print "; no tables (based on soundins)";
}
function print_output(instr, at, dur, amp, frq, sample,
	sample_start_time, real_dur)
{
	real_dur = get_sample_dur(sample);
	sample_start_time = get_sample_start_time(sample, frq);
	printf "i%d %8.5f %8.5f %f %f %02d %f\n", instr, at, real_dur, amp,
		frq, sample[1], sample_start_time;
}
BEGIN {
	frq_base = 7.00;

	_ampdb_[1] = 60;
	_ampdb_[100] = 66;
	_ampdb_[101] = 69;
	_ampdb_[1000] = 72;
	_ampdb_[1001] = 75;
	_ampdb_[1100] = 78;
	_ampdb_[1101] = 81;
	_ampdb_[2000] = 84;
	_ampdb_[2001] = 87;

	_sample_[1] = "wood-blocks";
	_sample_[2] = "snare al centro";
	_sample_[3] = "xylophone";
	_sample_[4] = "timpani low";
	_sample_[5] = "cassa";
	_sample_[6] = "tam-tam";
	_sample_[7] = "sonagli";
	_sample_[8] = "marimba";
	_sample_[9] = "temple blocks 1";
			# "tamburello";
			# "tamburo a fessura"
			# "ride"
			# "ride sulla campana"
			# "snare accento"
			# "snare sul lato"
			# "crash"
			# "hi-hat"
			# "mandjira (crotali)"
			# "campane tubolari"
			# "sekere"
			# "temple blocks 2"
			# "temple blocks 3"
			# "timbales high"
			# "timbales low"
			# "timpani high"
			# "triangolo"
			# "legnetti low"
			# "legnetti high"
	print_header();
}
/^#/ {
	gsub(/^#/, ";");
	print;
	next;
}
{
	sample[0] = 0;
	at=$1;
	convert_sample($2, sample);
	instr=convert_instr($2);
	frq=convert_frq($2);
	amp=convert_amp($3);
	dur=convert_dur($2);
#print "****", sample[1], sample[2], sample[3], sample[4];

	print_output(instr, at, dur, amp, frq, sample);

	delete_array(sample);
}
#
# $Log: soundin-conversion.awk,v $
# Revision 0.0  1997/03/23 11:06:58  nicb
# Initial Revision
#
#
