	sr=22050
	kr=2205
	ksmps=10
	nchnls=2
	

	instr 1
	inuminstr=28
	ipan=p6/inuminstr
	iscale=1.5
	iamp=ampdb(p4)/32767
	ipitch=cpspch(p5)
	idur=p3/2
	iamphalf=ampdb(p4-2)
; kamp	linseg	iamp,idur,iamphalf,idur,0
aout	soundin	p6,p7
	aout=aout*iamp*iscale
	outs	aout*ipan,aout*(1-ipan)
	endin
