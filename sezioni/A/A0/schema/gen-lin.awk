BEGIN {
	last=0;
	lastval=0;
	delta=2;
}
{
	cur=$1;
	if (lastval == $2)
	{
		for (i = last; i < cur; i += delta)
			printf("%4.2f %4.2f\n", i, lastval);
	}
#	else
#			printf("%4.2f %4.2f\n", cur, lastval);
	lastval=$2;
	last=cur;
}
