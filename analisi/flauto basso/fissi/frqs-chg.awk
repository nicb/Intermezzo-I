#
# $Id: frqs.awk,v 0.0 1997/09/21 18:25:28 nicb Exp nicb $
#
function delete_array(a,
	i)
{
	for (i = a[0]; i; --i)
		delete a[i];
}
function log2(n)
{
	return log(n)/log(2);
}
function nearest(n, result,
	i, oct)
{
	result[0] = 2;				# two-data array structure
	result[1] = int(n/12);			# octaves
	result[2] = ((n/12)-result[1])*12;	# semitones
}
function convert(n,
	cvt, nr_array, result, semitones)
{
	cvt = 12*log2(n/base_frq);
	nearest(cvt, nr_array);
#print "****** cvt:" cvt ", octave: " nr_array[1] ", semitones: " nr_array[2];
	result = sprintf("%f", nr_array[1]+(nr_array[2]/100));
	delete_array(nr_array);
	return result;
}
function frac_part(pch,
	intpart,pch100)
{
	pch100 = pch*100;
	intpart = int(pch100)
	return (pch100-intpart)/100;
}
function reconvert(pch)
{
}
function deduce_tuning(pch,
	invfracpart)
{
	invfracpart = -frac_part(pch);
	return reconvert(8.09+invfracpart);
}
BEGIN {
	tuning = tuning == "" ? 443 : tuning;
	base_frq=tuning/(2**(105/12));	# based on 8.09 pitch class tuning
	printf("# Produced by $RCSfile: frqs.awk,v $ $Revision: 0.0 $\n# tuning: %8.3f\n",
		tuning);
}
{
	frq = convert($2);
	tuning = deduce_tuning(frq);
	print $0 " " frq;
}
#
# $Log: frqs.awk,v $
# Revision 0.0  1997/09/21 18:25:28  nicb
# Initial Revision
#
