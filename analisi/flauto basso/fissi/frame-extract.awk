#
# $Id: frame-extract.awk,v 0.2 1997/10/12 06:55:50 nicb Exp $
#
BEGIN {
	frame = frame == "" ? 10 : frame;
	delta = delta == "" ? 5 : delta;
	start_frame = (frame-delta) < 1 ? 1 : (frame-delta);
	end_frame = frame+delta;
	output_flag = 0;
}
/^# Frame/ {
	if ($4 >= start_frame && $4 <= end_frame)
	{
		output_flag = 1;
		print $0;
	}
	else
		output_flag = 0;
	next;
}
{
	if (output_flag)
		printf("%d %-.6f %-.8f\n", $1, $2, $3);
}
#
# $Log: frame-extract.awk,v $
# Revision 0.2  1997/10/12 06:55:50  nicb
# modified default window to suit larger analisys windows
#
# Revision 0.1  1997/09/21 15:12:55  nicb
# 3d linear version of the plotter
#
# Revision 0.0  1997/09/20 14:16:19  nicb
# Initial Revision
#
