;
; $Id: simul.orc,v 0.0 1997/09/21 16:58:43 nicb Exp $
;
	sr=44100
	kr=441
	ksmps=100
	nchnls=1


	instr 1
	iamp=ampdb(80)*p4
	ipitch=cpspch(p5)
asig	oscil iamp,ipitch,1
	out asig
	endin
;
; $Log: simul.orc,v $
; Revision 0.0  1997/09/21 16:58:43  nicb
; Initial Revision
;
