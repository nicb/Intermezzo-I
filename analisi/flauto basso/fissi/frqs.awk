#
# $Id: frqs.awk,v 0.1 1997/11/29 15:12:34 nicb Exp $
#
function nearest(n, result,
	i, oct)
{
	result[0] = 2;				# two-data array structure
	result[1] = int(n/12);			# octaves
	result[2] = ((n/12)-result[1])*12;	# semitones
}
function convert(n,
	cvt, nr_array, result, semitones)
{
	cvt = 12*log2(n/base_frq);
	nearest(cvt, nr_array);
#print "****** cvt:" cvt ", octave: " nr_array[1] ", semitones: " nr_array[2];
	result = sprintf("%f", nr_array[1]+(nr_array[2]/100));
	delete_array(nr_array);
	return result;
}
BEGIN {
	tuning = tuning == "" ? 443 : tuning;
	base_frq=tuning/(2**(105/12));	# based on 8.09 pitch class tuning
	printf("# Produced by $RCSfile: frqs.awk,v $ $Revision: 0.1 $\n# tuning: %8.3f\n",
		tuning);
}
{
	frq = convert($2);
	print $0 " " frq;
}
#
# $Log: frqs.awk,v $
# Revision 0.1  1997/11/29 15:12:34  nicb
# version that uses awklib.awk
#
# Revision 0.0  1997/09/21 18:25:28  nicb
# Initial Revision
#
