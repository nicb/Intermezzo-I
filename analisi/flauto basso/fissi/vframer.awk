#
# $Id$
#
BEGIN {
	last = 0;
}
/^#/ {
	next;
}
{
	if ($1 != last)
	{
		print "";
		last = $1;
	}
	print $0;
}
#
# $Log$
