#
# $Id: csound-convert.awk,v 0.0 1997/09/21 18:25:28 nicb Exp $
#
function save_array(n, a,
	idx)
{
	idx = a[0];
	a[++idx] = n;
	a[0] = idx;
}
function save_line(amp, pitch)
{
	save_array(amp, _amps_);
	save_array(pitch, _pitches_);
}
function save_max_amp(amp)
{
	if (amp > max_amp)
		max_amp = amp;
}
function produce_lines(\
	i)
{
	for (i = 1; i <= _amps_[0]; ++i)
		printf("i1 0 10 %6.4f %.5f\n", _amps_[i]/max_amp,
			_pitches_[i]);
}
BEGIN {
	max_amp=0;
	_amps_[0] = 0;
	_pitches_[0] = 0;
	print "; Produced by $RCSfile: csound-convert.awk,v $ $Revision: 0.0 $";
	print "f1 0 4096 10 1";
}
END {
	produce_lines();
}
/^#/ {
	gsub(/^#/, ";");
	print;
	next;
}
{
	save_max_amp($3);
	save_line($3, $4);
}
#
# $Log: csound-convert.awk,v $
# Revision 0.0  1997/09/21 18:25:28  nicb
# Initial Revision
#
