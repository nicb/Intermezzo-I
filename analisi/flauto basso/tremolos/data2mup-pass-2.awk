#
# $Id: data2mup-pass-2.awk,v 0.3 1997/12/24 15:25:40 nicb Exp $
#
# this program converts data file as coming out from the "eframe" program
# (which itself is included into the "vframe" program) into a mup score
# that will include all the data present in the data file (partially in
# note form, partially in text form)
#
# The data format is as follows (one note per line):
#
# <frame> <Hz> <Amplitude> <Pitch class>
#
# The data includes two different frames, the "high note" of the tremolo
# first and the "low note" second and there are six notes for each frame
#
# PLEASE NOTE:  in the current release, this program expects the data
#		to pop in one line per note, one frame after another,
#		and with notes in descending order of amplitude (as they
#		come out of eframe 0.0
#
function calc_octave(pch)
{
	# Pitchclass octave 8 corresponds to mup octave 4
	return int(pch)-4;
}
function calc_pitch(rpch, cents, octave,
	pch_enh, pchnum, ch_idx, result)
{
	ch_idx = 1;
	pchnum = (rpch-int(rpch))*100;

	result = verify_pitch(pchnum, ch_idx, octave);

	return result;
}
function calc_staff(rounded_pch,
	result)
{
	result = 1;
	if (rounded_pch < 8.00)
		result = 2;
	
	return result;
}
function get_diatonic_pchname(pname)
{
	return substr(pname, 1, 1);
}
function get_pchname(pnum, ch_idx)
{
	return get_diatonic_pchname(_note_[ch_idx, pnum]);
}
function get_octave_from_name(pname,
	pnamel)
{
	pnamel = length(pname);
	return substr(pname, pnamel, 1);
}
function is_pitch_already_there(pname, octave,
	i, n, temp, result)
{
	result = 0;
	n = _pitches_[0];

	for (i = 1; i <= n; ++i)
	{
		temp[0] = split(_pitches_[i], temp, "|");
		if (pname == temp[1] && octave == temp[2])
			result = 1;

		delete_array(temp);
		if (result)
			break;
	}

	return result;
}
function save_pchname(pnum, octave,
	idx)
{
	append_to_array(sprintf("%s|%d", pnum, octave), _pitches_);
}
function modify_pitch_name(pchnum, ch_idx, octave,
	i, pname)
{
	result = ch_idx;
	pname = get_pchname(pchnum, ch_idx);

	for (i = 1; i <= _note_[0, pchnum]; ++i)
		if (i != ch_idx)
		{
			pname = get_pchname(pchnum, i);
			if (!is_pitch_already_there(pname, octave))
			{
				result = i;
				break;
			}
		}

	if (result == ch_idx)
		warning(sprintf("******* duplicate pitch name for %s\n",
			get_pchname(pchnum, ch_idx)));

	return result;
}
function b_c_relation(old, new, octave,
	result)
{
	result = octave;

	if (old == "c" && new == b)
		--result;
	else if (old == "b" && new == c)
		++result;

	return result;
}
function verify_pitch(pchnum, ch_idx, octave,
	pchname, result, new_ch_idx, new_pchname,
old_octave)
{
	result = _note_[ch_idx, pchnum];
	pchname = get_pchname(pchnum, ch_idx);

	if (is_pitch_already_there(pchname, octave))
	{
		new_ch_idx = modify_pitch_name(pchnum, ch_idx, octave);
		result = _note_[new_ch_idx, pchnum];
		new_pchname = get_pchname(pchnum, new_ch_idx);
old_octave=octave
		octave = b_c_relation(pchname, new_pchname, octave);
if(octave != old_octave)
warning("** verify_pitch: octave changed: old_octave=" old_octave " new octave=" octave " old pchname=" pchname " new_pchname=" new_pchname "\n");
		pchname = new_pchname;
	}

	save_pchname(pchname, octave);
	return result;
}
function calc_note(rounded_pch, cents,
	octave, pitch)
{
	octave = calc_octave(rounded_pch);
	pitch = calc_pitch(rounded_pch, cents, octave);

	return sprintf("%s%d", pitch, octave);
}
function _count_staff_notes(line, returned,
	class, staff)
{
	# struct input_line (inside END block)
	# {
	#1	line number;
	#2	tag;
	#3	pitch class;
	#4	tag;
	#5	dynamic order;
	#6	cents;
	#7	dynamics;
	#8	chord number; {= 1 | 2}
	#9	staff;
	#10	x_info_pos;
	# };
	class = get_field(line, 8, "|");
	staff = get_field(line, 9, "|");

	#staff1noteshi = returned[1];
	#staff2noteshi = returned[2];
	#staff1noteslo = returned[3];
	#staff2noteslo = returned[4];
	if (class == 1)
	{
		if (staff == 1)
			++returned[1];
		else if (staff == 2)
			++returned[2];
		else
			warning(sprintf("_count_staff_notes: class \"%s\" staff %d not recognized\n", 
				class, staff));
	}
	else if (class == 2)
	{
		if (staff == 1)
			++returned[3];
		else if (staff == 2)
			++returned[4];
		else
			warning(sprintf("_count_staff_notes: class \"%s\" staff %d not recognized\n", 
				class, staff));
	}
	else
		warning(sprintf("_count_staff_notes: class \"%s\" not recognized\n", 
			staff));
}
function count_staff_notes(notes, result,
	i)
{
	result[1] = result[2] = result[3] = result[4] = 0;
	result[0] = 4;
	for (i = 1; i <= notes[0]; ++i)
		_count_staff_notes(notes[i], result);
}
function modify_staff_position(notes, oldpos, newpos,
	i, staffpos)
{
	# struct input_line (inside END block)
	# {
	#1	line number;
	#2	tag;
	#3	pitch class;
	#4	tag;
	#5	dynamic order;
	#6	cents;
	#7	dynamics;
	#8	chord number; {= 1 | 2}
	#9	staff;
	#10	x_info_pos;
	# };
	for (i = 1; i <= notes[0]; ++i)
	{
		staffpos = get_field(notes[i], 9, "|");
		if (staffpos == oldpos)
			notes[i] = substitute_field(notes[i], 9, "|", newpos)
	}
}
function verify_staff_positioning(lines,
	staff1noteshi, staff1noteslo, staff2noteshi, staff2noteslo, returned)
{
	returned[0] = 0;

	count_staff_notes(lines, returned);
	staff1noteshi = returned[1];
	staff2noteshi = returned[2];
	staff1noteslo = returned[3];
	staff2noteslo = returned[4];

	if (!staff1noteslo || !staff1noteshi)	# no staff 1 notes
	{
		warning("*********: single staff 1 notes, move 1 -> 2\n");
		modify_staff_position(lines, 1, 2);
	}
	else if (!staff2noteslo || !staff2noteshi) # no staff 2 notes
	{
		warning("*********: single staff 2 notes, move 2 -> 1\n");
		modify_staff_position(lines, 2, 1);
	}

	delete_array(returned);
}
function is_pitch_contiguous(cur_p, last_p,
	cur_pname, last_pname, cur_oct, d, result)
{
	d = 0;
	result = 0;
	cur_pname = get_diatonic_pchname(cur_p);
	last_pname = get_diatonic_pchname(last_p);
	cur_oct = get_octave_from_name(cur_p);

	if (cur_oct == _last_oct_)
	{
		d = abs(_pname_[cur_pname]-_pname_[last_pname])
		result = (d < 2) || (d > 5) ? 1 : 0;
	}
	else if ((cur_oct-_last_oct_) == 1 && last_pname == "b" && cur_pname == "c")
			result = 1; # (bn->cn+1) contiguity


	_last_oct_ = cur_oct;

	return result;
}
function calc_x_info_pos(input_line,
	pch, result)
{
	# struct input_line
	# {
	#	line number;
	#	tag;
	#	pitch name;
	#	tag;
	#	dynamic order;
	#	cents;
	#	dynamics;
	#	chord number;
	# };
	result = short_x_info;
	pch = get_field(input_line, 3, "|");

	if (is_pitch_contiguous(pch, __last_pch__))
		result = _last_x_info_ == short_x_info ? long_x_info : short_x_info;

	__last_pch__ = pch;
	_last_x_info_ = result;
	return result;
}
function convert_pitch(input_line,
	pch, cents, pitch, cmpline, staff)
{
	pch = get_field(input_line, 3, "|");
	cents = get_field(input_line, 6, "|");
	pitch = calc_note(pch, cents);
	staff = calc_staff(pch);
	cmpline = sprintf("%s", pch);
	input_line = substitute_field(input_line, 3, "|", pitch);
	input_line = sprintf("%s|%d|%.2f", input_line, staff, pch); 
	return input_line;
}
function save_line(l)
{
	append_to_array(l, _line_);
}
function pass_two(input_line,
	x_info_pos, trem_part)
{
	trem_part = get_field(input_line, 8, "|"); 
	if (trem_part != _last_trem_part_)
	{
		__last_pch__ = "";
		delete_array(_pitches_);
		_pitches_[0] = 0;
		_last_trem_part_ = trem_part;
	}
	input_line = convert_pitch(input_line);
	x_info_pos = calc_x_info_pos(input_line);
	input_line = sprintf("%s|%.2f", input_line, x_info_pos);
	save_line(input_line);
}
function generate_mup_score_pass_two(\
	i)
{
	verify_staff_positioning(_line_);
	for (i = 1; i <= _line_[0]; ++i)
		print _line_[i];
}
BEGIN {
	FS="|";
	_line_[0] = 0;
	_pitches_[0] = 0;
	_last_trem_part_ = 1;
	_last_x_info_ = short_x_info = 4.9; long_x_info = 20.8;
	__last_pch__ = "";
	#
	# note->pchclass conversion table
	# !!! make sure that b->c changes octave!
	#
	#             normal (or sharp enh)     flat enh.           sharp enh.
	#
	_note_[0,0] = 3; _note_[1,0] = "c"; _note_[2,0] = "d&&"; _note_[3,0] = "b#";
	_note_[0,1] = 2; _note_[1,1] = "c#"; _note_[2,1] = "d&";
	_note_[0,2] = 3; _note_[1,2] = "d"; _note_[2,2] = "e&&"; _note_[3,2] = "cx"
	_note_[0,3] = 2; _note_[1,3] = "d#"; _note_[2,3] = "e&";
	_note_[0,4] = 3; _note_[1,4] = "e"; _note_[2,4] = "f&"; _note_[3,4] = "dx";
	_note_[0,5] = 3; _note_[1,5] = "f"; _note_[2,5] = "g&&"; _note_[3,5] = "e#";
	_note_[0,6] = 2; _note_[1,6] = "f#"; _note_[2,6] = "g&";
	_note_[0,7] = 3; _note_[1,7] = "g"; _note_[2,7] = "a&&"; _note_[3,7] = "fx";
	_note_[0,8] = 2; _note_[1,8] = "g#"; _note_[2,8] = "a&";
	_note_[0,9] = 3; _note_[1,9] = "a"; _note_[2,9] = "b&&"; _note_[3,9] = "gx";
	_note_[0,10] = 2; _note_[1,10] = "a#"; _note_[2,10] = "b&";
	_note_[0,11] = 3; _note_[1,11] = "b"; _note_[2,11] = "c&"; _note_[3,11] = "ax";
	#
	# diatonic name contiguity table
	#
	_pname_["c"] = 0;
	_pname_["d"] = 1;
	_pname_["e"] = 2;
	_pname_["f"] = 3;
	_pname_["g"] = 4;
	_pname_["a"] = 5;
	_pname_["b"] = 6;
	#
	#
	_nline_ = 0;
}
END {
	generate_mup_score_pass_two();
}
$2 == "chord" {
	print; # chord separator, pass it unchanged
	next;
}
$2 ~ /^\/\// {
	print; # this is a comment, pass it unchanged
	next;
}
$2 ~ /^_t/ {
	# this is a code line, in the following format:
	#    $1     $2    $3    $4      $5       $6     $7       $8
	# <lineno>|<tag>|<pch>|<tag>|<dyn.ord>|<cents>|<dyn>|<chord no>
	pass_two($0);
}
#
# $Log: data2mup-pass-2.awk,v $
# Revision 0.3  1997/12/24 15:25:40  nicb
# new display of info data
# corrected enharmonics bug
# best up-down alternation flag
#
# Revision 0.2  1997/12/19 14:37:58  nicb
# added pitch class into information tags
#
# Revision 0.1  1997/12/15 11:02:20  nicb
# corrected many bugs; still an intermediate version
#
# Revision 0.0  1997/12/08 09:37:25  nicb
# Initial Revision
#
