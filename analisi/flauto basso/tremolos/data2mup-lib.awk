#
# $Id: data2mup-lib.awk,v 0.0 1997/12/08 09:37:25 nicb Exp $
#
# data2mup function library
#
function pitchclass_rounding(pch,
	pch100,  rounded, rounded001, integer, frac100, eps)
{
	eps=0.0001;
	pch100 = pch*100;
	rounded = round(pch100);
	rounded001 = rounded/100;
	integer = int(rounded001);
	frac100 = round((rounded001 - integer)*100);
	if (frac100 >= 12)
	{
		frac100 -= 12;
		integer += 1;
	}

#print "pitchclass_rounding: " pch, pch100, rounded, rounded001, integer, frac100;
	return integer+(frac100/100);
}
#
# $Log: data2mup-lib.awk,v $
# Revision 0.0  1997/12/08 09:37:25  nicb
# Initial Revision
#
