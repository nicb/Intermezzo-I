#
# $Id: data2mup-pass-1.awk,v 0.2 1997/12/24 15:25:40 nicb Exp $
#
# this program converts data file as coming out from the "eframe" program
# (which itself is included into the "vframe" program) into a mup score
# that will include all the data present in the data file (partially in
# note form, partially in text form)
#
# The data format is as follows (one note per line):
#
# <frame> <Hz> <Amplitude> <Pitch class>
#
# The data includes two different frames, the first chord of the tremolo
# and then the second one and there are six notes for each frame
#
# PLEASE NOTE:  in the current release, this program expects the data
#		to pop in one line per note, one frame after another,
#		and with notes in descending order of amplitude (as they
#		come out of eframe 0.0
#
function cmp_tag(ntag, itag,
	result, tag1, tag2)
{
	result = 0;
	tag1 = get_field(ntag, 2, "|");
	tag2 = get_field(itag, 1, "|");

	result = (tag1 != tag2) ? 1 : 0;

	return result;
}
function calc_cents(pch, rpch,
	cpch, crpch, pchdiff)
{
	#!! watch for 11.9999 -> 12 (=0) condition
	# 
	cpch=pch*100;
	crpch=rpch*100;
	pchdiff=cpch-crpch;

	return pchdiff-int(pchdiff);
}
function calc_dyn(amp, nfact,
	normamp, i, result, temp)
{
	result = "ff";
	normamp = amp / nfact;

	for (i = 1; i <= _amp_[0]; ++i)
	{
		temp[0] = split(_amp_[i], temp, ":");
		if (normamp > temp[1])
			break;
		result = temp[2];
		delete_array(temp);
	}

	return result;
}
function extract_info(note_struct,
	pch, amp, dynord, nfact)
{
	# struct note (input)
	# {
	#	dynamic order;
	#	pitch;
	#	amplitude;
	#	normalizing_factor (max_amplitude);
	#	line_no;
	# }

	dynord = note_struct[1];
	pch = note_struct[2];
	amp = note_struct[3];
	nfact = note_struct[4];
	cents = calc_cents(pch, pitchclass_rounding(pch));
	dyn = calc_dyn(amp, nfact);
	return sprintf("%d|%.8f|%s", dynord, cents, dyn);

	# struct info (output)
	# {
	#	dynamic order;
	#	cents;
	#	normalized_dynamics;
	# }
}
function assign_tag()
{
	return sprintf("_t%d", __tag_no__++);
}
function save_note(line_num, chord_idx, nl, note,
	tag, idx)
{
	# struct pass_one_output_note
	# {
	#	line_no;
	#	tag;
	#	note;
	# }
	idx = _notes_[chord_idx, 0];
	tag = assign_tag();
	_notes_[chord_idx, ++idx] = sprintf("%d|%s|%.2f", nl, tag, note);
	_notes_[chord_idx, 0] = idx;
	return tag;
}
function save_info(chord_idx, info, tag,
	idx)
{
	# struct pass_one_output_info
	# {
	#	tag;
	#	info;
	# }
	idx = _infos_[chord_idx, 0];
	_infos_[chord_idx, ++idx] = sprintf("%s|%s|%d", tag, info, chord_idx+1);
	_infos_[chord_idx, 0] = idx;
}
function set_note(line_num, chord_idx, note_struct,
	temp, note, info, tag, note_field, linenum_field)
{
	# struct note
	# {
	#	dynamic order;
	#	pitch;
	#	amplitude;
	#	normalizing_factor (max_amplitude);
	#	line_no;
	# }
	temp[0] = split(note_struct, temp, "|");
	note_field = temp[2];
	linenum_field = temp[5];
	note = pitchclass_rounding(note_field);
	info = extract_info(temp);
	tag = save_note(line_num, chord_idx, linenum_field, note);
	save_info(chord_idx, info, tag);
	delete_array(temp);
}
function get_line_num(note_struct,
	temp, pch, result)
{
	result = 2;
	temp[0] = split(note_struct, temp, "|");
	pch = temp[1];
	if (pch > 8.00)
		result = 1;

	delete_array(temp);
	return result;
}
function accumulate_note(nline, frame, amplitude, pch, dyn_ord, nfact,
	idx)
{
	# struct note
	# {
	#	dynamic order;
	#	pitch;
	#	amplitude;
	#	staff_position;
	#	line_no;
	# }
	idx = _chord_[_chord_idx_, 0];
	_chord_[_chord_idx_, ++idx] = sprintf("%d|%.8f|%.8f|%.8f|%d",
		dyn_ord, pch, amplitude, nfact, nline);
	_chord_[_chord_idx_, 0] = idx;
}
function collect_chord(ch_idx,
	i,lnum)
{
	for (i = 1; i <= _chord_[ch_idx, 0]; ++i)
	{
		lnum = get_line_num(_chord_[ch_idx, i]);
		set_note(lnum, ch_idx, _chord_[ch_idx, i]);
	}
}
function pass_one()
{
	for (i = 1; i <= _notes_[0,0]; ++i)
	{
		if (cmp_tag(_notes_[0,i], _infos_[0,i]))
			warning(sprintf("_notes_[0,%d] and _infos_[0,%d] tags differ\n", i, i));
		print _notes_[0,i] "|" _infos_[0,i];
	}

	for (i = 1; i <= _notes_[1,0]; ++i)
	{
		if (cmp_tag(_notes_[1,i], _infos_[1,i]))
			warning(sprintf("_notes_[1,%d] and _infos_[1,%d] tags differ\n", i, i));
		print _notes_[1,i] "|" _infos_[1,i];
	}
}
function generate_mup_score_pass_one()
{
	collect_chord(0);
	collect_chord(1);
	pass_one();
}
BEGIN {
	_tag_idx_ = 0;
	_chord_[0,0] = 0;
	_chord_[1,0] = 0;
	_chord_idx_ = -1;
	_cur_frame_ = 0;
	_notes_[0,0] = 0;
	_notes_[1,0] = 0;
	_infos_[0,0] = 0;
	_infos_[1,0] = 0;
	__tag_no__ = 0;
	_dyn_order_ = 1;
	_norm_factor_ = 0;
	#
	# normalized amplitude -> dynamics conversion table
	#
	_amp_[1]	= "1:ff";
	_amp_[2]	= "0.5:f";
	_amp_[3]	= "0.25:mf";
	_amp_[4]	= "0.125:mp";
	_amp_[5]	= "0.0625:p";
	_amp_[6]	= "0.0312:pp";
	_amp_[0]	= 6;
	#
	#
	_nline_ = 0;
}
END {
	generate_mup_score_pass_one();
}
/^#/ {
	gsub(/^#/,"//");
	print _nline_++ "|" $0;
	next;
}
{
	if ($1 != _cur_frame_)
	{
		++_chord_idx_;
		_cur_frame_ = $1;
		_dyn_order_ = 1;
		_norm_factor_ = $3;
	}
	accumulate_note(_nline_++,$1,$3,$4,_dyn_order_,_norm_factor_);
	++_dyn_order_;
}
#
# $Log: data2mup-pass-1.awk,v $
# Revision 0.2  1997/12/24 15:25:40  nicb
# new display of info data
# corrected enharmonics bug
# best up-down alternation flag
#
# Revision 0.1  1997/12/08 09:35:58  nicb
# modified to do three-pass conversion;
# (this has become pass one)
#
# Revision 0.0  1997/11/21 14:56:12  nicb
# Initial Revision; not fully functional
#
