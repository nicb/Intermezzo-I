#
# frame-convert.awk,v 0.1 1997/09/21 15:12:55 nicb Exp
#
BEGIN {
	FS=",";
	record = 0;
	print "# Converted by frame-convert.awk,v 0.1";
}
NR <= 2  {
	print "# " $0;
	next;
}
NR > 2 {
	print "# Frame n. " ++record;
	for (i = 1; i < NF; i += 2)
	{
		printf("%d %-8s\t", record, $(i+1));
		printf("%-8s\n", $i);
	}
}
#
# frame-convert.awk,v
# Revision 0.1  1997/09/21 15:12:55  nicb
# 3d linear version of the plotter
#
# Revision 0.0  1997/09/20 14:16:19  nicb
# Initial Revision
#
