#
# $Id: suggest-tuning.awk,v 0.1 1997/11/29 15:18:06 nicb Exp $
#
# this program picks up the output of the filter.awk program which comes
# in the following format:
#
# 	<frame no> <Hz> <Amplitude> <Pitch class>
#
# it checks how far (in cents) is the first pitch (the louder one)
# and suggests a global tuning of the system that will correct the
# pitch aggregate
#
# PLEASE NOTE: when running with gawk 3.03, this program must be run with
#	       the --compat flag because of a bug (feature?): the variables
#	       in the BEGIN block do not get initialized properly (grrr....)
#
function cent_rounding(pch)
{
	return (round(pch*100)/100);
}
function produce_suggested_tuning(pch,
	rounded_pch, pch_diff, result)
{
	rounded_pch = cent_rounding(pch);
	pch_diff = (pch - rounded_pch)*100;
	result = current_tuning * (2**((pch_diff/12)));
#print "# rounded_pch: " rounded_pch ", pch_diff: " pch_diff ", result: " result;

	return result;
}
BEGIN {
	_first_ = 1;
}
END {
	printf("# Suggested tuning by $RCSfile: suggest-tuning.awk,v $ $Revision: 0.1 $: %.7f\n",
		_suggested_tuning_);
}
/^# tuning/ {
	current_tuning = $3;
	print;
	next;
}
/^#/ {
	print;
	next;
}
{
	if (_first_)
	{
		_suggested_tuning_ = produce_suggested_tuning($4);
		_first_ = 0;
	}

	print;
}
#
# $Log: suggest-tuning.awk,v $
# Revision 0.1  1997/11/29 15:18:06  nicb
# fitted to the awklib library function usage
#
# Revision 0.0  1997/11/17 08:53:00  nicb
# Initial Revision; functional
#
