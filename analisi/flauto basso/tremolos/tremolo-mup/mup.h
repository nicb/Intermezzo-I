// $Id: mup.h,v 0.0 1997/12/26 17:03:00 nicb Exp nicb $
//
// Header files for tremolo score
//
score
//
// a3 size
//
pagewidth=11.6944
pageheight=16.5278
packfact=2.8
scoresep=15,35
staffsep=28			// separation between staffs
scorepad=28			// separation between staff systems
time=4/4
//
//
beamstyle=1
staffs=2
barstyle=1-2
brace=1-2
staff 2
clef=bass
header
	title bold (20) "Tremolo multiphonics notes"
footer2
	title "- \% -"
music
//
// $Log: mup.h,v $
// Revision 0.0  1997/12/26 17:03:00  nicb
// Initial Revision
//
