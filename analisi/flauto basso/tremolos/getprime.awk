#
# $Id: getprime.awk,v 0.1 1998/01/10 14:00:00 nicb Exp $
#
# this program extracts the prime form from each chord of the tremolo
# data files, by:
#
# - extracting each unique note number for each chord
# - calculating the prime form and set for each chord
# - combining both chords and retrieving its prime form
#
function get_pitch(note,
	intpart,result)
{
	intpart=int(note);
	result=int((note-intpart)*100);

	return result;
}
function save(chidx, pitch, array,
	idx)
{
	idx=array[chidx,0];
	array[chidx,++idx] = pitch;
	array[chidx,0] = idx;
}
function not_existing(chidx, pitch,
	i, result)
{
	result = 1;

	for (i=1; i <= _chords_[chidx,0]; ++i)
		if (pitch == _chords_[chidx,i])
		{
			result = 0;
			break;
		}

	return result;
}
function record(chidx, note,
	idx, pitch, rpch)
{
	rpch = pitchclass_rounding(note);
	pitch = get_pitch(rpch);
	if (not_existing(chidx, pitch))
	{
		save(chidx, pitch, _chords_);
		if (not_existing(3, pitch))
			save(3, pitch, _chords_);
	}
	save(chidx, rpch, _collections_);
}
function build_string(chidx,
	i, result)
{
	result = "";
	for (i = 1; i <= _chords_[chidx,0]; ++i)
	{
		result = sprintf("%s%d", result, _chords_[chidx,i]);
		if (i != _chords_[chidx,0])
			result = sprintf("%s,", result);
	}

	return result;
}
function get_highest_note(chidx,
	i, result)
{
	result = 0;

	for (i=1; i <= _collections_[chidx,0]; ++i)
		if (_collections_[chidx,i] > result)
			result = _collections_[chidx,i];

	return result;
}
function get_lowest_note(chidx,
	i, result)
{
	result = 50;

	#
	# we should avoid to go in the lower staff
	# (this is just a hack, should be done a little better)
	#
	for (i=1; i <= _collections_[chidx,0]; ++i)
	{
		if ((_collections_[chidx,i] > 7.05) && (_collections_[chidx,i] < result))
			result = _collections_[chidx,i];
	}

	return result;
}
function calculate_interval(low, high,
	hioct, looct, hifrac, lofrac, frac, result, oct, intvs)
{
	looct = int(low);
	lofrac = (low - looct) * 100;
	hioct = int (high);
	hifrac = (high - hioct) * 100;

	if (hifrac < lofrac)
	{
		hifrac += 12;
		--hioct;
	}

	oct = hioct - looct;
	frac = hifrac - lofrac;
	
	result = (oct*12)+frac;
#print "***** low=" low ", high=" high ", frac=" frac ", oct=" oct ", result=" result;

	return result;
}
function calculate_extra_info(chidx,
	highest, lowest, intv, filename, result)
{
	filename = basename(FILENAME, ".data");
	if (chidx < 3)
	{
		highest = get_highest_note(chidx);
		lowest = get_lowest_note(chidx);
		intv = calculate_interval(lowest, highest);

		result = sprintf("%d:%.2f:%s:%d", intv, highest, filename,
			chidx);
	}
	else
		result = sprintf("::%s:%d", filename, chidx);

	return result;
}
function calculate_set(chidx,
	argstring, command, output, info)
{
	info = "";
	argstring = build_string(chidx);
	command = sprintf("%s/prime %s", _path_, argstring);

	command | getline output;
	close(command);

	info = calculate_extra_info(chidx);

	printf("%s:%s\n", output, info);
}
function calculate_sets()
{
	calculate_set(1);
	calculate_set(2);
	calculate_set(3);
}
END {
	calculate_sets();
}
BEGIN {
	_nchords_ = 3;
	_chords_[1,0] = 0;
	_chords_[2,0] = 0;
	_chords_[3,0] = 0;
	_collections_[1,0] = 0;
	_collections_[2,0] = 0;
	_path_ = _path_ == "" ? "/u/user/nicb/me/music/set-stuff/forte/bin" : _path_;
}
/^# tuning:/ {
	++_chord_idx_;
}
/^#/ {
	next;
}
{
	record(_chord_idx_, $4);
}
#
# $Log: getprime.awk,v $
# Revision 0.1  1998/01/10 14:00:00  nicb
# corrected bug in interval calculation
#
# Revision 0.0  1997/12/27 10:08:13  nicb
# Initial Revision
#
