#
# tblentry.awk,v 0.1 1998/01/23 17:24:04 nicb Exp
#
function analyze(line, patt1, patt2,
	temp, m1, m2)
{
	m1 = m2 = 0;
	temp[0] = split(line, temp, "|");

	if ((m1=index(temp[1], patt1)) && (m2=index(temp[2], patt2)))
	{
		print temp[3];
#print "***** line=" line ", patt1=" patt1 ", patt2=" patt2 ", m1=" m1 ", m2=" m2 ", temp[3]=" temp[3];
	}

	delete_array(temp);
}
BEGIN {
}
/^#/ {
	next;
}
{
	analyze($0, p1, p2)
}
#
# tblentry.awk,v
# Revision 0.1  1998/01/23 17:24:04  nicb
# corrected bug in pattern selection and extraction
#
# Revision 0.0  1998/01/20 11:28:41  nicb
# Initial Revision
#
