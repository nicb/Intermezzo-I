#
# $Id: extremes.awk,v 0.0 1998/01/12 05:08:57 nicb Exp nicb $
#
function _write_extremes(idx, which_distance)
{
	do
	{
		print _line_[idx++];
	} while ((idx <= _line_[0]) &&
	         (get_field(_line_[idx], 2, "|") == which_distance));
}
function write_extremes(\
	i, start_distance, end_distance)
{
	start_distance = get_field(_line_[1], 2, "|");
	end_distance = get_field(_line_[_line_[0]], 2, "|");

	i=1;
	_write_extremes(i, start_distance);

	i=_line_[0];
	while (get_field(_line_[i], 2, "|") == end_distance)
		--i;
	++i;
	_write_extremes(i, end_distance);
}
BEGIN {
	FS="|";
	begin_flag=1;
	_line_[0] = 0;
}
END {
	write_extremes();
}
/^#/ {
	print;
	next;
}
{
	append_to_array($0, _line_);
}
#
# $Log: extremes.awk,v $
# Revision 0.0  1998/01/12 05:08:57  nicb
# Initial Revision
#
