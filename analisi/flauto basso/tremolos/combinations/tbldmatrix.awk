#
# tbldmatrix.awk,v 0.1 1998/01/23 17:22:28 nicb Exp
#
function tbl_header(sno, dist,
	i)
{
	print ".S 9 9";
	printf(".DS C\n\\f3Common ordinal set table for set number %d (distance %s)\\fP\n.DE\n",
		sno, dist == 0 ? "0" : sprintf(">= %d", dist));
	print ".PF \"\\'\\'\\'\\s4tbldmatrix.awk,v 0.1 1998/01/23 17:22:28 nicb Exp\\s0\\'\"";
	print ".S 5 5";
	print ".char \\[hyph] \\fS\\(ba";
	print ".char \\[check] \\f(ZD\\N'52'";
	print ".TS H";
	printf "center box tab(|);\nlf3p+1w(1.5c) | ";
	for (i = 1; i <= _line_[0]; ++i)
	{
		printf("cf3w(0.1c)");
		if (i < _line_[0])
			printf(" | ");
	}
	printf(".\n.sp .1v\n");
}
function create_header(sno, dist,
	col, row, char)
{
	tbl_header(sno, dist);
	for (row = 1; row <= 8; ++row)
	{
		printf("|");
		for (col = 1; col <= _line_[0]; ++col)
		{
			char = substr(_line_[col], row, 1);
			if (char == "-")
				char = "\\[hyph]";
			printf("%s", char);
			if (col < _line_[0])
				printf("|");
			else
				printf("\n");
		}
	}
	print "_\n.TH";
}
function create_row(row_line, dist,
	col, command, entry, result)
{
	printf("%s", row_line);
	for (col = 1; col <= _line_[0]; ++col)
	{
		entry = "";
		if (row_line == _line_[col])
		{
			printf("|");
			continue;
		}
		command = sprintf("awk -f /u/user/nicb/me/lib/awk/awklib.awk -f tbldentry.awk -v p1=%s -v dist=%d %s/%d/tremolo-%s-distance_%d.data",
			_line_[col], dist, _path_, setno, row_line, setno);
		command | getline result;
		entry = result ? (dist == 0 ? "\\[check]" : sprintf("%d", result)) : "";
#warning("***** " _line_[col] "=" result " ---> " entry "\n");
		printf("|%s", "" entry);
		close(command);
	}
}
function create_following(dist,
	row)
{
	for (row = 1; row <= _line_[0]; ++row)
	{
		printf(".sp .2v\n")
		create_row(_line_[row], dist);
		if (row < _line_[0])
			printf("\n_\n");
	}
}
function create_matrix(sno)
{
	create_header(sno, distance);
	create_following(distance);
}
BEGIN {
	_line_[0] = 0;
	setno = setno == "" ? 3 : setno;
	_path_ = _path_ == "" ? "./stats/distance" : _path_;
	distance = distance == "" ? 0 : distance;
}
END {
	create_matrix(setno);
	print "\n.TE";
}
{
	append_to_array($0, _line_, distance);
}
#
# tbldmatrix.awk,v
# Revision 0.1  1998/01/23 17:22:28  nicb
# no changes, just debug diversity
#
# Revision 0.0  1998/01/23 16:45:57  nicb
# Initial Revision
#
