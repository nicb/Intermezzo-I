#
# majors.awk,v 0.0 1998/01/18 20:05:58 nicb Exp
#
function output(name, line)
{
	printf("%s|%s\n", name, line);
}
BEGIN {
	FS="|"
	_last_no_ = -1;
}
/^#/ {
	next;
}
{
	no = $2
	if (no >= _last_no_)
	{
		_last_no_ = no;
		output(FILENAME, $0);
	}
}
#
# majors.awk,v
# Revision 0.0  1998/01/18 20:05:58  nicb
# Initial Revision
#
