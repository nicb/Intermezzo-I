#
# tblmatrix.awk,v 0.2 1998/01/23 17:23:06 nicb Exp
#
function tbl_header(sno,
	i)
{
	print ".S 9 9";
	printf(".DS C\n\\f3Combination table for set number %d\\fP\n.DE\n",
		sno);
	print ".PF \"\\'\\'\\'\\s4tblmatrix.awk,v 0.2 1998/01/23 17:23:06 nicb Exp\\s0\\'\"";
	print ".S 5 5";
	print ".char \\[hyph] \\fS\\(ba";
	print ".TS H";
	printf "center box tab(|);\nlf3p+1w(1.5c) | ";
	for (i = 1; i <= _line_[0]; ++i)
	{
		printf("cf3w(0.1c)");
		if (i < _line_[0])
			printf(" | ");
	}
	printf(".\n.sp .1v\n");
}
function create_header(sno,
	col, row, char)
{
	tbl_header(sno);
	for (row = 1; row <= 8; ++row)
	{
		printf("|");
		for (col = 1; col <= _line_[0]; ++col)
		{
			char = substr(_line_[col], row, 1);
			if (char == "-")
				char = "\\[hyph]";
			printf("%s", char);
			if (col < _line_[0])
				printf("|");
			else
				printf("\n");
		}
	}
	print "_\n.TH";
}
function create_row(row_line,
	col, command, entry)
{
	printf("%s", row_line);
	for (col = 1; col <= _line_[0]; ++col)
	{
		entry = "";
		if (row_line == _line_[col])
		{
			printf("|");
			continue;
		}
		command = sprintf("awk -f /u/user/nicb/me/lib/awk/awklib.awk -f tblentry.awk -v p1=%s -v p2=%s %s/%d/majors %s/%d/minors | head -1",
			row_line, _line_[col], _path_, setno, _path_, setno);
		command | getline entry;
		printf("|%s", entry);
		close(command);
	}
}
function create_following(\
	row)
{
	for (row = 1; row <= _line_[0]; ++row)
	{
		printf(".sp .2v\n")
		create_row(_line_[row]);
		if (row < _line_[0])
			printf("\n_\n");
	}
}
function create_matrix(sno)
{
	create_header(sno);
	create_following();
}
BEGIN {
	_line_[0] = 0;
	setno = setno == "" ? 3 : setno;
	_path_ = _path_ == "" ? "./stats/common-notes" : _path_;
}
END {
	create_matrix(setno);
	print "\n.TE";
}
{
	append_to_array($0, _line_);
}
#
# tblmatrix.awk,v
# Revision 0.2  1998/01/23 17:23:06  nicb
# added left column width
#
# Revision 0.1  1998/01/21 17:24:25  nicb
# corrected set auto scanning bug
#
# Revision 0.0  1998/01/20 11:28:41  nicb
# Initial Revision
#
