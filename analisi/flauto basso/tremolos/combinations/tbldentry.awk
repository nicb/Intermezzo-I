#
# tbldentry.awk,v 0.0 1998/01/23 16:45:57 nicb Exp
#
function analyze(line, patt1, dist,
	temp, m1, level)
{
	m1 = 0;
	temp[0] = split(line, temp, "|");
	level = temp[2];

	if (dist == 0)
	{
		if ((m1=index(temp[1], patt1)) && level == dist)
		{
				_result_ = 1;
				exit(0);
		}
	}
	else
	{
		if ((m1=index(temp[1], patt1)) && level >= dist)
		{
				_result_ = level;
				exit(0);
		}
	}

	delete_array(temp);
}
BEGIN {
	_result_=0;
}
END {
	print _result_;
}
/^#/ {
	next;
}
{
	analyze($0, p1, dist);
}
#
# tbldentry.awk,v
# Revision 0.0  1998/01/23 16:45:57  nicb
# Initial Revision
#
