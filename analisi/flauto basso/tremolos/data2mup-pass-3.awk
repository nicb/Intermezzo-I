#
# $Id: data2mup-pass-3.awk,v 0.4 1998/01/24 18:38:53 nicb Exp $
#
# this is pass 3 of data2mup conversion, which receives comments and data
# in lines, in the following format:
#    $1     $2    $3    $4      $5       $6     $7      $8         $9
# <lineno>|<tag>|<pch>|<tag>|<dyn.ord>|<cents>|<dyn>|<chord no.>|<staff>|\
#        $10        $11
#    <pitchclass>|<info_x>
#
function _generate_info(ytag, ord, tuning, dynamics, x_pos, thischord, pch, n,
	xtag, ypos)
{
	_lowest_tag_ = ypos = (_y_delta_*n) + 0.8;
	xtag = ytag;
		
	printf("print (%s.x + %.2f, %s.y - %3.1f) \"\\s(-3)%d, %+4.2f, \\f(PX)%s\\f(PV), (%-.2f)\\s(+3)\"\n",
		xtag, x_pos, ytag, ypos, ord, tuning, dynamics, pch);
}
function save_line(l)
{
	append_to_array(l, _line_);
}
function save_tuning(tuning_line,
	tuning, temp)
{
	temp[0]=split(tuning_line, temp, " ");
	tuning = temp[3];
	append_to_array(tuning, _tuning_);
}
function create_header(staffno,
	updown)
{
	updown = staffno == 1 ? "[up]" : "[down]";
	clear_tallest();

	return sprintf("%d: %s 1", staffno, updown);
}
function print_name(string, tunings,
	i)
{
	printf("bold above 1: 1.1 \"%s (", name);
	
	for (i = 1; i <= tunings[0]; ++i)
	{
		printf("%.2f", tunings[i]);
		if (i != tunings[0])
			printf("-");
	}
	print ")\";\nbold above 1: 1.1 \"\";";
	print "bold above 1: 1.1 \"\";";
}
#
# <line> is a code line, in the following format:
#    $1     $2    $3    $4      $5       $6     $7       $8        $9
# <lineno>|<tag>|<pch>|<tag>|<dyn.ord>|<cents>|<dyn>|<chord no.>|<staff>|\
#         $10       $11
#    <pitchclass>|<info_x>
#
function generate_info(line, tallest_idx, nnote,
	temp, tag, dyn_ord, cents, dyn, info_x, pch, chno) 
{
	temp[0] = split(line, temp, "|");
	tag = _tallest_tag_[tallest_idx];
	dyn_ord = temp[5];
	cents = temp[6];
	dyn = temp[7];
	chno = temp[8];
	pch = temp[10];
	info_x = _x_offset_;

	_generate_info(tag, dyn_ord, cents, dyn, info_x, chno, pch, nnote);
	delete_array(temp);
}
function clear_tallest()
{
	++_tallest_idx_;
	_tallest_note_[_tallest_idx_] = 0;
	_tallest_tag_[_tallest_idx_]  = "";
}
function check_tallest(line,
	temp, tag, pch, chno)
{
	temp[0] = split(line,temp,"|");
	tag = temp[2];
	pch = temp[10];
	chno = temp[8];

	if (pch > _tallest_note_[chno])
	{
		_tallest_tag_[chno] = tag;
		_tallest_note_[chno] = pch;
	}

	delete_array(temp);
}
function get_absolute_y_pos(tallest_note, intv,
	pch, oct)
{
	oct = int(tallest_note);
	pch = (tallest_note-oct)*100;

	return ((oct*24)+pch)
}
#
# set output is as follows:
#
# <set notes>:[set name](prime form):<interval vector>:<iwidth>:<tallest note>
#
# the first line is the left chord, the second the right chord,
# and the third line is the combination
#
function generate_set_info(setfile,
	chord, i, positions, xoffss, xpos, yoffss, ypos, xtag, ytag, tagidx,
	labels, label, rypos, lowestypos, lowestabsypos, edgeypos, absypos,
	tag3idx)
{
	xoffss = "7:7:14.50";
	xpos[0] = split(xoffss, xpos, ":");
	yoffss = "21.20:21.20:26.20";
	ypos[0] = split(yoffss, ypos, ":");
	labels = "left:left:center";
	label[0] = split(labels, label, ":");
	chord[0] = 3;
	getline chord[1] < setfile;
	getline chord[2] < setfile;
	getline chord[3] < setfile;
	lowestabsypos = 1000;

	for (i = 1; i < chord[0]; ++i)
	{
		schord[0] = split(chord[i], schord, ":");
		edgeypos = schord[4]*0.8153;
		ytag = xtag = _tallest_tag_[i];
		absypos = get_absolute_y_pos(schord[5], schord[4]);
		rypos = edgeypos > ypos[i] ? edgeypos : ypos[i];
#print "**** absypos=" absypos ", lowestabsypos=" lowestabsypos ", rypos=" rypos;
		if (absypos < lowestabsypos)
		{
				tag3idx = i;
				lowestypos = rypos;
				lowestabsypos = absypos;
		}
		print_set_info(chord[i], positions[i], xtag, ytag, xpos[i], rypos,
			label[i]);
		delete_array(schord);
	}

	schord[0] = split(chord[i], schord, ":");
	ytag = _tallest_tag_[tag3idx];
	rypos = lowestypos > ypos[i] ? lowestypos+2 : ypos[i];
#print "**** i=" i ", tag3idx=" tag3idx ", lowestypos=" lowestypos ", lowestabsypos=" lowestabsypos ", _tallest_tag_[tag3idx]=" _tallest_tag_[tag3idx] ", ytag=" ytag;
	print_set_info(chord[i], positions[i], xtag, ytag, xpos[i], rypos, label[i]);
	delete_array(schord);


	delete_array(xpos);
	delete_array(ypos);
	delete_array(label);
}
function print_set_info(set, pos, xtag, ytag, xoff, yoff, label,
	pf, iv, temp)
{
	temp[0] = split(set, temp, ":")
	pf = temp[2];
	iv = temp[3];

	printf("%s (%s.x - %.2f, %s.y - %.2f) \"%s %s\";\n", label, xtag, xoff, ytag, yoff, pf, iv);
	delete_array(temp);
}
#
# <line> is a code line, in the following format:
#    $1     $2    $3    $4      $5       $6     $7       $8        $9
# <lineno>|<tag>|<pch>|<tag>|<dyn.ord>|<cents>|<dyn>|<chord no.>|<staff>|\
#         $10       $11
#    <pitchclass>|<info_x>
#
function pass_three(staffno, this_chord, line,
	temp, tag, pch)
{
	updown = staffno == 1 ? "[up]" : "[down]";
	temp[0] = split(line, temp, "|");
	tag = temp[2];
	pch = temp[3];

	if (_staff_[staffno] == "")
		_staff_[staffno] =  create_header(staffno);

	if (this_chord != _last_chord_[staffno] && this_chord == 2)
		_staff_[staffno] = sprintf("%s alt 2; %s 1", _staff_[staffno],
			updown);

	check_tallest(line);
	
	_staff_[staffno] = sprintf("%s %s = %s", _staff_[staffno], pch, tag);
	++_num_notes_[this_chord];

	_last_chord_[staffno] = this_chord;

	save_line(line);
	delete_array(temp);
}
function generate_mup_score_pass_three(name, tunings,
	i, j, k, tot_notes)
{
	tot_notes = 0;
	for (i = 1; i <= 2; ++i)
		if (_staff_[i] != "")
			printf("%s;\n", _staff_[i]);
	
	print_name(name, tunings);
	i=1;
	for (j = 1; j <= _num_notes_[0]; ++j)
	{
		k = 0;
		tot_notes += _num_notes_[j];
		while(i <= tot_notes)
		{
			generate_info(_line_[i],j,k);
			++i;
			++k;
		}
	}
	generate_set_info(setfile);
	print "bar";
}
BEGIN {
	FS="|";
	_staff_[1] = "";
	_staff_[2] = "";
	_line_[0] = 0;
	_tuning_[0] = 0;
	_last_chord_[0] = 2;
	_last_chord_[1] = _last_chord_[2] = 0;
	_tallest_idx_ = 0;
	_tallest_tag_[0] = 0;
	_tallest_note_[0] = 0;
	_num_notes_[0] = 2;
	_y_delta_=3.2;		# constant
	_x_offset_=6.2;		# constant
}
END {
	generate_mup_score_pass_three(name, _tuning_);
}
$2 ~ /tuning:/ {
	save_tuning($2);
}
$2 ~ /^\/\// {
	print $2; 		# this is a comment, pass it unchanged
				# except for line number
				# (this contains a bug - and not a so subtle
				# one: if there are separator characters in the
				# comment the latter will be truncated - but
				# no time to fix it)
	next;
}
$2 ~ /^_t/ {
	pass_three($9, $8, $0);
}
#
# $Log: data2mup-pass-3.awk,v $
# Revision 0.4  1998/01/24 18:38:53  nicb
# added lowest position extraction for sets
#
# Revision 0.3  1997/12/24 15:25:40  nicb
# new display of info data
# corrected enharmonics bug
# best up-down alternation flag
#
# Revision 0.2  1997/12/19 14:37:58  nicb
# added pitch class into information tags
#
# Revision 0.1  1997/12/15 11:02:20  nicb
# corrected many bugs; still an intermediate version
#
# Revision 0.0  1997/12/08 09:37:25  nicb
# Initial Revision
#
