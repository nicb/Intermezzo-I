#
# $Id: filter.awk,v 0.1 1997/11/29 15:19:32 nicb Exp nicb $
#
# this program picks up the output of the frqs.awk program which comes
# in the following format:
#
# 	<frame no> <Hz> <Amplitude> <Pitch class>
#
# It buffers the pitch class data and deletes all the lines that come
# with a pitch class too close to an already seen pitch class
#
# PLEASE NOTE: when running with gawk 3.03, this program must be run with
#	       the --compat flag because of a bug (feature?): the variables
#	       in the BEGIN block do not get initialized properly (grrr....)
#
function save(pch,
	idx)
{
	idx = _buffer_[0];
	_buffer_[++idx] = pch;
	_buffer_[0] = idx;
}
function not_seen_before(pch,
	n, i, result, rounded)
{
	result = 1;
	n = _buffer_[0];
	rounded = pitchclass_rounding(pch);

	for (i = 1; i <= n; ++i)
	{
		if (rounded == _buffer_[i])
		{
			result = 0;
			break;
		}
	}

	if (result)
		save(rounded);

	return result;
}
function print_header()
{
	print "# Filtered with $RCSfile: filter.awk,v $ $Revision: 0.1 $";
}
BEGIN {
	_delta_ = _delta_ == "" ? 0.008 : _delta_;
	_buffer_[0] = 0;
	_first_ = 1;
}
/^#/ {
	print;
	next;
}
{
	if (_first_)
	{
		print_header();
		_first_ = 0;
	}
	#
	# <Hz> could be zero, which would turn out to be NaN on $4
	#
	if ($4 != "NaN" && not_seen_before($4))
		print;
#
#	limit to 6 notes
#
	if (_buffer_[0] == 6)
		exit(0);
}
#
# $Log: filter.awk,v $
# Revision 0.1  1997/11/29 15:19:32  nicb
# modified to use pitchclass_rounding()
#
# Revision 0.0  1997/11/16 17:24:56  nicb
# Initial Revision
#
